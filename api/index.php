<?
header("Content-Type: application/json; charset=utf-8");
define('STOP_STATISTICS', true);
require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

$arResult = [];

if (isset($_REQUEST['action'])) {
	$login = htmlspecialchars($_REQUEST['login']);
	$partnerId = htmlspecialchars($_REQUEST['partner_id']);
	$password = htmlspecialchars($_REQUEST['password']);
	$rate = htmlspecialchars($_REQUEST['rate']);
	$date = date('d.m.Y H:i:s', strtotime($_REQUEST['date']));
	switch ($_REQUEST['action']) {
		case 'user_create':
			$existUser = CUser::GetList(($by = "id"), ($order = "desc"), ['LOGIN' => $login])->Fetch();
			if ($existUser) {
				$arResult['status'] = 0;
				$arResult['error'] = 3;
				$arResult['comment'] = 'Пользователь уже существует';
				break;
			}

			$arFields['LOGIN'] = $login;
			$arFields['UF_PARTNER_ID'] = $partnerId;
			$arFields['PASSWORD'] = $password;
			$arFields['CONFIRM_PASSWORD'] = $password;
			$arFields['UF_RATE'] = $rate;
			$arFields['DATE_REGISTER'] = $date;
			$arFields['ACTIVE'] = 'Y';

			$user = new CUser;
			if ($user->Add($arFields)) {
				$arResult['status'] = 1;
			} else {
				$arResult['status'] = 0;
				$arResult['error'] = 1;
				$arResult['comment'] = $user->LAST_ERROR;
			}

			break;
		case 'user_update':
			$existUser = CUser::GetList(($by = "id"), ($order = "desc"), ['UF_PARTNER_ID' => $partnerId])->Fetch();
			if (!$existUser) {
				$arResult['status'] = 0;
				$arResult['error'] = 2;
				$arResult['comment'] = 'Пользователь не найден';
				break;
			}

			$arFields['UF_PARTNER_ID'] = $partnerId;
			if ($password) {
				$arFields['PASSWORD'] = $password;
				$arFields['CONFIRM_PASSWORD'] = $password;
			}
			$arFields['UF_RATE'] = $rate;
			$arFields['TIMESTAMP_X'] = $date;
			$arFields['ACTIVE'] = 'Y';

			$user = new CUser;
			if ($user->Update($userId, $arFields)) {
				$arResult['status'] = 1;
			} else {
				$arResult['status'] = 0;
				$arResult['error'] = 1;
				$arResult['comment'] = $user->LAST_ERROR;
			}
			break;
		default:
			$arResult['status'] = 0;
			$arResult['error'] = 1;
			$arResult['comment'] = 'Неизвестное действие';
			break;
	}
}


echo json_encode($arResult);
die();
?>