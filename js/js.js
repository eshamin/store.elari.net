﻿$(function () {
    var isOldSafari = false;
    if (bowser) {
        var bn = bowser.name;
        var vv = parseFloat(bowser.version);
        if (bn == "Safari" && vv < 6) {
            $("body").addClass("old-safari");
            isOldSafari = true;
        }
    }
    $(".video").click(function (event) {
        if ($(this).hasClass("on")) return false; 
        $(".video").addClass("on");
        var ifrm = $(this).find("iframe:first");
        var src = ifrm.attr("src") + "?autoplay=1";
        ifrm.attr("src", src);
    });
    if (!isOldSafari) {

/*
        $('.intro_slider').bxSlider({
            nextText: '›',
            prevText: '‹',
             maxSlides: 4,
             startSlide: 0

        });

        */
        $('#colors').bxSlider({
            nextText: '›',
            prevText: '‹',
            minSlides: 2,
            maxSlides: 2,
            slideWidth: 52,
            pager: false
        });
    }
    $(".mobile-search").click(function () {
        $("header nav").removeClass("on");
        $("header .search-box").toggleClass("on");
    });
    $(".mobile-humburger").click(function () {
        $("header nav").toggleClass("on");
        $("header .search-box").removeClass("on");
    });
    $(".sidebar .menu-tail").click(function () {
        $(this).parents(".sidebar").toggleClass("on");
    });

    /* counter for shopping cart */

    /* increase count */
    $(".counter .plus").click(function () {
        var val = parseInt($(this).parent().find(".counter-index em").attr("data-count"));
        if (isNaN(val)) {
            $(this).parent().find(".counter-index em").attr("data-count", 1);
            $(this).parent().find(".counter-index em").html(1);
        }
        else {
            $(this).parent().find(".counter-index em").attr("data-count", val + 1);
            $(this).parent().find(".counter-index em").html(val + 1);
        }
    });

    /* decrease count */
    $(".counter a.minus").click(function () {
        var val = parseInt($(this).parent().find(".counter-index em").attr("data-count"));
        if (isNaN(val)) {
            $(this).parent().find(".counter-index em").attr("data-count", 1);
            $(this).parent().find(".counter-index em").html(1);
        }
        else {
            if (val > 1) {
                $(this).parent().find(".counter-index em").attr("data-count", val - 1);
                $(this).parent().find(".counter-index em").html(val - 1);
            }
            if (val <= 1) {
                $(this).parent().find(".counter-index em").attr("data-count", 1);
                $(this).parent().find(".counter-index em").html(1);
            }
        }
    });

    /* recalculate price if counter is changed */
    $(".shopping-cart-table .counter a.plus, .shopping-cart-table .counter a.minus").click(function () {
        var count = parseInt($(this).parent().find(".counter-index em").attr("data-count"));

        var str_per_one = $(this).parent().parent().parent().find(".col-price .price").attr("data-price");
        var per_one = parseFloat(str_per_one);
        var sum = 0;
        if (!isNaN(count) && !isNaN(per_one)) {
            sum = count * per_one;
            sum = sum.toFixed(2);
        }
        $(this).parent().parent().parent().find(".col-amount .price").attr("data-price", sum);
        $(this).parent().parent().parent().find(".col-amount .price").html(formatPrice(sum));
        var total = recalculateTotalPrice();
        $(".shopping-cart-table .amout-payment").attr("data-price", total[0]);
        $(".shopping-cart-table .amout-payment").html(total[1]);
    });

    /* remove item from shopping cart */
    $(".shopping-cart-table .btn-remove").click(function () {
        var obj = $(this).parent().parent();
        obj.fadeOut(500, function () {
            obj.remove();
            var total = recalculateTotalPrice();
            $(".shopping-box .amout-payment").attr("data-price", total[0]);
            $(".shopping-box .amout-payment").html(total[1]);
            if (total[0] == 0) {
                $(".shopping-cart-empty").show();
                $(".shopping-cart-table").hide();
            }
        });
    });
    if (!isOldSafari) {
        var $easyzoom = $('.easyzoom').easyZoom();
        var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');
        $('.thumbnails').on('click', 'a', function (e) {
            var $this = $(this);
            e.preventDefault();
            api1.swap($this.data('standard'), $this.attr('href'));
        });
    } else {
        $(".product-image-large a").removeAttr("href");
        $(".images.thumbnails a").each(function () {
            var href = $(this).attr("href");
            $(this).removeAttr("href");
            $(this).attr("data-large", href);
        });
        $(".images.thumbnails a").click(function () {
            var url = $(this).attr("data-large");
            $(".product-image-large a").removeAttr("href");
            $(".product-image-large img:first").attr("src", url);
        });
    }
});

/* recalculate price 
 * return array [float format, string format] */
function recalculateTotalPrice() {
    var amount = 0;
    if ($(".shopping-cart-table tbody tr").length) {
        var i = 0;
        $(".shopping-cart-table tbody tr").each(function () {
            amount = amount + parseFloat($(this).find(".col-amount .price:first").attr("data-price"));
            i++;
        });
        amount1 = formatPrice(amount);
        $("#shop-cart-amount-price").html(amount1);
        return [amount, amount1];
    } else {
        $("#shop-cart-amount-price").html(0);
        return [0, 0];
    }
}

/* function for money preformating (float) */
function formatPrice(float) {
    var string = float.toString();
    var floatPart = "";
    var intPart = "";
    var dot = string.indexOf(".");
    if (dot != -1) {
        if (string.indexOf(".00") == -1) {
            floatPart = string.substr(dot, string.length - 1);
        }
        intPart = string.substr(0, dot);
        intPart = formatMoney(intPart, "", "", " ", " ");
    } else {
        intPart = formatMoney(string, "", "", " ", " ");
    }
    return intPart + floatPart;
}

/* function for money format (integer) */
function formatMoney(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}