<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Контакты");?><section class="text">
<div class="wrapper">
	<h1>Контакты</h1>
	<p>
 <b>Телефон:</b> 8 800 500 56 00, +7 (495) 120 56 00
	</p>
	<p>
 <b>Адрес офиса:</b> 119331, Москва, Проспект Вернадского, д. 29, БЦ "Лето", 1 этаж, офис 1/29 (стеклянный магазин напротив турникетов)
	</p>
	<p>
 <b>Режим работы офиса:</b>&nbsp;понедельник-пятница с 9.30 до 18.30.<br>
</p>
<p>
 <b>Режим работы офиса в новогодние праздники:</b> 30-31 декабря, 1,2,5,6,7 января - выходные дни.<br>
	</p>
	<p>
		 Для СМИ и блоггеров –&nbsp;<a href="mailto:pr@elari.net">pr@elari.net</a>
	</p>
	<p>
		 Отдел продаж/предложения о сотрудничестве –&nbsp;<a href="mailto:sales@elari.net">sales@elari.net</a>
	</p>
	<p>
		 Общие вопросы –&nbsp;<a href="mailto:info@elari.net">info@elari.net</a>
	</p>
	<p>
	</p>
	 <iframe src="https://yandex.ru/map-widget/v1/-/CBuWQWeXhA" width="100%" height="400" frameborder="1" allowfullscreen="true"></iframe>
	<p>
	</p>
	<p>
 <b>Сервисный центр:</b> г. Москва, ул. Наметкина, д.3
	</p>
	<p>
 <b>Телефон:</b> +7 (495) 120 56 00 ; 8 (800) 500 56 00
	</p>
	<p>
 <b>Режим работы сервисного центра:</b> 9:30 - 18:15 понедельник-пятница <br>
 <br>
 <b>Сервисное обслуживание/техподдержка</b>:&nbsp;<a href="mailto:support@elari.net">support@elari.net</a>
	</p>
	<p>
	</p>
	<p>
 <b>Юридическая информация:</b> ООО «Соним»
	</p>
	<p>
		 ОГРН: 1117746941476
	</p>
	<p>
		 Юридический адрес: 119331, г. Москва, проспект Вернадского, д. 29, помещение&nbsp; I, этаж 1, комната 1Д
	</p>
</div>
 </section> <br><? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>