<?
IncludeModuleLangFile(__FILE__);
$moduleId = "dsaagency.marschroute";

// if (CModule::IncludeModule('sale')) die();

// $dbResult = CSaleDeliveryHandler::GetList(
	// array(
		// 'SORT' => 'ASC', 
		// 'ID' => 'ASC'
	// ), 		
	// array(
		// 'ACTIVE' => 'Y'
	// )
// );
// while ($arResult = $dbResult->GetNext()) {
	// $groups[$arResult["ID"]] = $arResult["NAME"]." [".$arResult["ID"]."]";
// }
$aTabs = array(
    array(
        'DIV' => 'settings',
        'TAB' => getMessage('MAIN_MARSCHROUTE_SETTINGS'),
        'OPTIONS' => array(
            array(
				'API_KEY',
                getMessage('MAIN_MARSCHROUTE_API_KEY'),
                null,
                array('text', 52),
            ),
            array(
				'API_URL',
                getMessage('MAIN_MARSCHROUTE_API_URL'),
                null,
                array('text', 52),
            ),
            // getMessage('MAIN_MARSCHROUTE_PAYMENT_TYPE'),
            // array(
				// 'PAYMENT_TYPE_CASH',
                // getMessage('MAIN_MARSCHROUTE_PAYMENT_TYPE_CASH'),
                // null,
                // Array("multiselectbox", $groups),
            // ),
            // array(
				// 'PAYMENT_TYPE_PREPAY',
                // getMessage('MAIN_MARSCHROUTE_PAYMENT_TYPE_PREPAY'),
                // null,
                // Array("multiselectbox", $groups),
            // )
        )
    )
);

if ($_SERVER['REQUEST_METHOD'] == 'POST' && strlen($_REQUEST['save']) > 0 && check_bitrix_sessid())
{
    foreach ($aTabs as $aTab)
    {
        __AdmSettingsSaveOptions($moduleId, $aTab['OPTIONS']);
    }

    LocalRedirect($APPLICATION->GetCurPage() . '?lang=' . LANGUAGE_ID . '&mid_menu=1&mid=' . urlencode($moduleId) .
        '&tabControl_active_tab=' . urlencode($_REQUEST['tabControl_active_tab']));
}
$tabControl = new CAdminTabControl('tabControl', $aTabs);
?>
<form method='post' action='' name='bootstrap'>
	<? $tabControl->Begin();

	foreach ($aTabs as $aTab)
	{
		$tabControl->BeginNextTab();
		__AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
	}

	$tabControl->Buttons(array('btnApply' => false, 'btnCancel' => false, 'btnSaveAndAdd' => false)); ?>

	<?= bitrix_sessid_post(); ?>
	<? $tabControl->End(); ?>
</form>