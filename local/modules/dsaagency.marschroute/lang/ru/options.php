<?
$MESS["MAIN_MARSCHROUTE_SETTINGS"] = "Настройки";
$MESS["MAIN_MARSCHROUTE_API_KEY"] = "API-ключ сервиса marschroute:";
$MESS["MAIN_MARSCHROUTE_API_URL"] = "Адрес веб-сервиса (формат - https://example.com/):";
$MESS["MAIN_MARSCHROUTE_PAYMENT_TYPE"] = "Соответствие типов оплаты на сайте и в Marschroute";
$MESS["MAIN_MARSCHROUTE_PAYMENT_TYPE_CASH"] = "Типы оплаты на сайте, соответствующие наличному расчёту для Marschroute:";
$MESS["MAIN_MARSCHROUTE_PAYMENT_TYPE_PREPAY"] = "Типы оплаты на сайте, соответствующие предоплате для Marschroute:";
?>