<?php
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/statistic/classes/general/statistic.php");
define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
use \Bitrix\Main\Web\HttpClient;
use Bitrix\Sale;

class cMainMarschrouteIntegrationFunctions {
	function apiKey () {
		return COption::GetOptionString("dsaagency.marschroute", "API_KEY");
	}
	function urlApi () {
		return COption::GetOptionString("dsaagency.marschroute", "API_URL");
	}
	function httpClientOptions () {
		return array(
			"waitResponse" => true,
			"socketTimeout" => 30,
			"streamTimeout" => 60,
			"version" => HttpClient::HTTP_1_1
		);
	}
	function getIdKladrCity ($city) {
		//Запрос города
		$httpClient = new HttpClient(self::httpClientOptions());
		$httpClient->query(HttpClient::HTTP_GET, "http://kladr-api.ru/api.php?contentType=city&query=" . $city);
		$result = json_decode( $httpClient->getResult(), true );
		foreach ($result["result"] as $item) {
			if ($item["name"] == $city) return $item["id"]/100;
		}
		return false;
	}
	function getDelivery($arParams) {
		$arError = array();
		if (!isset($arParams["CITY"]) || !$arParams["CITY"]) {
			return array(
				"result" => false,
				"message" => "Город не указан"
			);
		} elseif (!$city = self::getIdKladrCity($arParams["CITY"])) {
			return array(
				"result" => false,
				"message" => "Город не найден в базе КЛАДР (" . $arParams["CITY"] . ")"
			);
		} else {
			$url = self::urlApi() . self::apiKey() . "/delivery_city?kladr=" . $city;
		}
		
		if (isset($arParams["WEIGHT"]) && $arParams["WEIGHT"]) {
			$url .= "&weight=" . $arParams["WEIGHT"];
		} else {
			$url .= "&weight=1000";
		}
		//Возможно тут следует вынести в настройки типы оплаты и сверять с ними
		if (isset($arParams["PAID"]) && $arParams["PAID"]) {
			$url .= "&payment_type=2";
		} else {
			$url .= "&payment_type=1";
		}
		if (isset($arParams["PRICE"]) && $arParams["PRICE"]) {
			$url .= "&order_sum=" . $arParams["PRICE"];
		}
		
		$httpClient = new HttpClient($httpClientOptions);
		$httpClient->query(HttpClient::HTTP_GET, $url);
		$result = json_decode( $httpClient->getResult(), true );
		global $USER;
		//Ошибка запроса
		if (!$result['success']) {
			return array(
				"result" => false,
				"message" => "Ошибка [code]:".$result['code']."<br>".$result['comment'] . "<br>" . ($USER->IsAdmin())?json_encode($result):"" . "<br>"
			);
			AddMessage2Log("Ошибка [code]:".$result['code']."\n".$result['comment'] . "\n" .json_encode( $result['errors']));
		}
		//Нет доступных способов доставки
		if (!count($result["data"][0]["delivery_types"])) {
			return array(
				"result" => false,
				"message" => "Нет доступных способов доставки"
			);
		}
		$deliveryTypes = $result["data"][0]["delivery_types"];
		unset($result["data"][0]["delivery_types"]);
		$deliveries = array();
		foreach ($deliveryTypes as $delivery) {
			$deliveries[$delivery["delivery_code"]]["name"] = $delivery["name"];
			$deliveries[$delivery["delivery_code"]]["data"][] = $delivery;
		}
		
		return array(
			"result" => true,
			"info" => $result["data"][0],
			"delivery_types" => $deliveries
		);
	}
	function sendOrder ($arParams) {
		if (!isset($arParams["ID"]) || !$arParams["ID"] || 
			!isset($arParams["DELIVERY"]) || !$arParams["DELIVERY"] || 
			!isset($arParams["DELIVERY_DATE"]) || !$arParams["DELIVERY_DATE"] || 
			!CModule::IncludeModule("Sale")) 
			return false;

		$order = Sale\Order::load($arParams["ID"]);
		$propertyCollection = $order->getPropertyCollection();
		$arProperties = $propertyCollection->getArray();
		foreach ($arProperties["properties"] as $arProp) {
			if ($arProp["CODE"]) {
				$arOrderProp[$arProp["CODE"]] = $arProp["VALUE"][0];
			}
		}
		$payment = $order->getPaymentSystemId();
		$payment = $payment[0];
		$name = explode(" ", $arOrderProp["NAME"]);
		$city = $arOrderProp["CITY"]?array("CITY_NAME" => $arOrderProp["CITY"]):CSaleLocation::GetByID($arOrderProp["LOCATION"]);
		$city = $city["CITY_NAME"];
		if (!$city) {
			return array(
				"result" => false,
				"message" => "Город не указан"
			);
		} 
		if (!$city = self::getIdKladrCity($city)) {
			return array(
				"result" => false,
				"message" => "Город не найден в базе КЛАДР (" . $arParams["CITY"] . ")"
			);
		}
		$data = array(
			"order" => array(
				"id" => $arParams["ID"],
				"delivery_sum" => $order->getDeliveryPrice(),
				"payment_type" => ($payment == 1 || $payment == 20)?1:2,//1 - наличные, 2 - предоплаченный заказ
				"send_date" => $arParams["DELIVERY_DATE"],
				"weight" => $order->getField("ORDER_WEIGHT")?$order->getField("ORDER_WEIGHT"):1000,
				"city_id" => $city,
				"place_id" => $arParams["DELIVERY"],
				"index" => $arOrderProp["ZIP"], 
				"street" => $arOrderProp["STREET"], 
				"building_1" => $arOrderProp["HOUSE"],
				"room" => $arOrderProp["APARTMENTS_NUMBER"],
			),
			"customer" => array(
				"id" => $order->getUserId(),//id покупателя
				"firstname" => ($name[0])?$name[0]:"Иван",//Имя
				"middlename" => ($name[2])?$name[1]:"",//Отчество
				"lastname" => ($name[2])?$name[2]:$name[1],//Фамилия
				"phone" => $arOrderProp["PERSONAL_PHONE"],
			)
		);
		
		$basket = $order->getBasket();
		foreach ($basket as $basketItem) {
			$data["items"][] = array(
				"item_id" => $basketItem->getProductId(), //Код товара ИМ
				"name" => $basketItem->getField('NAME'), //Название товара
				"nds" => 0, //НДС
				"price" => $basketItem->getPrice(), //Цена товара
				"quantity" => $basketItem->getQuantity() //Количество
			);
		}
		$httpClient = new HttpClient(self::httpClientOptions());
		$httpClient->query(HttpClient::HTTP_PUT, self::urlApi() . self::apiKey() . "/order?check=1", json_encode($data));
		$result = json_decode( $httpClient->getResult(), true );
		global $USER;
		if (!$result['success']) {
			return array(
				"result" => false,
				"message" => "Ошибка [code]:".$result['code']."<br>".$result['comment'],
				"data" => $result
			);
			AddMessage2Log("Ошибка [code]:".$result['code']."\n".$result['comment'] . "\n" .json_encode( $result['errors']));
		}
		if (isset($result['id'])) {
			$order->setField('STATUS_ID', 'MR');
			$order->save();
			return array(
				"result" => true,
				"message" => "Заказ успешно отправлен в Marschroute",
				"data" => $result
			);
		}
		return array(
			"result" => false,
			"message" => "Неизвестная ошибка!",
			"data" => $result
		);
	}
}
