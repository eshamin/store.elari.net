<?
Class dsaagency_marschroute extends CModule
{
	var $MODULE_ID = "dsaagency.marschroute";
	var $MODULE_NAME = "Интеграция с сервисом доставки Marschroute";
	var $MODULE_DESCRIPTION = "После установки вы сможете передавать заказы в службу достаки Marschroute";

	function DoInstall(){
		$this->InstallFiles();
		RegisterModule("dsaagency.marschroute");
	}

	function DoUninstall(){
		UnRegisterModule("dsaagency.marschroute");
	}
	
	function InstallFiles() {
		$path = str_replace("\\", "/", __FILE__);
		$path = substr($path, 0, strlen($path) - strlen("/index.php"));

		CopyDirFiles($path."/admin", $_SERVER["DOCUMENT_ROOT"]."/bitrix/admin", true);
	}

}
?>