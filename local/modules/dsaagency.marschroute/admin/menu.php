<?
IncludeModuleLangFile(__FILE__);

$aMenu = array(
	"parent_menu" => "global_menu_store", 
	"sort"        => 112,
	"url"         => "/bitrix/admin/dsaagency_marschroute_index.php",	
	"more_url"    => array(
		"/bitrix/admin/dsaagency_marschroute_step2.php"
	),
	"text"        => "Интеграция с Marschroute",   
	"icon"        => "form_menu_icon",
	"page_icon"   => "form_page_icon",  
	"module_id"   => "dsaagency.marschroute"
);
return $aMenu;
?>