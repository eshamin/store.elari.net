<?
if ((!isset($_GET["ID"]) || !$_GET["ID"]) && $_REQUEST["action"] != "send") header('Location: /bitrix/admin/dsaagency_marschroute_index.php');
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); ?>

<?
use Bitrix\Sale;
if (!CModule::IncludeModule("dsaagency.marschroute")) die();
if (!CModule::IncludeModule("Sale")) return die();

// echo "<pre>", print_r($_REQUEST), "</pre>";

if ($_REQUEST["action"] == "send") {
	foreach ($_REQUEST["orders"] as $key => $arOrder) {
		$_REQUEST["ID"][] = $arOrder["id"];
		if (!isset($arOrder["status"]) && isset($arOrder["delivery"]) && $arOrder["delivery-date"]) {
			$result = cMainMarschrouteIntegrationFunctions::sendOrder(array(
				"ID" => $arOrder["id"],
				"DELIVERY" => $arOrder["delivery"],
				"DELIVERY_DATE" => $arOrder["delivery-date"]
			));
			if ($result["result"]) {
				$_REQUEST["orders"][$key]["status"] = "good";
				$_REQUEST["orders"][$key]["message"] = $result["message"];
			} else {
				$_REQUEST["orders"][$key]["status"] = "bad";
				$_REQUEST["orders"][$key]["message"] = $result["message"];
			}
			// echo "<pre>", print_r($result), "</pre>";
		}
	}
}
		
		
$APPLICATION->SetTitle("Выбор службы доставки");
$APPLICATION->SetAdditionalCSS("https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
$APPLICATION->AddHeadScript('https://code.jquery.com/jquery-3.2.1.slim.min.js"');
$APPLICATION->AddHeadScript('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js');
$APPLICATION->AddHeadScript('https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"');?>

<form class="container-fluid" method="POST" action="/bitrix/admin/dsaagency_marschroute_step2.php">
	<input type="hidden" name="action" value="send">
	<table class="table">
		<thead>
			<tr>
				<th scope="col">#</th>
				<th scope="col">Имя покупателя</th>
				<th scope="col">Информация о заказе</th>
				<th scope="col">Выбор службы доставки</th>
				<th scope="col">Информация о выбранном типе доставки</th>
			</tr>
		</thead>
		<tbody>
			<?foreach ($_REQUEST["ID"] as $id) :
				//Запрос данных заказа
				$order = Sale\Order::load($id);
				$propertyCollection = $order->getPropertyCollection();
				$arProperties = $propertyCollection->getArray();
				foreach ($arProperties["properties"] as $arProp) {
					if ($arProp["CODE"]) {
						$arOrderProp[$arProp["CODE"]] = $arProp["VALUE"][0];
					}
				}
				$payment = $order->getPaymentSystemId();
				$payment = $payment[0];
				?>
				<tr class="order-row">
					<input type="hidden" name="orders[<?=$id;?>][id]" value="<?=$id;?>">
					<th scope="row">
						<a href="/bitrix/admin/sale_order_view.php?ID=<?=$id;?>"><?=$id;?></a>
					</th>
					<td><?=$arOrderProp["NAME"];?></td>
					<?
					//Если заказ уже отправлен в Marschroute
					//ИЛИ
					//Если заказ отправлен в Marschroute с ошибкой или на этапе выбора доставки произошла ошибка
					if (isset($_REQUEST["orders"][$id]["status"]) && $_REQUEST["orders"][$id]["status"]) {?>
						<input type="hidden" name="orders[<?=$id;?>][status]" value="<?=$_REQUEST["orders"][$id]["status"];?>">
						<input type="hidden" name="orders[<?=$id;?>][message]" value="<?=$_REQUEST["orders"][$id]["message"];?>">
						<td colspan="3">
							<b><?=$_REQUEST["orders"][$id]["message"];?></b>
						</td>
						<?
						continue;
					}
					//Если в заказе не заполнено одно из обязателных полей
					$checkFieldsMessage = "";
					$name = explode(" ", $arOrderProp["NAME"]);
					if (count($name) < 2) {
						$checkFieldsMessage .= "Не заполнено полностью ФИО пользователя. Желательный порядок - ИОФ.<br>";
					}
					if ($order->getDeliveryPrice() === null) {
						$checkFieldsMessage .= "Не заполнено поле 'Сумма доставки покупателю'<br>";
					}
					if (!$arOrderProp['ZIP']) {
						$checkFieldsMessage .= "Не заполнено поле 'Индекс'<br>";
					}
					if (!$arOrderProp['STREET']) {
						$checkFieldsMessage .= "Не заполнено поле 'Улица'<br>";
					}
					if (!$arOrderProp['HOUSE']) {
						$checkFieldsMessage .= "Не заполнено поле 'Дом'<br>";
					}
					if ($order->getField("STATUS_ID") == "MR") {
						$checkFieldsMessage .= "Заказ уже передан в Marschroute";
					}
					if ($checkFieldsMessage) {?>
						<td colspan="3">
							<b><?=$checkFieldsMessage;?></b>
						</td>
						<?
						continue;
					}
					
					$city = $arOrderProp["CITY"]?array("CITY_NAME" => $arOrderProp["CITY"]):CSaleLocation::GetByID($arOrderProp["LOCATION"]);
					$city = $city["CITY_NAME"];
					$deliveries = cMainMarschrouteIntegrationFunctions::getDelivery(array(
						"CITY" => $city,
						"WEIGHT" => $order->getField("ORDER_WEIGHT"),
						"PAID" => $payment,
						"PRICE" => $order->getPrice(),
					));
					//Если получение типов доставки звершилось ошибкой (программная ошибка, нет доступных типов доставки, не указан город, город не найден в КЛАДР)
					if (!$deliveries["result"]) {?>
						<td colspan="3">
							<b><?=$deliveries["message"];?></b>
						</td>
						<?
						continue;
					}?>
					<?
					//Если в заказе не заполнено одно из желательных полей
					$checkFieldsMessage = "<br>";
					if (!$arOrderProp['APARTMENTS_NUMBER']) {
						$checkFieldsMessage .= "Замечание: не заполнено поле 'Квартира/Помещение'<br>";
					}
					if (count($name) < 3) {
						$checkFieldsMessage .= "Замечание: не заполнено полностью ФИО пользователя. Будет отправлено по шаблону ИФ.<br>";
					}
					?>
					
					<td>
						<b>Город в заказе:</b> <?=$city;?><br>
						<b>Город в Marschroute:</b> <?=$deliveries["info"]["name"];?><br>
						<b>Город (КЛАДР):</b> <?=$deliveries["info"]["kladr"];?><br>
						<b>Вес:</b> <?=$deliveries["info"]["weight"];?>
						<?=$checkFieldsMessage;?>
					</td>
					<td>
						<select id="delivery-<?=$id;?>" class="d-none custom-select delivery-date" name="orders[<?=$id;?>][delivery-date]">
							<option value="0"></option>
							<?foreach ($deliveries["delivery_types"] as $type):?>
								<?foreach ($type["data"] as $delivery):?>
									<?
									$selected = "";
									if (isset($_REQUEST["orders"][$id]["delivery"]) && $_REQUEST["orders"][$id]["delivery"] == $delivery["place_id"]) {
										$selected = "selected";
									}?>
									<option <?=$selected;?> data-delivery="<?=$delivery["place_id"];?>" value="<?=$delivery["delivery_date"];?>">
										<?=$delivery["delivery_date"];?>
									</option>
								<?endforeach;?>
							<?endforeach;?>
						</select>
						<select id="delivery-<?=$id;?>" class="custom-select delivery" name="orders[<?=$id;?>][delivery]">
							<option value="0">Варианты доставки...</option>
							<?foreach ($deliveries["delivery_types"] as $type):?>
								<? if (count($type["data"]) > 1) :?>
									<optgroup label="<?=$type["name"];?>">
										<?foreach ($type["data"] as $delivery):?>
											<?
											$selected = "";
											if (isset($_REQUEST["orders"][$id]["delivery"]) && $_REQUEST["orders"][$id]["delivery"] == $delivery["place_id"]) {
												$selected = "selected";
											}?>
											<option <?=$selected;?> value="<?=$delivery["place_id"];?>"><?=$delivery["address"];?></option>
										<?endforeach;?>
									</optgroup>
								<? else: ?>
									<?
									$selected = "";
									if (isset($_REQUEST["orders"][$id]["delivery"]) && $_REQUEST["orders"][$id]["delivery"] == $type["data"][0]["place_id"]) {
										$selected = "selected";
									}?>
									<option <?=$selected;?> value="<?=$type["data"][0]["place_id"];?>"><?=$type["data"][0]["name"];?></option>
								<? endif;?>
							<?endforeach;?>
						</select>
					</td>
					<td>
						<?foreach ($deliveries["delivery_types"] as $type):?>
							<?foreach ($type["data"] as $delivery):?>
								<div class="delivery-description d-none" data-delivery="<?=$delivery["place_id"];?>">
									<b>Цена:</b> <?=$delivery["cost"];?><br>
									<b>Способы оплаты:</b> 
									<?if (count($delivery["payment_types"]) > 1) {
										echo "Наличныме или предоплата";
									} elseif ($delivery["payment_types"][0] == 1) {
										echo "Наличныме";
									} elseif ($delivery["payment_types"][0] == 2) {
										"Предоплата";
									}?>
									<br>
									<b>Дата доставки:</b> <?=$delivery["delivery_date"];?>
								</div>
							<?endforeach;?>
						<?endforeach;?>
					</td>
				</tr>
			<? endforeach;?>
		</tbody>
	</table>
	<a href="/bitrix/admin/dsaagency_marschroute_index.php" class="adm-btn">К списку заказов</a>
	<input type="submit" class="float-right adm-btn adm-btn-save" value="Отправить в Marschroute">
</form>
<script>
	$(".delivery").change(function(){
		$('.delivery-date option[data-delivery=' + $(this).val() + ']').prop('selected', true);
		$(this).parents(".order-row").find(".delivery-description").addClass("d-none")
		$(this).parents(".order-row").find(".delivery-description[data-delivery=" + $(this).val() + "]").removeClass("d-none");
	});
</script>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>