<?
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_before.php");
use Bitrix\Sale;

$sTableID = "list_new_orders";
$oSort = new CAdminSorting($sTableID, "ID", "desc"); // объект сортировки
$lAdmin = new CAdminList($sTableID, $oSort); // основной объект списка

if (!CModule::IncludeModule("Sale")) break;
if (!$by || !$order) {
	$by = "ID";
	$order = "ASC";
}
$res = Sale\Order::getList(array(
	"order" => array($by => $order)
));


// аналогично CDBResult инициализируем постраничную навигацию.
$rsData = new CDBResult($res);
$rsData->NavStart();
// отправим вывод переключателя страниц в основной объект $lAdmin
$lAdmin->NavText($rsData->GetNavPrint("Заказы"));

$lAdmin->AddHeaders(array(
	array(  
		"id"=>"ID",
		"content"=>"ID",
		"sort"=>"ID",
		"default"=>true,
	),
	array(  
		"id"=>"DATE",
		"content"=>"Дата заказа",
		"sort"=>"DATE_INSERT",
		"default"=>true,
	),
	array(  
		"id"=>"PRICE",
		"content"=>"Цена",
		"sort"=>"PRICE",
		"default"=>true,
	),
	array(  
		"id"=>"PRICE_DELIVERY",
		"content"=>"Цена доставки",
		"sort"=>"PRICE_DELIVERY",
		"default"=>true,
	),
	array(  
		"id"=>"DELIVERY_ID",
		"content"=>"Тип доставки",
		"sort"=>"DELIVERY_ID",
		"default"=>true,
	),
	array(  
		"id"=>"UPLOAD_TO_MARSCHROUTE",
		"content"=>"Отправить в Marschroute",
		"sort"=>"UPLOAD_TO_MARSCHROUTE",
		"default"=>true,
	),
	
));

while($arRes = $rsData->NavNext(true, "f_")) {
	// echo "<pre>", print_r($arRes), "</pre>";
	$row = &$lAdmin->AddRow($arRes["ID"], $arRes); 
	$row->AddViewField("ID", '<a href="sale_order_view.php?ID='.$arRes["ID"].'&lang='.LANG.'">'.$arRes["ID"].'</a>');
	$row->AddViewField("DATE", $arRes["DATE_INSERT"]);
	$row->AddViewField("PRICE", $arRes["PRICE"]);
	$row->AddViewField("PRICE_DELIVERY", $arRes["PRICE_DELIVERY"]);
	$row->AddViewField("DELIVERY_ID", $arRes["DELIVERY_ID"]);
	$row->AddViewField("UPLOAD_TO_MARSCHROUTE", "<a class='adm-btn adm-btn-save'>Отправить</a>");
}

// резюме таблицы
$lAdmin->AddFooter(
	array(
		array("title"=>GetMessage("MAIN_ADMIN_LIST_SELECTED"), "value"=>$rsData->SelectedRowsCount()), // кол-во элементов
		array("counter"=>true, "title"=>GetMessage("MAIN_ADMIN_LIST_CHECKED"), "value"=>"0"), // счетчик выбранных элементов
	)
);

// групповые действия
$lAdmin->AddGroupActionTable(Array(
	"upload" => "Загрузить"
));

$lAdmin->CheckListMode();
// установим заголовок страницы
$APPLICATION->SetTitle("Выбор заказов для отправки в Marschroute");?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_admin_after.php"); ?>
<?
// выведем таблицу списка элементов
$lAdmin->DisplayList();
?>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_admin.php");?>