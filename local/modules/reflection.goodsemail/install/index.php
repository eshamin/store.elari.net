<?

global $MESS;
$strPath2Lang = str_replace("\\", "/", __FILE__);
$strPath2Lang = substr($strPath2Lang, 0, strlen($strPath2Lang) - 18);
include(GetLangFileName($strPath2Lang . "/lang/", "/install/index.php"));

Class reflection_goodsemail extends CModule {

    var $MODULE_ID = "reflection.goodsemail";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $MODULE_CSS;
    var $PARTNER_NAME;
    var $PARTNER_URI;

    function __construct() {
        $arModuleVersion = array();

        $path = str_replace("\\", "/", __FILE__);
        $path = substr($path, 0, strlen($path) - strlen("/index.php"));
        include($path . "/version.php");

        if (is_array($arModuleVersion) && array_key_exists("VERSION", $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        }

        $this->MODULE_NAME = GetMessage('REFLEKTO_MODULE_NAME_GOODSEMAIL');
        $this->MODULE_DESCRIPTION = GetMessage('REFLEKTO_MODULE_DESCRIPTION_GOODSEMAIL');
        $this->PARTNER_NAME = GetMessage('ECHO_PARTNER_NAME');
        $this->PARTNER_URI = "http://www.reflekto.ru";
    }

    function InstallFiles($arParams = array()) {
        // Иконки, темы
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/panel", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/panel", true, true);
        // Изображения
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/images", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/images/".$this->MODULE_ID, true, true);
        // JS файлы
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/js", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/js/".$this->MODULE_ID, true, true);
        // Файлы вызова административных скриптов
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/".$this->MODULE_ID."/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);

        return true;
    }

    // Установка инфоблоков
    function InstallIblock() {

    }

    // Установка обработчика зависимости событий
    function InstallDependences() {
        RegisterModuleDependences("sale", "OnSaleStatusOrder", $this->MODULE_ID, "reflection\clsEventsHandler", "OnStatusUpdateHandler");
        RegisterModuleDependences("sale", "OnOrderNewSendEmail", $this->MODULE_ID, "reflection\clsEventsHandler", "OnOrderNewSendEmailHandler");


    }


    // Удаление обработчика зависимости событий
    function UnInstallDependences() {
        UnRegisterModuleDependences("sale", "OnSaleStatusOrder", $this->MODULE_ID, "reflection\clsEventsHandler", "OnStatusUpdateHandler");
        UnRegisterModuleDependences("sale", "OnOrderNewSendEmail", $this->MODULE_ID, "reflection\clsEventsHandler", "OnOrderNewSendEmailHandler");

    }

    // Удаление инфоблоков
    function UnInstallIblock() {

    }

    function UnInstallFiles() {
        // Удаляем иконки, темы
        DeleteDirFilesEx("/bitrix/panel/".$this->MODULE_ID);
        DeleteDirFilesEx("/bitrix/js/".$this->MODULE_ID);
        DeleteDirFilesEx("/bitrix/images/".$this->MODULE_ID);
        return true;
    }

    function InstallAgents() {
    }

    function UnInstallAgents() {

    }
    
    // Функция заглушки для демо-периода
    function InstallDB() {
        
    }

    function InstallMailEvents() {
        // Добавляем новый тип почтового события
        $et = new CEventType;
        $et->Add(array(
            "LID"           => "ru",
            "EVENT_NAME"    => "REFLEKTO_ORDER_LIST",
            "NAME"          => GetMessage("REFLEKTO_USER_NOTIFICATION"),
            "DESCRIPTION"   => "#REPLACE_BY_MODULE# - ".GetMessage("REFLEKTO_MODULE_NOTIFICATION")
        ));

        $rsSites = CSite::GetList($by="sort", $order="desc", array());
        $arr_sites = array();
        while ($arSite = $rsSites->Fetch()) {
            $arr_sites[] = $arSite['ID'];
        }

        // Добавляем новый шаблон
        $arr["ACTIVE"] = "Y";
        $arr["EVENT_NAME"] = "REFLEKTO_ORDER_LIST";
        $arr["LID"] = $arr_sites;
        $arr["EMAIL_FROM"] = "#DEFAULT_EMAIL_FROM#";
        $arr["EMAIL_TO"] = "#EMAIL_TO#";
        $arr["BCC"] = "#BCC#";
        $arr["SUBJECT"] = GetMessage("YOUR_ORDER")." #ORDER_ID#";
        $arr["BODY_TYPE"] = "html";
        $arr["MESSAGE"] = "#REPLACE_BY_MODULE#";

        $emess = new CEventMessage;
        $emess->Add($arr);
    }

    function UnInstallMailEvents() {
        global $DB;

        // Удаляем шаблон сообщения
        $arFilter = Array(
            "TYPE_ID"       => "REFLEKTO_ORDER_LIST"
        );
        $rsMess = CEventMessage::GetList($by="site_id", $order="desc", $arFilter);
        while($ar_fields = $rsMess->GetNext()) {

            $emessage = new CEventMessage;
            $DB->StartTransaction();
            if(!$emessage->Delete(intval($ar_fields['ID'])))
            {
                $DB->Rollback();
                $strError.=GetMessage("DELETE_ERROR");
            }
            else $DB->Commit();
        }

        // Удаляем тип почтовго сообщения
        $et = new CEventType;
        $et->Delete("REFLEKTO_ORDER_LIST");
    }

    function DoInstall() {
        global $DOCUMENT_ROOT, $APPLICATION;

        if (!IsModuleInstalled("sale")) {
            echo CAdminMessage::ShowMessage(GetMessage("REFLEKTO_NEEDS_SALE_MODULE"));
            return false;
        }

		RegisterModule($this->MODULE_ID);
        $this->InstallDB();
        $this->InstallFiles();
        $this->InstallDependences();
        $this->InstallDependences();
        $this->InstallMailEvents();


        $APPLICATION->IncludeAdminFile(GetMessage('ECHO_IBLOCK_INSTALL_MODULE')." ".$this->MODULE_ID, $DOCUMENT_ROOT . "/bitrix/modules/".$this->MODULE_ID."/install/step.php");
    }

    function DoUninstall() {
        global $DOCUMENT_ROOT, $APPLICATION;
        $this->UnInstallFiles();
        $this->UnInstallDependences();
        $this->UnInstallMailEvents();

        UnRegisterModule($this->MODULE_ID);
        $APPLICATION->IncludeAdminFile(GetMessage('ECHO_IBLOCK_DEINSTALL_MODULE')." ".$this->MODULE_ID, $DOCUMENT_ROOT . "/bitrix/modules/".$this->MODULE_ID."/install/unstep.php");
    }

}

?>