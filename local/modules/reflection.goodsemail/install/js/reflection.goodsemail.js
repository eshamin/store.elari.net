var ajax_pending_XMLHttpRequest = "";

$(document).ready(function() {

    var optController = new OptionsController();

    optController.setListeners();
    optController.refreshOptions($('#select_reflekto_goodsemail_template').val());

});

// Класс
OptionsController = function(arParams) {
    this.test = true; // Локальная переменная
};

// получаем список опций шаблона
OptionsController.prototype.setListeners = function() {
    var object_link = this;
    $('#select_reflekto_goodsemail_template').on('change', function() {
        var cur_option = $(this).val();
        object_link.refreshOptions(cur_option);
    });
}

OptionsController.prototype.refreshOptions = function(template_id) {
    // Вызываем AJAX запрос на поиск дополнительных опций шаблона
    var params = new Object();
    params['template'] = template_id;

    // Если уже обрабатывается запрос - обрубить его
    if(getAjaxState()) {
        ajax_pending_XMLHttpRequest.abort();
    }

    $.ajax({
        type: "POST",
        url: "/bitrix/admin/reflection.check_template_options.php",
        data: params,
        beforeSend: function ( xhr ) {
            ajax_pending_XMLHttpRequest = xhr;
        }
    }).done(function( msg ) {
        $('#tempalte_options').html(msg);

    });

}

function getAjaxState() {
    if(ajax_pending_XMLHttpRequest.readyState > 0 && ajax_pending_XMLHttpRequest.readyState < 4) return true;
    return false;
}