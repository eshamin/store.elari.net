<?
global $MESS;

include(GetLangFileName($GLOBALS["DOCUMENT_ROOT"] . "/bitrix/modules/reflection.goodsemail/lang/", "/options.php"));
$module_id = "reflection.goodsemail";
CJSCore::Init(array('jquery'));
$APPLICATION->SetAdditionalCSS("/bitrix/panel/".$module_id."/".$module_id.".css");
$APPLICATION->AddHeadScript("/bitrix/js/".$module_id."/".$module_id.".js");



CModule::IncludeModule($module_id);
CModule::IncludeModule("sale");

$aTabs = array(
    array("DIV" => "edit1",
        "TAB" => GetMessage("TAB_MAIN"),
        "TITLE" => GetMessage("TAB_MAIN")
    ),
    array("DIV" => "edit2",
        "TAB" => GetMessage("TAB_ACCESS"),
        "TITLE" => GetMessage("TAB_ACCESS")
    )
);

$tabControl = new CAdminTabControl("tabControl", $aTabs);

$MOD_RIGHT = $APPLICATION->GetGroupRight($module_id);

if ($MOD_RIGHT >= "R")
    $arAllOptions =
            Array(
                Array("reflekto_goodsemail_saletype[]", GetMessage("REFLEKTO_SALETYPE_EVENT"), "", Array("select", 45)),
                Array("reflekto_goodsemail_text", GetMessage("REFLEKTO_TEXT"), "", Array("textarea", 5)),
                Array("reflekto_goodsemail_logo", GetMessage("REFLEKTO_LOGO"), "", Array("file", 45)),
                Array("reflekto_goodsemail_phone", GetMessage("REFLEKTO_PHONE"), "", Array("text", 45)),
                Array("reflekto_goodsemail_email", GetMessage("REFLEKTO_EMAIL"), "", Array("text", 45)),
                Array("reflekto_goodsemail_orderprop_col_left[]", GetMessage("REFLEKTO_ORDERPROP_COL_LEFT"), "", Array("select", 45)),
                Array("reflekto_goodsemail_orderprop_center[]", GetMessage("REFLEKTO_ORDERPROP_CENTER"), "", Array("select", 45)),
                Array("reflekto_goodsemail_template", GetMessage("REFLEKTO_TEMPLATE"), "", Array("select_template", 45))
                
    );

// Выбираем типы статусов заказов
$arrSaleStatus = array();
$res = CSaleStatus::GetList(array("ID"=>"ASC"), array("LID"=>"ru"), false,false,array("ID","NAME","DESCRIPTION"));
while($ar_fields = $res->GetNext()) {
    $arrSaleStatus[] = $ar_fields;
}

// Выбираем список свойств заказа
$arrOrderProp = array();
$db_props = CSaleOrderProps::GetList(
    array("SORT" => "ASC"),
    array(),
    false,
    false,
    array()
);
while ($ar_fields = $db_props->Fetch()) {
    $arrOrderProp[] = $ar_fields;
}

// Выбираем информацию о типах плательщиков
$arrPersonType = array();
$db_ptype = CSalePersonType::GetList(Array("SORT" => "ASC"), Array());
while ($ptype = $db_ptype->Fetch()) {
    $arrPersonType[$ptype["ID"]] = $ptype["NAME"];
}


// Создаем листинг набора шаблонов на основе, файлов в директории mail_template
$templates_list_files = scandir(__DIR__."/classes/mail_template");
$templates_list = array();

foreach($templates_list_files as $template) {
    if(preg_match("#^(template_.+?)\.php#ims",$template, $match)) {
        $templates_list[] = $match[1];
    }
}


if ($MOD_RIGHT >= "W") {

    if ($REQUEST_METHOD == "GET" && strlen($RestoreDefaults) > 0) {
        COption::RemoveOption($module_id);
        reset($arGROUPS);
        while (list(, $value) = each($arGROUPS))
            $APPLICATION->DelGroupRight($module_id, array($value["ID"]));
    }
    if ($REQUEST_METHOD == "POST" && strlen($Update) > 0) {

        // Сбросим базовые свойства
        COption::SetOptionString($module_id, "reflekto_goodsemail_orderprop_col_left[]", "");
        COption::SetOptionString($module_id, "reflekto_goodsemail_orderprop_col_center[]", "");

        foreach($_POST as $key_post => $val_post) {
            $val = '';
            $in_base_options_list = false;
            foreach($arAllOptions as $opt => $opt_val) {
                // Убираем скобки массива из названия
                $opt_val[0] = str_replace("[]","",$opt_val[0]);
                if($opt_val[0]==$key_post) {
                    $in_base_options_list = true;
                    $name = $arAllOptions[$opt][0];

                    if(is_array($val_post)) {

                        foreach($val_post as $arr_key => $arr_val) {
                            $val .= $arr_val."|";
                        }
                    } else {
                        $val = $$name;
                    }

                    if ($arAllOptions[$opt][3][0] == "checkbox" && $val != "Y")
                        $val = "N";

                    COption::SetOptionString($module_id, $name, $val, $arAllOptions[$opt][1]);
                }
            }

            // Дополнительные опции
            if(!$in_base_options_list) {
                COption::SetOptionString($module_id, $key_post, $val_post);
            }
        }

        // Отключаем опции шаблонов (чекбоксы, если их не было в POST запросе)
        // Устанавливаем в шаблоне дополнительные опции
        if(file_exists(__DIR__."/classes/mail_template/optionsraw_".$_POST['reflekto_goodsemail_template'].".php")) {

            // Найдены опции шаблона
            require(__DIR__."/classes/mail_template/optionsraw_".$_POST['reflekto_goodsemail_template'].".php");

            for ($i = 0; $i < count($options_list); $i++) {
                $Option = $options_list[$i];
                if($Option[3][0]=="checkbox") {
                    if(!isset($_POST[$Option[0]])) {
                        COption::SetOptionString($module_id, $Option[0], "");
                    }
                }
            }
        }

        // Загружаем файлы
        foreach($_FILES as $key_file => $val_file) {
            if($val_file['type']==null) continue;
            $ext_mime_arr = array("image/jpeg", "image/png", "image/pjpeg");
            if(!in_array($val_file['type'], $ext_mime_arr)) {
                echo CAdminMessage::ShowMessage(GetMessage("REFLEKTO_WRONG_FILE_EXT"));

            } else {
                $ext = explode(".", $val_file['name']);

                copy($val_file['tmp_name'], $_SERVER['DOCUMENT_ROOT']."/upload/reflekto_goodsemail_logo.".$ext[count($ext)-1]);

                foreach($arAllOptions as $opt => $opt_val) {
                    if($opt_val[0]==$key_file) {
                        $name = $arAllOptions[$opt][0];
                        COption::SetOptionString($module_id, $name, 'http://'.$_SERVER["SERVER_NAME"]."/upload/reflekto_goodsemail_logo.".$ext[count($ext)-1], $arAllOptions[$opt][1]);
                    }
                }

            }


        }

    }
} //if($MOD_RIGHT>="W"): 
?> 

<? $tabControl->Begin(); ?>
<form method="POST" action="<? echo $APPLICATION->GetCurPage() ?>?mid=<?= htmlspecialchars($mid) ?>&lang=<? echo LANG ?>" enctype="multipart/form-data">
    <?
    // основные настройки
    $tabControl->BeginNextTab();
    ?>

    <?
    $client_id = "";
    for ($i = 0; $i < count($arAllOptions); $i++):
        $Option = $arAllOptions[$i];
        $val = COption::GetOptionString($module_id, $Option[0], $Option[2]);
        $type = $Option[3];
        ?> 
        <tr> 
            <td valign="top" class="option_min_w_200"><? if ($type[0] != "hidden") { ?><font class="tablefieldtext"><? echo $Option[1] ?></font><? } ?></td> 
            <td valign="top"> 
                <font class="tablebodytext">
                    <? if ($type[0] == "select" && $Option[0]=="reflekto_goodsemail_saletype[]"): ?>
                        <?

                            $val_arr = explode("|", $val);
                        ?>
                        <select name="<? echo htmlspecialchars($Option[0]) ?>" multiple="multiple" class="reflekto_w_300">
                            <?
                                foreach($arrSaleStatus as $key_pay => $value_pay) {
                                    ?>
                                        <option value="<?=$value_pay['ID'];?>" <? if(in_array($value_pay['ID'],$val_arr)) echo "selected"; ?>><?=$value_pay['NAME'];?></option>
                                    <?
                                }

                            ?>

                        </select>

                    <? elseif ($type[0] == "checkbox"): ?>
                    <input type="checkbox" name="<? echo htmlspecialchars($Option[0]) ?>" value="Y"<? if ($val == "Y") echo" checked"; ?> />
                <? elseif ($type[0] == "file"): ?>

                        <input type="file" name="<? echo htmlspecialchars($Option[0]) ?>" value="Загрузите файл" />
                            <?
                                if($val!="") {
                                    ?>
                                        <p><img src="<?=$val;?>" /></p>
                                    <?
                                }

                            ?>
                <? elseif ($type[0] == "text"): ?> 
                    <input type="text" class="typeinput reflekto_w_300" size="<? echo $type[1] ?>"
                           maxlength="255" value="<? echo htmlspecialchars($val) ?>" 
                           name="<? echo htmlspecialchars($Option[0]) ?>" />     

                <? elseif ($type[0] == "hidden"): ?> 
                    <input type="hidden" class="typeinput" size="<? echo $type[1] ?>" 
                           maxlength="255" value="<? echo htmlspecialchars($val) ?>" 
                           name="<? echo htmlspecialchars($Option[0]) ?>" />  
                       <? elseif ($type[0] == "textarea"): ?>
                    <textarea rows="<? echo $type[1] ?>" 
                              class="typearea reflekto_w_300" cols="<? echo $type[2] ?>"
                              name="<? echo htmlspecialchars($Option[0]) ?>"><? echo htmlspecialchars($val) ?></textarea>

                <? elseif ($type[0] == "select_template"): ?>
                            <select name="<? echo htmlspecialchars($Option[0]) ?>" id="select_<? echo htmlspecialchars($Option[0]) ?>" class="typeinput">
                                <?
                                foreach($templates_list as $template) {
                                    ?>
                                        <option value="<?= $template?>" <? if($val==$template) echo "selected"; ?>><?= GetMessage("REFLEKTO_TEMPLATE_NAME_".$template);?></option>
                                    <?
                                }
                                ?>
                            </select>

                <? elseif ($type[0] == "select" && ($Option[0]=="reflekto_goodsemail_orderprop_col_left[]" || $Option[0]=="reflekto_goodsemail_orderprop_center[]")): ?>
                    <?

                    $val_arr = explode("|", $val);
                    ?>
                    <select name="<? echo htmlspecialchars($Option[0]) ?>" multiple="multiple" class="reflekto_w_300">
                        <?
                        foreach($arrOrderProp as $key_prop => $value_prop) {
                            ?>
                            <option value="<?=$value_prop['ID'];?>" <? if(in_array($value_prop['ID'],$val_arr)) echo "selected"; ?>><?=$value_prop['NAME'];?> (<?= $arrPersonType[$value_prop['PERSON_TYPE_ID']]?>)</option>
                        <?
                        }

                        ?>

                    </select>



                <? endif ?>

                </font> 
            </td> 
        </tr> 
        <?
    endfor;
    ?>
    <tr>
        <td valign="top" class="option_min_w_200 adm-detail-content-cell-l">
            <font class="tablefieldtext"><?= GetMessage("REFLEKTO_ADDITIONAL_TEMPLATE_OPTIONS");?></font>
        </td>
        <td id="tempalte_options"></td>
    </tr>

    <? $tabControl->BeginNextTab(); ?>
    <? require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/admin/group_rights.php"); ?> 


    <? $tabControl->Buttons(); ?>

    <input type="submit" class="button" name="Update" <? if ($MOD_RIGHT < "W") echo "disabled" ?> 
           value="<? echo GetMessage("ECHO_MODULE_SAVE") ?>">
    <input type="hidden" name="Update" value="Y" />&nbsp;                

    <? $tabControl->End(); ?>
</form> 
