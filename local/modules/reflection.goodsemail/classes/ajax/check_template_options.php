<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?php
/**
 * Created by PhpStorm.
 * User: Andrey
 * Date: 18.09.14
 * Time: 11:00
 */

IncludeModuleLangFile(__FILE__);

if(isset($_REQUEST['template'])) {
    if(file_exists(__DIR__."/../mail_template/options_".$_REQUEST['template'].".php")) {
        // Найдены опции шаблона
        require(__DIR__."/../mail_template/options_".$_REQUEST['template'].".php");

    } else {

        echo GetMessage("REFLEKTO_NO_OPTIONS");
    }


} else {
    echo GetMessage("REFLEKTO_NO_OPTIONS");
}
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_after.php"); ?>