<table cellspacing="0" cellpadding="0" style="margin:0;width: 528px;">
    <tbody>
    <tr>
        <td style="padding: 15px;background-color: #ffffff;margin:0 0 0 0;width:500px;float:left;border-bottom: 1px solid #dadada;">
            <table style="margin:0;padding:0;" cellspacing="0" cellpadding="0">
                <tbody>
                <tr>
                    <td style="height: 105px;margin: 0;padding: 0 15px 0 0;width: 120px;vertical-align: top;">
                        <a href="_DETAIL_URL_" style="text-decoration: none; outline: 0; border: 0; display: block; width: 120px;" target="_blank">
                            <img src="_PREVIEW_PICTURE_" alt="" title="" style="outline: 0; border: 0;">
                        </a>
                    </td>
                    <td>
                        <table style="width: 375px;word-wrap: break-word;white-space: normal;margin: 0;padding: 0;vertical-align: top;">
                            <tbody>
                            <tr>
                                <td>
                                    <ul style="list-style-type:none;margin: 0;padding: 0;">
                                        <li style="list-style-type:none;display: block;margin-bottom: 5px;">
                                            <span style="font-size: 14px;">_PRODUCT_NAME_</span>
                                        </li>
                                        <li style="list-style-type:none;display: block;margin-bottom: 5px;">
									<span style="font-size: 12px;">
									_PRODUCT_PROP_</span>
                                        </li>
                                    </ul>
                                </td>
                                <td align="right" style="margin:0;padding:0;float: right;vertical-align: top; width: 150px;">
                                    <div style="list-style-type:none;margin: 0;padding: 0;">
                                        <p style="list-style-type:none;display: block;margin:0;padding:0;margin-bottom: 5px;text-align: right;">
								            <span style="font-size: 16px;">
									        _PRODUCT_PRICE_ _PRODUCT_PRICE_VAL_</span>
                                        </p>

                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" style="border-top:1px dotted #929294;"></td>
                            </tr>
                            <tr>
                                <td width="200" style="vertical-align: top;">
                                    <div style="margin: 0;padding: 0;">
                                        <p style="font-size: 13px;font-family:arial;display: block;margin:0 0 5px 0px;padding:0;">
                                            <span>_PRODUCT_LABEL_QUANTITY_:</span>
									   <span style="font-weight: bold;"> _PRODUCT_QUANTITY_</span>
                                        </p>

                                    </div>
                                </td>
                                <td align="right" style="margin:0;padding:0;float: right;vertical-align: top;">
                                    <table style="margin: 0;padding: 0;" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr style="padding:15px 0 0;margin:0;">
                                            <td style="text-decoration: none;margin:0 0 0 5px;pading:0; padding-left: 10px; text-align: right;">
                                                <p style="margin-top: 0px; margin-bottom: 3px;">__SUMM__</p>
                                                <span style="font-size: 16px;">__SUMM_ITEM_VAL__</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>