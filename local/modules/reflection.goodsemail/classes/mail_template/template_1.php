<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN"
    "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Order</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
</head>
<body>

<table width="850" border="0" cellspacing="0" cellpadding="0" style="margin: 0 auto;padding: 0;">
<tbody>
<tr style="border: none;">
    <td style="border: none;padding-bottom: 3px;">
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
            <tr>
                <td style="width: 50%;border: none;" valign="middle" align="left">
                    <a href="_SITE_LINK_" target="_blank" style="outline: 0; border: 0; text-decoration: none;">
                        _LOGO_IMG_
                    </a>
                </td>
                <td style="width: 50%;border: none;" align="right">
                    <p style="font-size: 18px;line-height: 18px;margin: 0 0 0 0;word-wrap: break-word;padding:0 0 10px 0;">
                        _CONTACTS_PHONE_
                    </p>
                    <p style="font-size: 18px;line-height: 18px;margin: 0 0 0 0;word-wrap: break-word;padding:0 0 10px 0;">
                        <a href="mailto:_CONTACTS_EMAIL_">_CONTACTS_EMAIL_</a>
                    </p>
                </td>

            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr style="border:none;">
    <td style="margin: 0 0 0 0;width: 100%;background-color: #E4E4E9;border: 1px solid #dadada;float: left;padding:14px 0">
        <table cellspacing="0" cellpadding="0" style="width: 100%;">
            <tbody>
            <tr>
                <td style="padding-left:14px;padding-right:14px;">
                    <p style="font-size: 16px;line-height: 20px;margin: 0 0 20px 0;word-wrap: break-word;padding:0;font-weight:bold;">
                        _YOUR_ORDER_</p>

                    <p style="line-height: 20px;line-height: 20px;margin: 0 0 20px 0;word-wrap: break-word;padding:0;">
                        _ADD_TEXT_</p>

                    <p style="font-size: 16px;line-height: 10px;margin: 0 0 0 0;word-wrap: break-word;padding:0 0 10px 0;">
                        <span style="font-weight:bold;">_ORDER_ID_: _ORDER_ID_VAL_</span>
                    </p>

                    <p style="font-size: 16px;line-height: 10px;margin: 0 0 0 0;word-wrap: break-word;padding:0 0 10px 0;">
                        <span style="font-weight:bold;">_ORDER_DATE_: _ORDER_DATE_VAL_</span>
                    </p>

                    <p style="font-size: 12px;line-height: 20px;margin: 0 0 0 0;word-wrap: break-word;padding:0;">
                    </p>
                </td>
            </tr>
            </tbody>
        </table>
    </td>
</tr>
<tr>
    <td height="15">&nbsp;</td>
</tr>
<tr style="border: none;">
<td style="border: none;" width="100%">
<table width="100%" cellspacing="0" cellpadding="0">
<tbody>
<tr style="border: none;">
<td style="border: none;vertical-align: top;border: 1px solid #dadada;padding: 0px;">
    _PRODUCTS_LIST_

    <table idth="100%" cellspacing="0" cellpadding="0">
        <tr>
            <td style="padding:10px;">
                __ORDER_PROPS_CENTER__

                <p style="line-height: 20px;line-height: 20px;margin: 10px 10px 10px 10px 0;word-wrap: break-word;">
                    _ORDER_COMMENT_TEXT_</p>

            </td>
        </tr>
    </table>
</td>
<td width="15" style="border: none;vertical-align: top;">&nbsp;</td>
<td style="border: none;vertical-align: top;float: right;">
<table cellspacing="0" cellpadding="0" style="width: 100%;">
    <tbody>
    <tr>
        <td style="width: 100%;margin:0;padding:3px 0px;background-color: #1C68AC;">
            <!--DELIVERY_BLOCK-->
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                <tbody>
                <tr>
                    <td width="3">&nbsp;</td>
                    <td width="100%" bgcolor="#ffffff">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:10px 0;background:#ffffff"
                               bgcolor="#ffffff">
                            <tbody>
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="90%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td align="left">
                                                <span style="font-size: 14px;float: left;">_DELIVERY_TYPE_:</span>
                                            </td>
                                            <td align="right">
                                                <span style="font-size: 14px;float: right;">_DELIVERY_TYPE_VAL_</span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left">
                                                <span style="font-size: 14px;float: left;">_DELIVERY_PRICE_:</span>
                                            </td>
                                            <td align="right">
                                                <span style="font-size: 14px;float: right;">_DELIVERY_PRICE_VAL_</span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="3">&nbsp;</td>
                </tr>
                </tbody>
            </table>
            <!--DELIVERY_BLOCK_END-->
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                <tbody>
                <tr>
                    <td width="3">&nbsp;</td>
                    <td width="100%">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:10px 0;">
                            <tbody>
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="90%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td align="left" style="color:#ffffff;font-size:18px;">
                                                <strong style="color:#ffffff;font-size:18px;">_SUMM_:</strong>
                                            </td>
                                            <td align="right">
                                                <span
                                                    style="float: right;font-size: 16px;line-height: 22px;color:#ffffff"><strong>_SUMM_VAL_</strong></span>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="3">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" style="width: 100%;">
    <tbody>
    <tr style="margin: 0;padding: 0;">
        <td style="margin: 0;padding: 0;" height="10">

        </td>
    </tr>
    </tbody>
</table>
    __RIGHT_BLOCK_INFO__

<table cellspacing="0" cellpadding="0"
       style="border: 1px solid #dadada;width: 100%;background-color: #383939;border-bottom:none;">
    <tbody>
    <tr style="margin: 0;padding: 0;">
        <td style="margin: 0;padding: 0;">
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                <tbody>
                <tr>
                    <td width="3">&nbsp;</td>
                    <td width="100%">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="90%">
                                    <table width="100%" cellspacing="0" cellpadding="0">
                                        <tbody>
                                        <tr>
                                            <td align="left"
                                                style="color: #FFFFFF;font-size: 14px;font-weight: bold;padding: 5px 0px;">
                                                _PAY_SYSTEM_
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="3">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
<table cellspacing="0" cellpadding="0" style="border: 1px solid #dadada;width: 100%;border-top:none;">
    <tbody>
    <tr style="margin: 0;padding: 0;">
        <td style="margin: 0;padding: 0;">
            <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                <tbody>
                <tr>
                    <td width="3">&nbsp;</td>
                    <td width="100%">
                        <table width="100%" cellspacing="0" cellpadding="0" style="padding:0;">
                            <tbody>
                            <tr>
                                <td width="5%">&nbsp;</td>
                                <td width="90%">
                                    <table width="178" cellspacing="0" cellpadding="0">

                                        <tbody>
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        <tr>
                                            <td style="margin: 0;padding: 0;line-height: 20px;font-size: 14px;">
                                                _PAY_SYSTEM_VAL_
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="5"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                                <td width="5%">&nbsp;</td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td width="3">&nbsp;</td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
    </tbody>
</table>
</td>
</tr>

<tr>
    <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin:0 auto;padding:0;">
        <tbody>
        <tr style="border:none;">
            <td style="border:none;background-color: #eeeef6;color: #2f2f2f;text-align: center;line-height: 30px;font-size:11px;font-family:arial;margin-left:20px;">
				<span style="margin-left:20px;">
                    _COPYRIGHT_
                </span>
            </td>
        </tr>
        </tbody>
    </table>
</tr>
</tbody>
</table>

</td>
</tr>


</tbody>
</table>

</body>
</html>