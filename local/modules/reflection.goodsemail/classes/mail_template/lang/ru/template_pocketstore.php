<?
$MESS['YOUR_ORDER'] = "Благодарим за Ваш заказ";
$MESS['ORDER_ID'] = "Номер заказа";
$MESS['ORDER_DATE'] = "Дата заказа";
$MESS['DELIVERY_TYPE'] = "Тип доставки";
$MESS['DELIVERY_PRICE'] = "Цена доставки";
$MESS['SUMM'] = "Сумма";
$MESS['DELIVERY_ADDRESS'] = "Параметры заказа";
$MESS['PAY_SYSTEM'] = "Способ оплаты";
$MESS['PRICE'] = "Цена";
$MESS['SHARE_ABOUT'] = "Присоединяйтесь к нам";
$MESS['QUANTITY_LABEL'] = "Количество";
$MESS['YOUR_COMMENT_TO_ORDER'] = "<b>Ваш комментарий к заказу</b>";


?>