<?php
/**
 * Created by ADV/web-engineering co.
 * User: burtsev
 * Date: 19.12.14
 * Time: 21:16
 */

namespace Adv;

use Bitrix\Currency\CurrencyManager;
use Bitrix\Main\Application;
use Bitrix\Main\Context;
use Bitrix\Main\DB\Exception;
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;
use Bitrix\Sale\Basket;
use Bitrix\Sale\DiscountCouponsManager;
use Bitrix\Sale\Order;
use Bitrix\Sale\Result;

class BasketHelper
{
    public static function add2Basket($productId, $quantity, $withGift = false, array $properties = array(), array $fields = array())
    {
        global $APPLICATION;

        $connection = Application::getConnection();

        $quantity = intval($quantity);
        $quantity = $quantity <= 0 ? 1:$quantity;

        $gift = array();

        if($withGift !== false) {
            if($gifts = self::getProductGifts($productId)) {
                $gift = $gifts[$withGift] ? $gifts[$withGift]:reset($gifts);

                $fields['TYPE'] = \CSaleBasket::TYPE_SET;

                $properties[] = array(
                    'CODE' => 'GIFT',
                    'NAME' => 'Подарок',
                    'VALUE' => $gift['NAME']
                );
            }
        }

        try {
            $connection->startTransaction();

            if(!$basketId = \Add2BasketByProductID(intval($productId), $quantity, $fields, $properties)) {
                if ($ex = $APPLICATION->GetException()) {
                    throw new \Exception($ex->GetString());
                } else {
                    throw new \Exception('Ошибка добавления в корзину');
                }
            }

            if($gift) {
                $giftProperties = array(array(
                    'CODE' => 'GIFT_PARENT',
                    'NAME' => 'Подарок к',
                    'VALUE' => $basketId
                ));

                self::add2Basket($gift['ID'], $quantity, false, $giftProperties, array(
                    'SET_PARENT_ID' => $basketId,
                    'PRICE' => 0,
                    'CUSTOM_PRICE' => 'Y',
                    'IGNORE_CALLBACK_FUNC' => 'Y',
                    'PRODUCT_PROVIDER_CLASS' => '\\Adv\\GiftProductProvider'
                ));
            }

            $connection->commitTransaction();
        } catch (\Exception $e) {
            $connection->rollbackTransaction();
            throw $e;
        }

        return $basketId;
    }

    public static function delete($id)
    {
        $db = \CSaleBasket::GetList(
            array('ID' => 'ASC'),
            array(
                'ID' => intval($id),
                'FUSER_ID' => \CSaleBasket::GetBasketUserID(),
                'LID' => SITE_ID,
                'ORDER_ID' => 'NULL'
            ),
            false,
            false,
            array('ID')
        );
        if(!$item = $db->Fetch()) {
            throw new Exception(sprintf('Basket with id %d not found', $id));
        }

        return \CSaleBasket::Delete($item['ID']);
    }

    public static function getBasketGift($basketId)
    {
        $db = \CSaleBasket::GetList(array('ID' => 'DESC'), array('SET_PARENT_ID' => $basketId), false, false, array('ID', 'PRODUCT_ID'));
        if($gift = $db->Fetch()) {
            try {
                return array_merge(self::getProductInfo($gift['PRODUCT_ID'], true), array(
                    'BASKET_ID' => $gift['ID']
                ));
            } catch (\Exception $e) {}
        }

        return array();
    }

    public static function makeOrder($productId, $name, $phone, $email, $captcha = false, $captcha_sid = '')
    {
        global $APPLICATION, $USER;

        if(!strlen($name)) {
            throw new \Exception('Не указано имя покупателя!');
        }

        if(!strlen($phone)) {
            throw new \Exception('Не указан телефон покупателя!');
        }

        if(!strlen($email)) {
            throw new \Exception('Не указана почта покупателя!');
        }

        if($captcha){
            if(\Bitrix\Main\Loader::includeModule('twim.recaptchafree') && !$_SESSION[ 'LAST_RECAPTCHA' ][ $captcha_sid ][ 'success' ]){
                throw new \Exception('Ошибка капчи!');
            }
        }

        $user = array();
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $email = sprintf('%s@oneclick.small.ru', preg_replace('([^\\d]+)', '', $phone));
        }
        $password = randString(6);

        $connection = Application::getConnection();

        try {

            $connection->startTransaction();

            if(!$USER->IsAuthorized()) {
                $db = \CUser::GetList($by = 'id', $order = 'asc', array('=EMAIL' => $email));
                if(!$user = $db->Fetch()) {
                    $obUser = new \CUser();

                    $userId = $obUser->Add($user = array(
                        'NAME' => $name,
                        'EMAIL' => $email,
                        'LOGIN' => $email,
						'PERSONAL_PHONE' => $phone,
                        'PASSWORD' => $password,
                        'CONFIRM_PASSWORD' => $password,
                    ));

                    if(!$userId) {
                        throw new \Exception($obUser->LAST_ERROR);
                    }

                    $user['ID'] = $userId;
                }
            } else {
                $user['ID'] = $USER->GetID();
            }

            $fields = array(
                'LID' => SITE_ID,
                'PERSON_TYPE_ID' => \Constants::PERSON_TYPE_INDIVIDUAL_ID,
                'PAYED' => 'N',
                'CANCELED' => 'N',
                'STATUS_ID' => 'N',
                'PRICE' => 0,
                'CURRENCY' => 'RUB',
                'USER_ID' => $user['ID'],
                'PRICE_DELIVERY' => 0,
                'COMMENTS' => 'Заказ в 1 клик'
            );

            if(!$orderId = \CSaleOrder::Add($fields)) {
                if($ex = $APPLICATION->GetException()) {
                    throw new \Exception($ex->GetString());
                } else {
                    throw new \Exception('Ошибка при создании заказа!');
                }
            }

            Utils::setOrderProp($orderId, 'NAME', $name, false);
            Utils::setOrderProp($orderId, 'PERSONAL_PHONE', $phone, false);
            Utils::setOrderProp($orderId, 'EMAIL', $email, false);

            $basketId = self::add2Basket($productId, 1, false, array(array('CODE' => 'ONECLICK', 'NAME' => 'Заказ в 1 клик', 'VALUE' => 'Заказ №' . $orderId)), array('ORDER_ID' => $orderId));

            $basket = \CSaleBasket::GetByID($basketId);
            \CSaleOrder::Update($orderId, array('PRICE' => $basket['PRICE']));

            $connection->commitTransaction();

            self::sendEmailNewOrder($orderId, $user['ID']);

            /**
             * Модуль imaginweb.sms срабратывает на событии OnSaleComponentOrderComplete
             */
            $order = \CSaleOrder::GetByID($orderId);
            foreach(GetModuleEvents('sale', 'OnSaleComponentOrderComplete', true) as $event) {
                ExecuteModuleEventEx($event, array($orderId, $order));
            }

            $site = \CSite::GetByID($order['LID'])->Fetch();

            $basketItems = array();
            $db = \CSaleBasket::GetList(
                array('NAME' => 'ASC'),
                array('ORDER_ID' => $orderId)
            );
            while ($basket = $db->fetch()) {
                $basketItems[] = array(
                    'NAME' => $basket['NAME'],
                    'PRODUCT_ID' => $basket['PRODUCT_ID'],
                    'PRICE' => $basket['PRICE'],
                    'QUANTITY' => $basket['QUANTITY']
                );
            }

            return array(
                'ORDER_ID' => $order['ID'],
                'ORDER' => array(
                    'ID' => $order['ID'],
                    'PRICE' => $order['PRICE'],
                    'PRICE_DELIVERY' => $order['PRICE_DELIVERY'],
                    'SITE' => $site['SITE_NAME']
                ),
                'ITEMS' => $basketItems
            );

        } catch (\Exception $e) {
            $connection->rollbackTransaction();
            throw $e;
        }
    }

    public static function getProductInfo($id, $useAvailable = false)
    {
        $catalogIblockId = 5;

        $filter = array(
            'IBLOCK_ID' => $catalogIblockId,
            '=ID' => intval($id),
            'ACTIVE' => 'Y'
        );

        if($useAvailable) {
            $filter['CATALOG_AVAILABLE'] = 'Y';
        }

        $db = \CIBlockElement::GetList(
            array('ID' => 'ASC'),
            $filter,
            false,
            false,
            array('ID', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'NAME', 'DETAIL_PAGE_URL', 'CATALOG_GROUP_2', 'PREVIEW_PICTURE', 'PROPERTY_rating')
        );
        if(!$item = $db->GetNext(true, false)) {
            throw new \Exception('Товар не найден!');
        }

        $item['PRICES'] = \CIBlockPriceTools::GetItemPrices(
            $catalogIblockId,
            \CIBlockPriceTools::GetCatalogPrices($catalogIblockId, array('Розничная реализация')),
            $item
        );

        $product = array(
            'ID' => $item['ID'],
            'NAME' => htmlspecialcharsback($item['NAME']),
            'URL' => $item['DETAIL_PAGE_URL'],
            'PRICE' => $item['PRICES']['Розничная реализация']['DISCOUNT_VALUE'],
            'RATING' => intval($item['PROPERTY_RATING_VALUE'])
        );

        return $product;
    }

    public static function setBasketGift($basketId, $giftProductId)
    {
        if(!$basket = \CSaleBasket::GetByID($basketId)) {
            throw new \Exception(sprintf('Basket %d not found', $basketId));
        }

        $db = \CSaleBasket::GetList(array('ID' => 'DESC'), array('SET_PARENT_ID' => $basket['ID']), false, false, array('ID', 'PRODUCT_ID'));
        if($currentGift = $db->Fetch()) {
            if($currentGift['PRODUCT_ID'] == $giftProductId) {
                return true;
            }

            \CSaleBasket::Delete($currentGift['ID']);
        }

        if(!$product = self::getProductInfo($giftProductId)) {
            throw new \Exception(sprintf('Product with id %d not found!', $giftProductId));
        }

        $giftProperties = array(array(
            'CODE' => 'GIFT_PARENT',
            'NAME' => 'Подарок к',
            'VALUE' => $basket['ID']
        ));

        $parentProperties = array_merge(self::getBasketProperties($basket['ID']), array('GIFT' => array(
            'CODE' => 'GIFT',
            'NAME' => 'Подарок',
            'VALUE' => $product['NAME']
        )));

        self::add2Basket($giftProductId, $basket['QUANTITY'], false, $giftProperties, array('SET_PARENT_ID' => $basket['ID']));

        return \CSaleBasket::Update($basket['ID'], array('PROPS' => $parentProperties));
    }

    public static function getProductGifts($id)
    {
        return self::getProductRelations($id, 'SM_GIFT', 2);
    }

    public static function getProductAccessories($id)
    {
        return self::getProductRelations($id, 'ACCESSORIES', 10);
    }

   public static function getProductSet($id)
    {
        $result = array();
        $products = array();
        $sets = array();

        $catalogIblockId = Utils::getIBlockIdByCode(\Constants::CATALOG_IB_CODE, \Constants::CATALOG_IB_TYPE);
        $prices = \CIBlockPriceTools::GetCatalogPrices($catalogIblockId, array(\Constants::CATALOG_PRICE_CODE));

        $db = \CIBlockElement::GetProperty($catalogIblockId, $id, 'SORT', 'ASC', array('CODE' => 'SM_SET'));
        while($product = $db->Fetch())
        {
            if(strlen($product['VALUE']))
            {
                $sets[$product['VALUE']] = array_merge(array('ID' => $product['VALUE']), (array)unserialize($product['DESCRIPTION']));
            }
        }
        if($sets)
        {
            if(count($sets) > 2)
            {
                $sets = array_slice($sets, 0, 2);
            }
            $sets[] = array("ID" => $id,"SORT" => 10000,"DISCOUNT" => 0);
            $tempArr = array();
            foreach ($sets as $key => $value)
            {
                $tempArr[$value["ID"]] = $value;
            }
            $sets = $tempArr;
            $db = \CIBlockElement::GetList(
                array('SORT' => 'ASC'),
                array(
                    'ACTIVE' => 'Y',
                    'CATALOG_AVAILABLE' => 'Y',
                    'IBLOCK_ID' => $catalogIblockId,
                    'ID' => array_map(function($item) {
                        return $item['ID'];
                    }, $sets)
                ),
                false,
                false,
                array('ID', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'NAME', 'XML_ID', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'PROPERTY_rating', 'CATALOG_QUANTITY', 'CATALOG_AVAILABLE', 'CATALOG_GROUP_' . \Constants::CATALOG_PRICE_ID)
            );
            while($product = $db->GetNext(true, false))
            {

                $product['PRICES'] = \CIBlockPriceTools::GetItemPrices(
                    $catalogIblockId,
                    $prices,
                    $product
                );

                if($product['PRICES'][\Constants::CATALOG_PRICE_CODE])
                {
                    $product['DISCOUNT_PRICE'] = array(
                        'DISCOUNT_VALUE' => $product['PRICES'][\Constants::CATALOG_PRICE_CODE]['DISCOUNT_VALUE'] * (1 - $sets[$product['ID']]['DISCOUNT'] / 100),
                        'VALUE' => $product['PRICES'][\Constants::CATALOG_PRICE_CODE]['DISCOUNT_VALUE'],
                        'DISCOUNT_DIFF' =>  $product['PRICES'][\Constants::CATALOG_PRICE_CODE]['DISCOUNT_VALUE'] - $product['PRICES'][\Constants::CATALOG_PRICE_CODE]['DISCOUNT_VALUE'] * (1 - $sets[$product['ID']]['DISCOUNT'] / 100),
                        'DISCOUNT_DIFF_PERCENT' => $sets[$product['ID']]['DISCOUNT'],
                    );
                }

                $products[] = array(
                    'ID' => $product['ID'],
                    'XML_ID' => $product['XML_ID'],
                    'SORT' => $sets[$product['ID']]['SORT'],
                    'NAME' => htmlspecialcharsBack($product['NAME']),
                    'URL' => $product['DETAIL_PAGE_URL'],
                    'IMAGE' => $product['PREVIEW_PICTURE'],
                    'CATEGORY' => Helper::getCatalogParentSectionName($product['IBLOCK_SECTION_ID']),
                    'PRICE' => $product['PRICES'][\Constants::CATALOG_PRICE_CODE]['DISCOUNT_VALUE'],
                    'DISCOUNT_PRICE' => $product['DISCOUNT_PRICE'],
                    'FORMAT_PRICE' => Utils::format($product['DISCOUNT_PRICE']['DISCOUNT_VALUE']),
                    'RATING' => intval($product['PROPERTY_RATING_VALUE'])
                );
            }
            usort($products, function($a, $b)
            {
                return $a['SORT'] == $b['SORT'] ? 0:($a['SORT'] < $b['SORT'] ? 1:-1);
            });
            // if(count($products) > 3)
            // {
            //     $products = array_slice($products, 0, 3);
            // }
        }

        foreach($products as $product) {
            $result[$product['ID']] = $product;
        }

        return $result;
    }

    public static function buySet($mainProductId, array $items)
    {
        if(!$product = self::getProductInfo($mainProductId)) {
            throw new \Exception('Set product not found!');
        }

        if(!count($items)) {
            throw new \Exception('Set is empty!');
        }

        $products = self::getProductSet($product['ID']);

        $parentBasket = self::add2Basket($product['ID'], 1, false, array('IS_SET' => array(
            'CODE' => 'IS_SET',
            'NAME' => 'С комплектными товарами',
            'VALUE' => 'да'
        )));

        $basketProperties = array(
            'SET_NAME' => array(
                'CODE' => 'SET_ID',
                'NAME' => 'В комплекте с товаром',
                'VALUE' => $product['NAME']
            ),
            'SET_ID' => array(
                'CODE' => 'SET_ID',
                'NAME' => 'Номер комплекта',
                'VALUE' => $parentBasket
            ),
            'SET_PRODUCT_ID' => array(
                'CODE' => 'SET_PRODUCT_ID',
                'NAME' => 'Родительский товар комплекта',
                'VALUE' => $mainProductId
            ),
            'SET_DISCOUNT' => array(
                'CODE' => 'SET_DISCOUNT',
                'NAME' => 'Скидка за комплект',
                'VALUE' => 0
            )
        );

        $result = array('ITEMS' => array($product));

        foreach($items as $id) {
            if($product = $products[$id]) {
                $product['IMAGE'] = Utils::showImage($product['IMAGE'] ? $product['IMAGE']:'/static/img/no_photo.png', 100, 132, false, 'single');

                $result['ITEMS'][] = $product;

                $basketProperties['SET_DISCOUNT']['VALUE'] = $product['DISCOUNT_PRICE']['DISCOUNT_DIFF'];

                self::add2Basket($id, 1, false, $basketProperties, array(
                    'PRICE' => $product['DISCOUNT_PRICE']['DISCOUNT_VALUE'],
                    'CUSTOM_PRICE' => 'Y',
                    'IGNORE_CALLBACK_FUNC' => 'Y',
                    'PRODUCT_PROVIDER_CLASS' => '\\Adv\\SetProductProvider',
                ));
            }
        }

        return $result;
    }

    public static function onBeforeBasketDelete($id)
    {
        $properties = BasketHelper::getBasketProperties($id);
        if($properties['IS_SET']['VALUE']) {
            $db = \CSaleBasket::GetList(
                array('ID' => 'ASC'),
                array(
                    'FUSER_ID' => \CSaleBasket::GetBasketUserID(),
                    'ORDER_ID' => 'NULL'
                )
            );
            while($item = $db->Fetch()) {
                $props = self::getBasketProperties($item['ID']);
                if($props['SET_ID']['VALUE'] == $id) {
                    foreach($props as $k => $prop) {
                        if(preg_match('/^SET_.*$/', $prop['CODE'])) {
                            unset($props[$k]);
                        }
                    }

                    /** @noinspection PhpParamsInspection */
                    $product = \CCatalogProductProvider::GetProductData(array(
                        'PRODUCT_ID' => $item['PRODUCT_ID'],
                        'QUANTITY'   => 1,
                        'RENEWAL'    => 'N',
                        'USER_ID'    => $item['USER_ID'],
                        'SITE_ID'    => $item['LID'],
                        'BASKET_ID' => $item['ID']
                    ));

                    \CSaleBasket::Update($item['ID'], array(
                        'PRICE' => $product['PRICE'],
                        'IGNORE_CALLBACK_FUNC' => 'N',
                        'CUSTOM_PRICE' => 'N',
                        'DISCOUNT_PRICE' => '',
                        'PRODUCT_PROVIDER_CLASS' => 'CCatalogProductProvider',
                        'DISCOUNT_NAME' => '',
                        'PROPS' => $props
                    ));
                }
            }
        }
    }

    public static function getBasketProperties($id)
    {
        $properties = array();

        $db = \CSaleBasket::GetPropsList(
            array('SORT' => 'ASC'),
            array("BASKET_ID" => intval($id))
        );
        while ($property = $db->Fetch()) {
            $properties[$property['CODE']] = array(
                'CODE' => $property['CODE'],
                'NAME' => $property['NAME'],
                'SORT' => $property['SORT'],
                'VALUE' => $property['VALUE']
            );
        }

        return $properties;
    }

    public static function getTotalBasketState()
    {
        $result = array();

        $db = \CSaleBasket::GetList(
            array('ID' => 'ASC'),
            array(
                'FUSER_ID' => \CSaleBasket::GetBasketUserID(),
                'ORDER_ID' => 'NULL'
            )
        );
        while($item = $db->Fetch()) {
            if(\CSaleBasketHelper::isSetItem($item)) {
                continue;
            }

            $result['ITEMS'][$item['ID']] = array(
                'PRICE' => round($item['PRICE']),
                'QUANTITY' => intval($item['QUANTITY']),
                'FORMAT_PRICE' => Utils::format($item['PRICE']),
            );

            $result['QUANTITIES'][$item['PRODUCT_ID']] = intval($item['QUANTITY']);

            $result['PRICE'] += $item['PRICE'] * $item['QUANTITY'];
            $result['QUANTITY'] += $item['QUANTITY'];
        }

        $result['FORMAT_PRICE'] = Utils::format($result['PRICE']);

        return $result;
    }

    protected static function getProductRelations($id, $type, $count)
    {
        $catalogIblockId = Utils::getIBlockIdByCode(\Constants::CATALOG_IB_CODE, \Constants::CATALOG_IB_TYPE);

        $products = array();
        $relatedXmlId = array();

        $db = \CIBlockElement::GetProperty($catalogIblockId, $id, 'SORT', 'ASC', array('CODE' => $type));
        while($item = $db->Fetch()) {
            if(strlen($item['VALUE'])) {
                $relatedXmlId[] = $item['VALUE'];
            }
        }

        if($relatedXmlId) {
            $prices = \CIBlockPriceTools::GetCatalogPrices($catalogIblockId, array(\Constants::CATALOG_PRICE_CODE));

            $db = \CIBlockElement::GetList(
                array('SORT' => 'ASC'),
                array('IBLOCK_ID' => $catalogIblockId, 'ACTIVE' => 'Y', 'CATALOG_AVAILABLE' => 'Y', '=XML_ID' => $relatedXmlId),
                false,
                array('nTopCount' => $count),
                array('ID', 'IBLOCK_SECTION_ID', 'IBLOCK_ID', 'NAME', 'XML_ID', 'DETAIL_PAGE_URL', 'PREVIEW_PICTURE', 'CATALOG_QUANTITY', 'CATALOG_AVAILABLE', 'CATALOG_GROUP_' . \Constants::CATALOG_PRICE_ID, 'PROPERTY_rating')
            );
            while($item = $db->GetNext(true, false)) {

                $item['PRICES'] = \CIBlockPriceTools::GetItemPrices(
                    $catalogIblockId,
                    $prices,
                    $item
                );

                $products[$item['ID']] = array(
                    'ID' => $item['ID'],
                    'XML_ID' => $item['XML_ID'],
                    'NAME' => $item['NAME'],
                    'URL' => $item['DETAIL_PAGE_URL'],
                    'IMAGE' => $item['PREVIEW_PICTURE'],
                    'DESC' => '', // TODO: add real description $data['PREVIEW_TEXT'],
                    'CATEGORY' => Helper::getCatalogParentSectionName($item['IBLOCK_SECTION_ID']),
                    'PRICE' => $item['PRICES'][\Constants::CATALOG_PRICE_CODE]['DISCOUNT_VALUE']
                );
            }
        }

        return $products;
    }

    public static function sendEmailNewOrder($newOrderId, $buyerId)
    {
        global $DB, $SERVER_NAME;

        $userName = null;

        if(!$user = \CUser::GetByID($buyerId)->Fetch()) {
            return false;
        }

        $orderUserName = Utils::getOrderProp($newOrderId, 'NAME');
        $orderUserEmail = Utils::getOrderProp($newOrderId, 'EMAIL');

        if($orderUserName) {
            $userName .= ' ' . $orderUserName;
        } elseif ($user['NAME']) {
            $userName .= ' ' . $user['NAME'];
        }

        $userEmail = $orderUserEmail ? $orderUserEmail:$user['EMAIL'];

        $strOrderList = '';
        $baseLangCurrency = \CSaleLang::GetLangCurrency(LANGUAGE_ID);
        $orderNew = \CSaleOrder::GetByID($newOrderId);
        $orderNew['BASKET_ITEMS'] = array();

        $db = \CSaleBasket::GetList(
            array('SET_PARENT_ID' => 'DESC', 'TYPE' => 'DESC', 'NAME' => 'ASC'),
            array('ORDER_ID' => $newOrderId),
            false,
            false,
            array('ID', 'PRICE', 'QUANTITY', 'NAME',"PRODUCT_ID")
        );
        while ($arBasketTmp = $db->GetNext()) {
            $orderNew['BASKET_ITEMS'][] = $arBasketTmp;
        }

        $orderNew['BASKET_ITEMS'] = getMeasures($orderNew["BASKET_ITEMS"]);
        $productId = $orderNew['BASKET_ITEMS']["0"]["PRODUCT_ID"];
        foreach ($orderNew['BASKET_ITEMS'] as $val) {
            if (\CSaleBasketHelper::isSetItem($val))
                continue;

            $measure = (isset($val['MEASURE_TEXT'])) ? $val['MEASURE_TEXT'] : GetMessage('SALE_YMH_SHT');
            $strOrderList .= $val['NAME'] . ' - ' . $val['QUANTITY'] . ' ' . $measure . ': ' . SaleFormatCurrency($val['PRICE'], $baseLangCurrency);
            $strOrderList .= "\n";
        }

        \CEvent::Send('SALE_NEW_ORDER_ONE_CLICK', \Constants::DEFAULT_SITE_ID, array(
            'ORDER_ID' => $orderNew['ACCOUNT_NUMBER'],
            'ORDER_DATE' => Date($DB->DateFormatToPHP(\CLang::GetDateFormat('SHORT', LANGUAGE_ID))),
            'ORDER_USER' => $userName,
            'PRICE' => SaleFormatCurrency($orderNew['PRICE'], $baseLangCurrency),
            'EMAIL' => $userEmail,
            'ORDER_LIST' => $strOrderList,
            'PRODUCT_ID' => $productId,
            'SALE_EMAIL' => \COption::GetOptionString('sale', 'order_email', 'order@' . $_SERVER['SERVER_NAME']),
            'DELIVERY_PRICE' => $orderNew['DELIVERY_PRICE'],
            'MANAGE_LINK' => 'http://' . $SERVER_NAME . '/bitrix/admin/sale_order_detail.php?ID=' . $newOrderId . '&lang=' . LANGUAGE_ID
        ));

        return true;
    }
}