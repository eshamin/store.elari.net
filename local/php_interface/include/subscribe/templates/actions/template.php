<?
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $APPLICATION, $SUBSCRIBE_TEMPLATE_RUBRIC;
$SUBSCRIBE_TEMPLATE_RUBRIC = $arRubric;

if($SUBSCRIBE_TEMPLATE_RUBRIC['ELEMENT_ID']) { ?>
    <div><?=$SUBSCRIBE_TEMPLATE_RUBRIC['NAME']?></div>
    <? $result = $APPLICATION->IncludeComponent(
        "adv:iblock.post",
        "",
        array(
            "IBLOCK_ID" => \Adv\Utils::getIBlockIdByCode('actions', 'news'),
            "ELEMENT_ID" => $SUBSCRIBE_TEMPLATE_RUBRIC['ELEMENT_ID'],
        )
    );

    if(!$result) {
        return false;
    }

	$return = array(
		'SUBJECT' => $SUBSCRIBE_TEMPLATE_RUBRIC['NAME'],
		'BODY_TYPE' => 'html',
		'CHARSET' => 'UTF-8',
		'DIRECT_SEND' => 'Y',
		'FROM_FIELD' => COption::GetOptionString('main', 'email_from'),
	);

    return $return;
} else {
	return false;
}