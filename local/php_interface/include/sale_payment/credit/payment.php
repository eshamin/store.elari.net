<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

\Bitrix\Main\Loader::includeModule('iblock');


$ORDER_ID = IntVal($GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["ID"]);

$arOrder = CSaleOrder::GetByID($ORDER_ID);

$arXml = array(
    'INN' => CSalePaySystemAction::GetParamValue('INN'),
    'tradePointPhone' => CSalePaySystemAction::GetParamValue('TRADEPOINTPHONE'),
    'email' => CSalePaySystemAction::GetParamValue('EMAIL'),
    'tradePointPhone' => CSalePaySystemAction::GetParamValue('TRADEPOINTPHONE'),
    'tradeSite' => CSalePaySystemAction::GetParamValue('TRADESITE'),
    'userId' => CSalePaySystemAction::GetParamValue('USERID'),
    'reference' => $ORDER_ID,
    'specificationList' => array()
);






$dbBasket = CSaleBasket::GetList(array("DATE_INSERT" => "ASC", "NAME" => "ASC"), array("ORDER_ID" => $ORDER_ID));
while ($arBasket = $dbBasket->Fetch()) {
    if(CSaleBasketHelper::isSetItem($arBasket)) {
        continue;
    }
    $arElement = CIBlockElement::GetByID($arBasket['PRODUCT_ID'])->Fetch();
    $sectionCode = getAlfaSectionCode($arElement['IBLOCK_SECTION_ID']);

    if(strlen($arBasket['NAME']) > 50) {
        $arBasket['NAME'] = substr($arBasket['NAME'], 0, 47).'...';
    }

    $arXml['specificationList'][] = array(
        'category' => $sectionCode,
        'code' => $arBasket['PRODUCT_ID'],
        'description' => $arBasket['NAME'],
        'amount' => (int)$arBasket['QUANTITY'],
        'price' => $arBasket['PRICE'],
    );
}

function getAlfaSectionCode($sectionId)
{
    $catalogIblockId = \Adv\Utils::getIBlockIdByCode('catalog', '1c_catalog');

    $arSection = CIBlockSection::GetList(array(), array(
        'IBLOCK_ID' => $catalogIblockId,
        'ID' => $sectionId
    ), false, array('UF_ALFA_SECTION_CODE'))->Fetch();

    if(!empty($arSection['UF_ALFA_SECTION_CODE'])) {
        $arElement = CIBlockElement::GetByID($arSection['UF_ALFA_SECTION_CODE'])->Fetch();
        if(!empty($arElement['CODE'])) {
            return $arElement['CODE'];
        }
    }

    if($arSection['IBLOCK_SECTION_ID'] > 0) {
        return getAlfaSectionCode($arSection['IBLOCK_SECTION_ID']);
    }

    return false;
}






$dom = new DOMDocument();
$inParams = $dom->createElement('inParams');

foreach($arXml as $tag => $value) {
    if($tag == 'specificationList') {
        $specificationList = $dom->createElement('specificationList');
        foreach($value as $rows) {
            $specificationListRow = $dom->createElement('specificationListRow');
            foreach($rows as $col => $val) {
                $col = $dom->createElement($col);
                $text = $dom->createTextNode($val);
                $col->appendChild($text);
                $specificationListRow->appendChild($col);
            }
            $specificationList->appendChild($specificationListRow);
        }

        $inParams->appendChild($specificationList);

    } else {
        $element = $dom->createElement($tag);
        $text = $dom->createTextNode(trim($value));
        $element->appendChild($text);
        $inParams->appendChild($element);
    }
}

$dom->appendChild($inParams);


//echo htmlentities($dom->saveHTML());die;

?>

<form id="alfa_bank" method="post" action="https://anketa.alfabank.ru/alfaform-pos/endpoint" target="_blank">
    <input type="hidden" name="InXML" value="<?=addslashes($dom->saveHTML())?>">
    <input class="ordering_thanks__cabinet timer" type="submit" value="Перейти к оформлению заявки на кредит">
</form>

<script>
    $(function(){
        var s = 5;
        var inputVal = $('input.timer').val();

        var timer = setInterval(function(){
            $('input.timer').val(inputVal +' '+ s--);
        }, 1000);

        setTimeout(function(){
            $('#alfa_bank').submit();
            clearInterval(timer);
            $('input.timer').val(inputVal);
        }, 7000);

    });
</script>