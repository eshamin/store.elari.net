<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/cash.php"));

$psTitle = 'В кредит';
$psDescription = 'Оформление заявки на кредит на сайте Альфа банка.';





$arPSCorrespondence = array(
    "INN" => array(
        "NAME" => "ИНН магазина",
        "DESCR" => "",
        "VALUE" => "",
        "TYPE" => ""
    ),
    "EMAIL" => array(
        "NAME" => "E-mail интернет-магазина",
        "DESCR" => "Адрес электронной почты, на который будет отправлено уведомление о кредитном решении банка.",
        "VALUE" => "",
        "TYPE" => ""
    ),
    "TRADEPOINTPHONE" => array(
        "NAME" => "Телефон интернет-магазина",
        "DESCR" => "в формате 79161695135",
        "VALUE" => "",
        "TYPE" => ""
    ),
    "USERID" => array(
        "NAME" => "Уникальный код магазина.",
        "DESCR" => "",
        "VALUE" => "771349",
        "TYPE" => ""
    ),
    "TRADESITE" => array(
        "NAME" => "Номер торговой площадки.",
        "DESCR" => "",
        "VALUE" => "AIZQ01_IS",
        "TYPE" => ""
    )
);
?>