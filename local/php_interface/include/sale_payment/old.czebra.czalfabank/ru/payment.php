<?
global $MESS;
$MESS["SPCP_DTITLE"] = "Альфа-Банк";
$MESS["SPCP_DDESCR"] = "Оплата банковскими картами через платежную систему Альфа-Банка";


$MESS["ShopGatewayURL"] = "Адрес шлюза";
$MESS["ShopGatewayURL_DESCR"] = "* Обязательный параметр.<br/> Тестовый адрес - https://test.paymentgate.ru/testpayment/ <br/> Рабочий адрес - https://engine.paymentgate.ru/payment/";
$MESS["ShopLogin"] = "Логин магазина";
$MESS["ShopLogin_DESCR"] = "* Обязательный параметр. Выдается при подключении";
$MESS["ShopPassword"] = "Пароль магазина";
$MESS["ShopPassword_DESCR"] = "* Обязательный параметр. Выдается при подключении";
$MESS["OrderID"]="Номер заказа";
$MESS["OrderID_DESCR"]="* Обязательный параметр.";
$MESS["OrderSum"] = "Стоимость заказа";
$MESS["OrderSum_DESCR"] = "* Обязательный параметр.";
$MESS["OrderCurrency"] = "Код валюты платежа ISO 4217";
$MESS["OrderCurrency_DESCR"] = "Не обязательный параметр.<br/> По умолчанию Российские рубли - 810. <br/> Тенге - 398 <br/> Гривны - 980";
$MESS["ReturnURL"] = "Адрес, на который надо перенаправить пользователя после оплаты";
$MESS["ReturnURL_DESCR"] = "* Обязательный параметр.";
$MESS["Type"] = "Тип платежа";
$MESS["Type_DESCR"] = "* Обязательный параметр.";
$MESS["TYPE_1"] = "Одностадийный платеж";
$MESS["TYPE_2"] = "Двухстадийный платеж";
$MESS["SessionTimeoutSecs"] = "Продолжительность сессии в секундах";
$MESS["SessionTimeoutSecs_DESCR"] = "Не обязательный параметр. Не может превышать 1200 секунд (20 минут).";

$MESS["ERRORS"] = "Ошибка";
$MESS["ERRORS_SaleOrderProp"] = "Ошибка: нет обязательного свойства заказа с мнемоническим кодом PaymentAlfaBank_URL";
$MESS["SUCCESS"] = "Ваш заказ успешно оплачен";
$MESS["ORDER_DESCRIPTION"] = 'Оплата заказа №#ORDER_ID# на сайте small.ru';
$MESS["ERROR_TAKE_HELP"] = 'Обратитесь к менеджерам магазина по телефону';