<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$shop_gateway =  CSalePaySystemAction::GetParamValue("ShopGatewayURL");
$shop_login = CSalePaySystemAction::GetParamValue("ShopLogin");
$shop_pass =  CSalePaySystemAction::GetParamValue("ShopPassword");

$url = $shop_gateway.'/rest/getOrderStatus.do';
$params = array(
    'userName' => $shop_login,
    'password' => $shop_pass,
    'orderId' => $_REQUEST["orderId"],
);

$result = file_get_contents($url, false, stream_context_create(array(
    'http' => array(
        'method'  => 'POST',
        'header'  => 'Content-type: application/x-www-form-urlencoded',
        'content' => http_build_query($params)
    )
)));

$result = json_decode((string)$result, true);

//for debugging
$result_log = print_r($result,true);
file_put_contents($_SERVER["DOCUMENT_ROOT"]."/upload/cardpay_log.txt", date('d.m.Y H:i:s')." result_rec.php received response (result):\n".$result_log."\n", FILE_APPEND | LOCK_EX);

if((int)$result["ErrorCode"] == 0){
    $message = GetMessage("SUCCESS");
    CSaleOrder::PayOrder((int)$result["OrderNumber"], "Y");
} else {
    $message = GetMessage("ERRORS")." ".$result["ErrorCode"].": ".$result["ErrorMessage"];
}
?>
<?
//Sending transaction data to GA e-commerce
if($result["ErrorCode"] == 0 && $result["OrderNumber"] > 0) {
	$orderId = intval($result["OrderNumber"]);
	$order = CSaleOrder::GetByID($orderId);

	if(!(isset($_SESSION['GA_ORDER_ID_BANKCARD']) && intval($_SESSION['GA_ORDER_ID_BANKCARD']) == $orderId)) {
		$gaOutput = array();
		$site = \CSite::GetByID($order['LID'])->Fetch();

		$gaOutput[] = "ga('require', 'ecommerce', 'ecommerce.js');";
		$gaOutput[] = sprintf(
			"ga('ecommerce:addTransaction', {'id': '%s', 'affiliation': '%s', 'revenue': '%s', 'shipping': '%s', 'tax': '0'});",
			$orderId,
			$site['SITE_NAME'],
			$order['PRICE'],
			$order['PRICE_DELIVERY']
		);
	
		$db = \CSaleBasket::GetList(
			array("NAME" => "ASC"),
			array("ORDER_ID" => $orderId)
		);
		while ($basket = $db->fetch()) {
			$gaOutput[] = sprintf(
				"ga('ecommerce:addItem', {'id': '%s', 'name': '%s', 'sku': '%s', 'category': '', 'price': '%s', 'quantity': '%s'});",
				$orderId,
				$basket['NAME'],
				$basket['PRODUCT_ID'],
				$basket['PRICE'],
				$basket['QUANTITY']
			);
		}
		$gaOutput[] = "ga('ecommerce:send');";
	
		$_SESSION['GA_ORDER_ID_BANKCARD'] = $orderId;
	
		$APPLICATION->AddViewContent('GA_ECOMMERCE', implode("\n", $gaOutput));
	}
}
?>
<div class="order-checkout">
    <div class="cart_items">
        <div class="ordering_thanks<?=$result["ErrorCode"] > 0 ? ' error':''?>">
            <h1><?=$result["ErrorCode"] == 0 ? 'Спасибо за заказ!':'Платеж отклонен!'?></h1>
            <? if($result["ErrorCode"] == 0 && $result["OrderNumber"] > 0) { ?>
                <div class="ordering_thanks__details">
                    <p>Номер вашего заказа № <?=$result["OrderNumber"]?>.</p>
                    <p>Вам отправлен e-mail и SMS с номером заказа.</p>
                </div>
            <? } elseif($result["ErrorCode"] > 0) {?>
                <div class="receive-error"><?$APPLICATION->IncludeFile(SITE_DIR . 'include/payment_error.php');?></div>
            <? } ?>
            <div class="ordering_thanks__process">
                <div>Способ оплаты заказа: <b>Банковские карты</b></div>
                <? if($result["ErrorCode"] == 0) { ?>
                    <div class="payment-info">
                        <?=$message?>
                    </div>
                <? } ?>
            </div>

            <a class="ordering_thanks__cabinet" href="/personal/orders/">Перейти в личный кабинет</a>
            <a class="ordering_thanks__continue" href="/">Продолжить покупки</a>
        </div>
    </div>
</div>
<?
$APPLICATION->IncludeComponent(
    "quetzal:tracking.order",
    "",
    Array(
        "COMPONENT_TEMPLATE" => ".default",
        "ORDER_PARAM_TRANSACTION" => $result["OrderNumber"]
    )
);?>
