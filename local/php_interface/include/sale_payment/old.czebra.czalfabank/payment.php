<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

	$shop_gateway =  CSalePaySystemAction::GetParamValue("ShopGatewayURL");
	$shop_login = CSalePaySystemAction::GetParamValue("ShopLogin");
	$shop_pass =  CSalePaySystemAction::GetParamValue("ShopPassword");
	$order_id =  CSalePaySystemAction::GetParamValue("OrderID");
	$order_sum =  str_replace(".","",CSalePaySystemAction::GetParamValue("OrderSum"));
	$order_currency =  CSalePaySystemAction::GetParamValue("OrderCurrency");
	$return_url =  CSalePaySystemAction::GetParamValue("ReturnURL");
	//$language =  CSalePaySystemAction::GetParamValue("Language");
	$time_out =  CSalePaySystemAction::GetParamValue("SessionTimeoutSecs");
	$type = CSalePaySystemAction::GetParamValue("Type");
	
	$db_props = CSaleOrderProps::GetList(array(),array("CODE"=>"PaymentAlfaBank_URL"));
	//��������� ����� ������������� �������� ������ � ������������� �����: PaymentAlfaBank_URL
	if ($db_props->Fetch()){
		// ������� �������� �������� PaymentAlfaBank_URL ��� �������� ������
		$db_vals = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $order_id, "CODE" => "PaymentAlfaBank_URL")); 
		if($arVals = $db_vals->Fetch()) {
			$id_props = $arVals['ID'];
			$url = $arVals['VALUE'];
		}
		
		//���� ����� ��� ���������������� � ��������� �����, �� �������� PaymentAlfaBank_URL ������ ���� ��������� ��������� �������� �� "none"
		if(empty($url) || $url=="none")	
		{
			//������� ��� ������� (������������� ��� �������������)
			if($type=="mono")
				$url = $shop_gateway.'/rest/register.do';
			else
				$url = $shop_gateway.'/rest/registerPreAuth.do';
				
			$params = array(
				'userName' => $shop_login,
				'password' => $shop_pass,
				'orderNumber' => $order_id, 
				'amount' => $order_sum,
				'currency' => $order_currency,
				'returnUrl' => $return_url,
                'description' => GetMessage('ORDER_DESCRIPTION', array('#ORDER_ID#' => $order_id)),
				//'language' => $language,
				'sessionTimeoutSecs' => $time_out,
			);
			//���������� ����� � �������
			$result = file_get_contents($url, false, stream_context_create(array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($params)
				)
			)));
			
			$result=json_decode((string)$result, true);
			//��������� ������� ������ ��� ����������� ������ � �������
			if((int)$result["errorCode"]==0)
			{
				//���� �� ���� ������ ������� url �������� ����� ������ ����� 
				$url=(string)$result["formUrl"];
				//��������� ���������� url � �������� PaymentAlfaBank_URL
				CSaleOrderPropsValue::Update($id_props, array("VALUE"=>$url));
				//�������������� �� �������� ����� ������ ����� 
				LocalRedirect($url);
			}
			else
				//����� ������ ��� ����������� ������ �� �����
				echo GetMessage("ERRORS")." ".$result["errorCode"].": ".$result["errorMessage"];
		}
		else { LocalRedirect($url); }
	}
	else{ echo GetMessage("ERRORS_SaleOrderProp"); }
	
?>