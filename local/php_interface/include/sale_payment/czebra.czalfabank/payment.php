<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

	$shop_gateway =  CSalePaySystemAction::GetParamValue("ShopGatewayURL");
	$shop_login = CSalePaySystemAction::GetParamValue("ShopLogin");
	$shop_pass =  CSalePaySystemAction::GetParamValue("ShopPassword");
	$order_id =  CSalePaySystemAction::GetParamValue("OrderID");
	$order_sum =  str_replace(".","",CSalePaySystemAction::GetParamValue("OrderSum"));
	$order_currency =  CSalePaySystemAction::GetParamValue("OrderCurrency");
	$return_url =  CSalePaySystemAction::GetParamValue("ReturnURL");
	//$language =  CSalePaySystemAction::GetParamValue("Language");
	$time_out =  CSalePaySystemAction::GetParamValue("SessionTimeoutSecs");
	$type = CSalePaySystemAction::GetParamValue("Type");
        
        //AlfaAttempts = 0
        //AlfaErrorCode = 0
        //AlfaLastAttemptTime = 0
        
        
        $copyOrder = false;
        
        $db_attempts = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $order_id, "CODE" => "AlfaAttempts")); 
        if($arAttempts = $db_attempts->Fetch()) {
                $id_attempts = $arAttempts['ID'];
                $attempt = $arAttempts['VALUE'];
        } else {
            if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => 'AlfaAttempts'))->Fetch()) {
                $id_attempts = CSaleOrderPropsValue::Add(array(
                   'NAME' => $arProp['NAME'],
                   'CODE' => $arProp['CODE'],
                   'ORDER_PROPS_ID' => $arProp['ID'],
                   'ORDER_ID' => $order_id,
                   'VALUE' => 1,
                ));
                $attempt = 1;
            }
        }    
        
        $db_lastattempttime = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $order_id, "CODE" => "AlfaLastAttemptTime")); 
        if($arLastAttemptTime = $db_lastattempttime->Fetch()) {
                $id_lastattempttime = $arLastAttemptTime['ID'];
                $lastAttemptTime = $arLastAttemptTime['VALUE'];
        } else {
            if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => 'AlfaLastAttemptTime'))->Fetch()) {
                $id_lastattempttime = CSaleOrderPropsValue::Add(array(
                   'NAME' => $arProp['NAME'],
                   'CODE' => $arProp['CODE'],
                   'ORDER_PROPS_ID' => $arProp['ID'],
                   'ORDER_ID' => $order_id,
                   'VALUE' => 0,
                ));
                $lastAttemptTime = 0;
            }
        }

        
        if ($attempt != 0) {
            
            $db_errorcode = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $order_id, "CODE" => "AlfaErrorCode")); 
            if($arErrorCode = $db_errorcode->Fetch()) {
                    $id_errorcode = $arErrorCode['ID'];
                    $errorCode = $arErrorCode['VALUE'];
            } else {
                if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => 'AlfaErrorCode'))->Fetch()) {
                    $id_errorcode = CSaleOrderPropsValue::Add(array(
                       'NAME' => $arProp['NAME'],
                       'CODE' => $arProp['CODE'],
                       'ORDER_PROPS_ID' => $arProp['ID'],
                       'ORDER_ID' => $order_id,
                       'VALUE' => 0,
                    ));
                    $errorCode = 0;
                }
            }
            
            if ($errorCode != 0) {
                $copyOrder = true;
            } else {                
                $maxCardPaymentTime = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time() - (20 * 60));

                if ($lastAttemptTime < $maxCardPaymentTime) {
                    $copyOrder = true;
                }           
            } 

        }

	$db_props = CSaleOrderProps::GetList(array(),array("CODE"=>"PaymentAlfaBank_URL"));
	//��������� ����� ������������� �������� ������ � ������������� �����: PaymentAlfaBank_URL
	if ($db_props->Fetch()){
		// ������� �������� �������� PaymentAlfaBank_URL ��� �������� ������
		$db_vals = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $order_id, "CODE" => "PaymentAlfaBank_URL")); 
		if($arVals = $db_vals->Fetch()) {
			$id_props = $arVals['ID'];
			$url = $arVals['VALUE'];
		}
                
                if ($copyOrder){
                    $url = false;
                    $order_id = $order_id."-".$attempt;
                }

		//���� ����� ��� ���������������� � ��������� �����, �� �������� PaymentAlfaBank_URL ������ ���� ��������� ��������� �������� �� "none"
		if(empty($url) || $url=="none")	
		{
			//������� ��� ������� (������������� ��� �������������)
			if($type=="mono")
				$url = $shop_gateway.'/rest/register.do';
			else
				$url = $shop_gateway.'/rest/registerPreAuth.do';
				
			$params = array(
				'userName' => $shop_login,
				'password' => $shop_pass,
				'orderNumber' => $order_id, 
				'amount' => $order_sum,
				'currency' => $order_currency,
				'returnUrl' => $return_url,
                'description' => GetMessage('ORDER_DESCRIPTION', array('#ORDER_ID#' => $order_id)),
				//'language' => $language,
				'sessionTimeoutSecs' => $time_out,
			);
			//���������� ����� � �������
			$result = file_get_contents($url, false, stream_context_create(array(
				'http' => array(
					'method'  => 'POST',
					'header'  => 'Content-type: application/x-www-form-urlencoded',
					'content' => http_build_query($params)
				)
			)));
			
			$result=json_decode((string)$result, true);
                        
                        $result_log = print_r($result,true);

			//��������� ������� ������ ��� ����������� ������ � �������
			if((int)$result["errorCode"]==0)
			{
                            //���� �� ���� ������ ������� url �������� ����� ������ ����� 
                            $url=(string)$result["formUrl"];
                            //��������� ���������� url � �������� PaymentAlfaBank_URL
                            CSaleOrderPropsValue::Update($id_props, array("VALUE"=>$url));
                            CSaleOrderPropsValue::Update($id_attempts, array("VALUE"=>($attempt + 1)));
                            CSaleOrderPropsValue::Update($id_lastattempttime, array("VALUE"=>(date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL"))))));
                            //�������������� �� �������� ����� ������ ����� 
                            LocalRedirect($url);
			}
			else {
                            //����� ������ ��� ����������� ������ �� �����
                            CSaleOrderPropsValue::Update($id_attempts, array("VALUE"=>($attempt + 1)));
                            CSaleOrderPropsValue::Update($id_lastattempttime, array("VALUE"=>(date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL"))))));
                            CSaleOrderPropsValue::Update($id_errorcode, array("VALUE"=>$result["errorCode"]));                            
                            echo GetMessage("ERRORS")." ".$result["errorCode"].": ".$result["errorMessage"];
                        }        
		}
		else { LocalRedirect($url); }
	}
	else{ echo GetMessage("ERRORS_SaleOrderProp"); }
?>