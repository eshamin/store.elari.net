<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?
include(GetLangFileName(dirname(__FILE__)."/", "/payment.php"));

$psTitle = GetMessage("SPCP_DTITLE");
$psDescription = GetMessage("SPCP_DDESCR");

$arPSCorrespondence = array(
		"ShopGatewayURL" => array(
				"NAME" => GetMessage("ShopGatewayURL"),
				"DESCR" => GetMessage("ShopGatewayURL_DESCR"),
				"VALUE" => "https://test.paymentgate.ru/testpayment/",
				"TYPE" => ""
			),
		"ShopLogin" => array(
				"NAME" => GetMessage("ShopLogin"),
				"DESCR" => GetMessage("ShopLogin_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),
		"ShopPassword" => array(
				"NAME" => GetMessage("ShopPassword"),
				"DESCR" => GetMessage("ShopPassword_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),
		"OrderID" => array(
				"NAME" => GetMessage("OrderID"),
				"DESCR" => GetMessage("OrderID_DESCR"),
				"VALUE" => "ID",
				"TYPE" => "ORDER"
			),	
		"OrderSum" => array(
				"NAME" => GetMessage("OrderSum"),
				"DESCR" => GetMessage("OrderSum_DESCR"),
				"VALUE" => "PRICE",
				"TYPE" => "ORDER"
			),	
		"OrderCurrency" => array(
				"NAME" => GetMessage("OrderCurrency"),
				"DESCR" => GetMessage("OrderCurrency_DESCR"),
				"VALUE" => "810",
				"TYPE" => ""
			),	
		"ReturnURL" => array(
				"NAME" => GetMessage("ReturnURL"),
				"DESCR" => GetMessage("ReturnURL_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),	
		"Type" => array(
				"NAME" => GetMessage("Type"),
				"DESCR" => GetMessage("Type_DESCR"),
				"TYPE" => "SELECT",
				"VALUE" => array(
					"mono" => array(
						"NAME" => GetMessage("TYPE_1"),
					),
					"" => array(
						"NAME" => GetMessage("TYPE_2"),
					),
				),
				
			),
		/*"Language" => array(
				"NAME" => GetMessage("Language"),
				"DESCR" => GetMessage("Language_DESCR"),
				"VALUE" => "",
				"TYPE" => ""
			),*/	
		"SessionTimeoutSecs" => array(
				"NAME" => GetMessage("SessionTimeoutSecs"),
				"DESCR" => GetMessage("SessionTimeoutSecs_DESCR"),
				"VALUE" => "1200",
				"TYPE" => ""
			),	
	);
?>
