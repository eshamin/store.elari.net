<?
//require_once($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/ims.payture/payment/payment.php");
?>

<?
/*
 * 1C-Bitrix
 * Модуль для подключения платежной системы Payture
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("ims.payture")) return;
IncludeModuleLangFile(__FILE__);

$CSalePaySystemAction=new CSalePaySystemAction();
// params
$Sum = $CSalePaySystemAction->GetParamValue("SHOULD_PAY", "");
$ShopID = 'SmallMallOpen3DS616';//$CSalePaySystemAction->GetParamValue("SHOP_ID", "");
$host = $CSalePaySystemAction->GetParamValue("HOST_CONNECT", "sandbox.payture.com");
$customerNumber = $CSalePaySystemAction->GetParamValue("ORDER_ID", "");
$orderDate = $CSalePaySystemAction->GetParamValue("ORDER_DATE", "");
$orderNumber = $CSalePaySystemAction->GetParamValue("ORDER_ID", "");
$SessionType = $CSalePaySystemAction->GetParamValue("SESSION_TYPE", "pay");
$FinalUrl = "http://store.elari.net/personal/order/payment/receive/?ID={orderid}&result={success}";//$CSalePaySystemAction->GetParamValue("FINAL_URL", "/");
$ViewType = $CSalePaySystemAction->GetParamValue("VIEW_TYPE", "");

// iframe
$iframe_width = $CSalePaySystemAction->GetParamValue("IFRAME_WIDTH", "0");
$iframe_height = $CSalePaySystemAction->GetParamValue("IFRAME_HEIGHT", "0");
//
//debugfile($Sum);
$Sum_print = number_format($Sum, 2, '.', '');
$Sum = number_format($Sum, 2, '', '');

$orderId = $orderNumber.'-'.randString(5);

// init payture
if (!$PAYTURE_PAYMENT) {
$initUrl = "/apim/Init";
    CModule::IncludeModule("sale");
    $arOrderDB = CSaleOrder::GetList(array('ID' => 'DESC'), array('ID' => $orderNumber));
    $arOrder = $arOrderDB->fetch();
    $dbBasketItems = CSaleBasket::GetList(
        array("NAME" => "ASC"),
        array("ORDER_ID" => $arOrder['ID']),
        false,
        false,
        array("*")
    );

    while ($arBasketItems = $dbBasketItems->Fetch()) {
        $cur_product['Quantity'] = $arBasketItems['QUANTITY'];
        $cur_product['Price'] = $arBasketItems['PRICE'];
        $cur_product['Tax'] = 6;
        $cur_product['Text'] = mb_substr($arBasketItems['NAME'], 0, 120);
        $arCheck['Positions'][] = $cur_product;
    }

    if ($arOrder['PRICE_DELIVERY'] > 0) {
        $cur_product['Quantity'] = 1;
        $cur_product['Price'] = $arOrder['PRICE_DELIVERY'];
        $cur_product['Tax'] = 6;
        $cur_product['Text'] = 'Доставка';
        $arCheck['Positions'][] = $cur_product;
    }
    //$arCheck['CheckClose'] = 0;
    $arCheck['CustomerContact'] = $arOrder['USER_EMAIL'];
    $arCheck['Message'] = "Оплата Payture StoreElari";
    $Cheque = base64_encode(json_encode($arCheck));
    debugfile($arCheck);
    $request_data = array('Cheque' => $Cheque);
	AddMessage2Log('$Cheque = ' . print_r($Cheque, true),'');//
$initData = urlencode('SessionType='.$SessionType.';Cheque='.$Cheque.';OrderId='.$orderId.';Product=Заказ №'.$orderNumber.';Amount='.$Sum.';IP='.$_SERVER['REMOTE_ADDR'].';Url='.$FinalUrl.';Total='.$Sum_print.';OrderNumber='.$orderNumber);
$initVars = 'Key='.$ShopID.'&Data='.$initData;
debugfile($initVars);
$initResult = QueryGetData($host, 443, $initUrl, $initVars, $errno, $errstr, "POST", "ssl://");

if ($initResult <> "")
{
// reject payture xml
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
	$initXML = new CDataXML();
	$initXML->LoadString($initResult);

	$arInitResult = $initXML->GetArray();
	if (count($arInitResult)>0 && $arInitResult["Init"]["@"]["Success"] == "True")
	{
	 	//$_SESSION['PAYTURE_PAYMENT_'.$orderNumber] = $arInitResult["Init"]["@"]["SessionId"];
		$PAYTURE_PAYMENT = $arInitResult["Init"]["@"]["SessionId"];
	}
	else {//var_dump('SessionType='.$SessionType.';Cheque='.$Cheque.';OrderId='.$orderId.';Product=Заказ №'.$orderNumber.';Amount='.$Sum.';IP='.$_SERVER['REMOTE_ADDR'].';Url='.$FinalUrl.';Total='.$Sum_print.';OrderNumber='.$orderNumber));
		ShowError('Ошибка инициализации: '. $arInitResult["Init"]["@"]["ErrCode"]);
	}
}
}

// load payture interface
//if ($_SESSION['PAYTURE_PAYMENT_'.$orderNumber] <> "")
if ($PAYTURE_PAYMENT)
{
$PayAddress = 'https://'.$host.'/apim/Pay?SessionId='.$PAYTURE_PAYMENT;
if ($ViewType == "iframe") {
?>
<iframe src="<?=$PayAddress?>" width="<?=$iframe_width?>px" height="<?=$iframe_height?>px" frameBorder="0"></iframe>
<?
}
if ($ViewType == "button") {
?>
<button onclick="window.open('<?=CUtil::JSEscape($PayAddress)?>')">Оплатить</button>
<?
}
if ($ViewType == "current") {
?>
<script type="text/javascript">
window.top.location.href='<?=CUtil::JSEscape($PayAddress)?>';

</script>
<?
}
}
?>