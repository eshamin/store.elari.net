<?
require_once($_SERVER['DOCUMENT_ROOT'] . "/bitrix/modules/main/include/prolog_before.php");

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die("no prolog");

$errorCodesList = array(
    'ACCESS_DENIED' => 'Доступ с текущего IP или по указанным параметрам запрещен',
    'AMOUNT_ERROR' => 'Неверно указана сумма транзакции',
    'AMOUNT_EXCEED' => 'Сумма транзакции превышает доступный остаток средств на выбранном счете',
    'AMOUNT_EXCEED_BALANCE' => 'Сумма транзакции превышает доступный остаток средств на выбранном счете',
    'API_NOT_ALLOWED' => 'Данный API не разрешен к использованию',
    'COMMUNICATE_ERROR' => 'Ошибка возникла при передаче данных в МПС',
    'DUPLICATE_ORDER_ID' => 'Номер заказа уже использовался ранее',
    'DUPLICATE_CARD' => 'Карта уже зарегистрирована',
    'DUPLICATE_USER' => 'Пользователь уже зарегистрирован',
    'EMPTY_RESPONSE' => 'Ошибка процессинга',
    'FORMAT_ERROR' => 'Неверный формат переданных данных',
    'FRAUD_ERROR' => 'Недопустимая транзакция согласно настройкам антифродового фильтра',
    'FRAUD_ERROR_BIN_LIMIT' => 'Превышен лимит по карте(BINу, маске) согласно настройкам антифродового фильтра',
    'FRAUD_ERROR_BLACKLIST_BANKCOUNTRY' => 'Страна данного BINа находится в черном списке или не находится в списке допустимых стран',
    'FRAUD_ERROR_BLACKLIST_AEROPORT' => 'Аэропорт находится в черном списке',
    'FRAUD_ERROR_BLACKLIST_USERCOUNTRY' => 'Страна данного IP находится в черном списке или не находится в списке допустимых стран',
    'FRAUD_ERROR_CRITICAL_CARD' => 'Номер карты(BIN, маска) внесен в черный список антифродового фильтра',
    'FRAUD_ERROR_CRITICAL_CUSTOMER' => 'IP-адрес внесен в черный список антифродового фильтра',
    'ILLEGAL_ORDER_STATE' => 'Попытка выполнения недопустимой операции для текущего состояния платежа',
    'INTERNAL_ERROR' => 'Неправильный формат транзакции с точки зрения сети',
    'INVALID_FORMAT' => 'Неправильный формат транзакции с точки зрения сети',
    'ISSUER_BLOCKED_CARD' => 'Владелец карты пытается выполнить транзакцию, которая для него не разрешена банком-эмитентом, либо произошла внутренняя ошибка эмитента',
    'ISSUER_CARD_FAIL' => 'Банк-эмитент запретил интернет транзакции по карте',
    'ISSUER_FAIL' => 'Владелец карты пытается выполнить транзакцию, которая для него не разрешена банком-эмитентом, либо внутренняя ошибка эмитента',
    'ISSUER_LIMIT_FAIL' => 'Предпринята попытка, превышающая ограничения банка-эмитента на сумму или количество операций в определенный промежуток времени',
    'ISSUER_LIMIT_AMOUNT_FAIL' => 'Предпринята попытка выполнить транзакцию на сумму, превышающую (дневной) лимит, заданный банком-эмитентом',
    'ISSUER_LIMIT_COUNT_FAIL' => 'Превышен лимит на число транзакций: клиент выполнил максимально разрешенное число транзакций в течение лимитного цикла и пытается провести еще одну',
    'ISSUER_TIMEOUT' => 'Нет связи с банком эмитентом',
    'LIMIT_EXCHAUST' => 'Время или количество попыток, отведенное для ввода данных, исчерпано',
    'MERCHANT_RESTRICTION' => 'Превышен лимит Магазина или транзакции запрещены Магазину',
    'NOT_ALLOWED' => 'Отказ эмитента проводить транзакцию. Чаще всего возникает при запретах наложенных на карту',
    'OPERATION_NOT_ALLOWED' => 'Действие запрещено',
    'ORDER_NOT_FOUND' => 'Не найдена транзакция',
    'ORDER_TIME_OUT' => 'Время платежа (сессии) истекло',
    'PROCESSING_ERROR' => 'Ошибка функционирования системы, имеющая общий характер. Фиксируется платежной сетью или банком-эмитентом',
    'PROCESSING_TIME_OUT' => 'Таймаут в процессинге',
    'REAUTH_NOT_ALOWED' => 'Изменение суммы авторизации не может быть выполнено',
    'REFUND_NOT_ALOWED' => 'Возврат не может быть выполнен',
    'REFUSAL_BY_GATE' => 'Отказ шлюза в выполнении операции',
    'RETRY_LIMIT_EXCEEDED' => 'Превышено допустимое число попыток произвести возврат (Refund)',
    'THREE_DS_FAIL' => 'Невозможно выполнить 3DS транзакцию',
    'THREE_DS_TIME_OUT' => 'Срок действия транзакции был превышен к моменту ввода данных карты',
    'USER_NOT_FOUND' => 'Пользователь не найден',
    'WRONG_CARD_INFO' => 'Введены неверные параметры карты',
    'WRONG_CARD_PAN' => 'Неверный номер карты',
    'WRONG_CARDHOLDER_NAME' => 'Недопустимое имя держателя карты',
    'WRONG_PARAMS' => 'Неверный набор или формат параметров',
    'WRONG_PAY_INFO' => 'Некорректный параметр PayInfo (неправильно сформирован или нарушена криптограмма)',
    'WRONG_AUTH_CODE' => 'Неверный код активации',
    'WRONG_CARD' => 'Переданы неверные параметры карты, либо карта в недопустимом состоянии',
    'WRONG_CONFIRM_CODE' => 'Неверный код подтверждения',
    'WRONG_USER_PARAMS' => 'Пользователь с такими параметрами не найден'
);

$host = CSalePaySystemAction::GetParamValue("HOST_CONNECT", "secure.payture.com");
$url = 'https://'.$host.'/apim/PayStatus';
//$url = 'https://secure.payture.com/apim/PayStatus';
$params = array(
    'Key' => 'SmallMallOpen3DS616',//CSalePaySystemAction::GetParamValue("SHOP_ID", ""),
    'OrderId' => $_REQUEST["ID"]
);

$queryString = http_build_query($params);
$result = file_get_contents($url."?".$queryString);
$resultXML = new SimpleXMLElement($result);

$orderIdHyphenPos = strpos($resultXML["OrderId"], "-");
if ($orderIdHyphenPos !== false){
    $resultXML["OrderId"] = substr($resultXML["OrderId"], 0, $orderIdHyphenPos);
}

if($resultXML['Success'] == "True"){
    $message = "Заказ успешно оплачен";
    CSaleOrder::PayOrder((int)$resultXML['OrderId'], "Y");
} else {
    $errorDescription =  $errorCodesList[(string)$resultXML['ErrCode']] ? $errorCodesList[(string)$resultXML['ErrCode']] : $resultXML['ErrCode'];
    $message = "Причина отклонения платежа: ".$errorDescription;

    $db_errorcode = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $resultXML['OrderId'], "CODE" => "AlfaErrorCode"));
    if($arErrorCode = $db_errorcode->Fetch()) {
            $id_errorcode = $arErrorCode['ID'];
    }
    CSaleOrderPropsValue::Update($id_errorcode, array('VALUE' => $resultXML['ErrCode']));
}
?>
<?
//Sending transaction data to GA e-commerce
if($resultXML['OrderId'] > 0) {
	$orderId = intval($resultXML['OrderId']);
	$order = CSaleOrder::GetByID($orderId);

	if(!(isset($_SESSION['GA_ORDER_ID_BANKCARD']) && intval($_SESSION['GA_ORDER_ID_BANKCARD']) == $orderId)) {
		$gaOutput = array();
		$site = \CSite::GetByID($order['LID'])->Fetch();

		$gaOutput[] = "ga('require', 'ecommerce', 'ecommerce.js');";
		$gaOutput[] = sprintf(
			"ga('ecommerce:addTransaction', {'id': '%s', 'affiliation': '%s', 'revenue': '%s', 'shipping': '%s', 'tax': '0'});",
			$orderId,
			$site['SITE_NAME'],
			$order['PRICE'],
			$order['PRICE_DELIVERY']
		);

		$db = \CSaleBasket::GetList(
			array("NAME" => "ASC"),
			array("ORDER_ID" => $orderId)
		);
		while ($basket = $db->fetch()) {
			$gaOutput[] = sprintf(
				"ga('ecommerce:addItem', {'id': '%s', 'name': '%s', 'sku': '%s', 'category': '', 'price': '%s', 'quantity': '%s'});",
				$orderId,
				$basket['NAME'],
				$basket['PRODUCT_ID'],
				$basket['PRICE'],
				$basket['QUANTITY']
			);
		}
		$gaOutput[] = "ga('ecommerce:send');";

		$_SESSION['GA_ORDER_ID_BANKCARD'] = $orderId;

		$APPLICATION->AddViewContent('GA_ECOMMERCE', implode("\n", $gaOutput));
	}
}
?>
<section class="shopping-box">
	<div class="wrapper">
        <div class="ordering_thanks<?=$resultXML['Success'] == 'False' ? ' error':''?>">
            <h1><?=$resultXML['Success'] == "True" ? 'Спасибо за заказ!':'Платеж отклонен :('?></h1>
            <? if($resultXML['Success'] == "True" && $resultXML['OrderId'] > 0) { ?>
                <div class="ordering_thanks__details">
                    <p>Номер вашего заказа № <?=(string)$resultXML['OrderId'] ?>.</p>
                    <p>Вам отправлен e-mail и SMS с номером заказа.</p>
                </div>
            <? } elseif($resultXML['Success'] == "False") {?>
                <div class="receive-error"><?$APPLICATION->IncludeFile(SITE_DIR . 'include/payment_error.php');?>
                    <br><br>или<br><br>Попробуйте <a href="/personal/order/payment/?ORDER_ID=<?=(string)$resultXML['OrderId'] ?>">оплатить снова</a>
                </div>
            <? } ?>
            <div class="ordering_thanks__process">
                <div>Способ оплаты заказа: <b>Банковские карты</b></div>
                <? if($resultXML['Success'] == "False") { ?>
                    <div class="payment-info">
                        <?=$message?>
                    </div>
                <? } ?>
            </div>

            <!--<a class="ordering_thanks__cabinet" href="/personal/orders/">Перейти в личный кабинет</a>-->
        </div>
    </div>
</section>
<script type="text/javascript">
    (function(w, p) {
        var a, s;
        (w[p] = w[p] || []).push({
            counter_id: 423693735,
            tag: '00baed38d362212d641c65de765a5cf7'
        });
        a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
        a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
        s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
    })(window, 'begun_analytics_params');
</script>