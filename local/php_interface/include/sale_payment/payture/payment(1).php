<?
/*
 * 1C-Bitrix
 * Модуль для подключения платежной системы Payture
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if(!CModule::IncludeModule("ims.payture")) return;
IncludeModuleLangFile(__FILE__);

//$CSalePaySystemAction=new CSalePaySystemAction();
//// params
//$Sum = $CSalePaySystemAction->GetParamValue("SHOULD_PAY", "");
//$ShopID = $CSalePaySystemAction->GetParamValue("SHOP_ID", "");
//$host = $CSalePaySystemAction->GetParamValue("HOST_CONNECT", "sandbox.payture.com");
//$orderDate = $CSalePaySystemAction->GetParamValue("ORDER_DATE", "");
//$orderNumber = $CSalePaySystemAction->GetParamValue("ORDER_ID", "");
//$SessionType = $CSalePaySystemAction->GetParamValue("SESSION_TYPE", "pay");
//$FinalUrl = $CSalePaySystemAction->GetParamValue("FINAL_URL", "/");
//$ViewType = $CSalePaySystemAction->GetParamValue("VIEW_TYPE", "");
//$Product = "Оплата заказа №".$orderNumber." на сайте small.ru";
//
//// iframe
//$iframe_width = $CSalePaySystemAction->GetParamValue("IFRAME_WIDTH", "0");
//$iframe_height = $CSalePaySystemAction->GetParamValue("IFRAME_HEIGHT", "0");
////
//
//
//$Sum_print = number_format($Sum, 2, '.', '');
//$Sum = number_format($Sum, 2, '', '');
//
//$copyOrder = false;
//
//
//$db_attempts = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $orderNumber, "CODE" => "AlfaAttempts"));
//if($arAttempts = $db_attempts->Fetch()) {
//        $id_attempts = $arAttempts['ID'];
//        $attempt = $arAttempts['VALUE'];
//} else {
//    if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => 'AlfaAttempts'))->Fetch()) {
//        $id_attempts = CSaleOrderPropsValue::Add(array(
//           'NAME' => $arProp['NAME'],
//           'CODE' => $arProp['CODE'],
//           'ORDER_PROPS_ID' => $arProp['ID'],
//           'ORDER_ID' => $orderNumber,
//           'VALUE' => 1,
//        ));
//        $attempt = 1;
//    }
//}
//
//
//$db_lastattempttime = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $orderNumber, "CODE" => "AlfaLastAttemptTime"));
//if($arLastAttemptTime = $db_lastattempttime->Fetch()) {
//        $id_lastattempttime = $arLastAttemptTime['ID'];
//        $lastAttemptTime = $arLastAttemptTime['VALUE'];
//} else {
//    if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => 'AlfaLastAttemptTime'))->Fetch()) {
//        $id_lastattempttime = CSaleOrderPropsValue::Add(array(
//           'NAME' => $arProp['NAME'],
//           'CODE' => $arProp['CODE'],
//           'ORDER_PROPS_ID' => $arProp['ID'],
//           'ORDER_ID' => $orderNumber,
//           'VALUE' => 0,
//        ));
//        $lastAttemptTime = 0;
//    }
//}
//
//
//if ($attempt != 0) {
//
//    $db_errorcode = CSaleOrderPropsValue::GetList(array("SORT" => "ASC" ), array("ORDER_ID" => $orderNumber, "CODE" => "AlfaErrorCode"));
//    if($arErrorCode = $db_errorcode->Fetch()) {
//            $id_errorcode = $arErrorCode['ID'];
//            $errorCode = $arErrorCode['VALUE'];
//    } else {
//        if ($arProp = CSaleOrderProps::GetList(array(), array('CODE' => 'AlfaErrorCode'))->Fetch()) {
//            $id_errorcode = CSaleOrderPropsValue::Add(array(
//               'NAME' => $arProp['NAME'],
//               'CODE' => $arProp['CODE'],
//               'ORDER_PROPS_ID' => $arProp['ID'],
//               'ORDER_ID' => $orderNumber,
//               'VALUE' => 'no',
//            ));
//            $errorCode = 'no';
//        }
//    }
//
//
//
//    if ($errorCode != 'no') {
//        $copyOrder = true;
//    }
//
//    /*else {
//        $maxCardPaymentTime = date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL")), time() - (20 * 60));
//
//        if ($lastAttemptTime < $maxCardPaymentTime) {
//            $copyOrder = true;
//        }
//    } */
//
//}
//
//if ($copyOrder) {
//    $orderNumber = $orderNumber."-".$attempt;
//}
//
//
//// init payture
//if (!$_SESSION['PAYTURE_PAYMENT_'.$orderNumber]) {
//    $initUrl = "/apim/Init";
//    $initData = urlencode('SessionType='.$SessionType.';OrderId='.$orderNumber.';Amount='.$Sum.';IP='.$_SERVER['REMOTE_ADDR'].';Url='.$FinalUrl.';Total='.$Sum_print.';Product='.$Product.';Language=Ru');
//    $initVars = 'Key='.$ShopID.'&Data='.$initData;
//    $initResult = QueryGetData($host, 443, $initUrl, $initVars, $errno, $errstr, "GET", "ssl://");
//    if ($initResult <> "") {
//        // reject payture xml
//        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
//        $initXML = new CDataXML();
//        $initXML->LoadString($initResult);
//        $arInitResult = $initXML->GetArray();
//        if (count($arInitResult)>0 && $arInitResult["Init"]["@"]["Success"] == "True") {
//            $_SESSION['PAYTURE_PAYMENT_'.$orderNumber] = $arInitResult["Init"]["@"]["SessionId"];
//        } else {
//            CSaleOrderPropsValue::Update($id_attempts, array("VALUE"=>($attempt + 1)));
//            CSaleOrderPropsValue::Update($id_lastattempttime, array("VALUE"=>(date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL"))))));
//            CSaleOrderPropsValue::Update($id_errorcode, array("VALUE"=>$arInitResult["Init"]["@"]["ErrCode"]));
//            ShowError('Ошибка инициализации: '.$arInitResult["Init"]["@"]["ErrCode"]);
//        }
//    }
//}
//// load payture interface
//if ($_SESSION['PAYTURE_PAYMENT_'.$orderNumber] <> "") {
//    $PayAddress = 'https://'.$host.'/apim/Pay?SessionId='.$_SESSION['PAYTURE_PAYMENT_'.$orderNumber];
//
//    CSaleOrderPropsValue::Update($id_attempts, array("VALUE"=>($attempt + 1)));
//    CSaleOrderPropsValue::Update($id_lastattempttime, array("VALUE"=>(date($DB->DateFormatToPHP(CSite::GetDateFormat("FULL"))))));
//
//    if ($ViewType == "iframe") {
//        ?>
<!--        <iframe src="--><?//=$PayAddress?><!--" width="--><?//=$iframe_width?><!--px" height="--><?//=$iframe_height?><!--px" frameBorder="0"></iframe>-->
<!--        --><?//
//    }
//    if ($ViewType == "button") {
//        ?>
<!--        <div id="payture-payment">-->
<!--	        Вы будете автоматически перенаправлены на страницу оплаты через <span class="countdown-timer"></span> сек.<br>-->
<!--	        Если не хотите ждать, нажмите эту кнопку:-->
<!--			<div style="margin: 20px 0 100px;"><a href="--><?//=$PayAddress?><!--" class="form-button">Перейти к оплате</a></div>-->
<!--		</div>-->
<!--		<script>-->
<!--		    $(function(){-->
<!--		        var s = 10;-->
<!--		        var spanTimer = $('#payture-payment span.countdown-timer');-->
<!--		        spanTimer.html(s);-->
<!--		        var timer = setInterval(function(){-->
<!--		            spanTimer.html(--s);-->
<!--		        }, 1000);-->
<!--		        setTimeout(function(){-->
<!--		            $('#payture-payment a')[0].click();-->
<!--		            clearInterval(timer);-->
<!--		        }, s * 1000);-->
<!---->
<!--		    });-->
<!--		</script>-->
<!--        --><?//
//    }
//    if ($ViewType == "current") {
//        LocalRedirect($PayAddress);
//    }
//}
$CSalePaySystemAction=new CSalePaySystemAction();
// params
$Sum = $CSalePaySystemAction->GetParamValue("SHOULD_PAY", "");
$ShopID = $CSalePaySystemAction->GetParamValue("SHOP_ID", "");
$host = $CSalePaySystemAction->GetParamValue("HOST_CONNECT", "sandbox.payture.com");
$customerNumber = $CSalePaySystemAction->GetParamValue("ORDER_ID", "");
$orderDate = $CSalePaySystemAction->GetParamValue("ORDER_DATE", "");
$orderNumber = $CSalePaySystemAction->GetParamValue("ORDER_ID", "");
$SessionType = $CSalePaySystemAction->GetParamValue("SESSION_TYPE", "pay");
$FinalUrl = $CSalePaySystemAction->GetParamValue("FINAL_URL", "/");
$ViewType = $CSalePaySystemAction->GetParamValue("VIEW_TYPE", "");

// iframe
$iframe_width = $CSalePaySystemAction->GetParamValue("IFRAME_WIDTH", "0");
$iframe_height = $CSalePaySystemAction->GetParamValue("IFRAME_HEIGHT", "0");
//

$Sum_print = number_format($Sum, 2, '.', '');
$Sum = number_format($Sum, 2, '', '');

$orderId = $orderNumber.'-'.randString(5);

// init payture
if (!$PAYTURE_PAYMENT) {
    $initUrl = "/apim/Init";
    CModule::IncludeModule("sale");
    $arOrderDB = CSaleOrder::GetList(array('ID' => 'DESC'), array('ACCOUNT_NUMBER' => $orderNumber));
    $arOrder = $arOrderDB->fetch();
    $dbBasketItems = CSaleBasket::GetList(
        array("NAME" => "ASC"),
        array("ORDER_ID" => $arOrder['ID']),
        false,
        false,
        array("*")
    );
    while ($arBasketItems = $dbBasketItems->Fetch()) {
        $cur_product['Quantity'] = $arBasketItems['QUANTITY'];
        $cur_product['Price'] = $arBasketItems['PRICE'];
        $cur_product['Tax'] = 6;
        $cur_product['Text'] = $arBasketItems['NAME'];
        $arCheck['Positions'][] = $cur_product;
    }

    //$arCheck['CheckClose'] = 0;
    $arCheck['CustomerContact'] = $arOrder['USER_EMAIL'];
    $arCheck['Message'] = "Оплата Payture";
    $Cheque = base64_encode(json_encode($arCheck));
    $request_data = array('Cheque' => $Cheque);
    AddMessage2Log('$Cheque = '.print_r($Cheque, true),'');//
    $initData = urlencode('SessionType='.$SessionType.';Cheque='.$Cheque.';OrderId='.$orderId.';Product=Заказ №'.$orderNumber.';Amount='.$Sum.';IP='.$_SERVER['REMOTE_ADDR'].';Url='.$FinalUrl.';Total='.$Sum_print.';OrderNumber='.$orderNumber);
    $initVars = 'Key='.$ShopID.'&Data='.$initData;
    $initResult = QueryGetData($host, 443, $initUrl, $initVars, $errno, $errstr, "GET", "ssl://");

    if ($initResult <> "")
    {
// reject payture xml
        require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
        $initXML = new CDataXML();
        $initXML->LoadString($initResult);
        $arInitResult = $initXML->GetArray();
        if (count($arInitResult)>0 && $arInitResult["Init"]["@"]["Success"] == "True")
        {
            //$_SESSION['PAYTURE_PAYMENT_'.$orderNumber] = $arInitResult["Init"]["@"]["SessionId"];
            $PAYTURE_PAYMENT = $arInitResult["Init"]["@"]["SessionId"];
        }
        else {//var_dump('SessionType='.$SessionType.';Cheque='.$Cheque.';OrderId='.$orderId.';Product=Заказ №'.$orderNumber.';Amount='.$Sum.';IP='.$_SERVER['REMOTE_ADDR'].';Url='.$FinalUrl.';Total='.$Sum_print.';OrderNumber='.$orderNumber));
            ShowError('Ошибка инициализации: '. $arInitResult["Init"]["@"]["ErrCode"]);
        }
    }
}

// load payture interface
//if ($_SESSION['PAYTURE_PAYMENT_'.$orderNumber] <> "")
if ($PAYTURE_PAYMENT)
{
    $PayAddress = 'https://'.$host.'/apim/Pay?SessionId='.$PAYTURE_PAYMENT;
    if ($ViewType == "iframe") {
        ?>
        <iframe src="<?=$PayAddress?>" width="<?=$iframe_width?>px" height="<?=$iframe_height?>px" frameBorder="0"></iframe>
        <?
    }
    if ($ViewType == "button") {
        ?>
        <button onclick="window.open('<?=CUtil::JSEscape($PayAddress)?>')">Оплатить</button>
        <?
    }
    if ($ViewType == "current") {
        ?>
        <script type="text/javascript">
            window.top.location.href='<?=CUtil::JSEscape($PayAddress)?>';

        </script>
        <?
    }
}
?>

