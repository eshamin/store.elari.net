<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

$psTitle = 'Счет small.ru';
$psDescription = '';

$arPSCorrespondence = array(
	"PAYEE_BANK" => array(
		"NAME" => "Банк получателя",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"INN" => array(
		"NAME" => "ИНН",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"KPP" => array(
		"NAME" => "КПП",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"BIC" => array(
		"NAME" => "БИК",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"BIC_ACCOUNT" => array(
		"NAME" => "БИК/Сч. №",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"ACCOUNT" => array(
		"NAME" => "Сч. №",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"PAYEE" => array(
		"NAME" => "Получатель",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"ADDRESS_STRING" => array(
		"NAME" => "Адрес поставщика",
		"DESCR" => "Прим.: 117312, Москва г, Вавилова ул, дом № 17, кв. пом.2Б, тел.: +7 (495) 540-42-66",
		"VALUE" => "",
		"TYPE" => ""
	),
	"CHIEF" => array(
		"NAME" => "Руководитель",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
	"ACCOUNTANT" => array(
		"NAME" => "Бухгалтер",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => ""
	),
    "MANAGER" => array(
        "NAME" => "Менеджер",
        "DESCR" => "",
        "VALUE" => "",
        "TYPE" => ""
    ),
	"PATH_TO_CHIEF_SIGN" => array(
		"NAME" => "Подпись руководителя",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => "FILE"
	),
	"PATH_TO_ACCOUNTANT_SIGN" => array(
		"NAME" => "Подпись бухгалтера",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => "FILE"
	),
	"PATH_TO_STAMP" => array(
		"NAME" => "Печать",
		"DESCR" => "",
		"VALUE" => "",
		"TYPE" => "FILE"
	),
	"LAST_NAME" => array(
		"NAME" => "Фамилия покупателя",
		"DESCR" => "",
		"VALUE" => "LAST_NAME",
		"TYPE" => "PROPERTY"
	),
	"NAME" => array(
		"NAME" => "Имя покупателя",
		"DESCR" => "",
		"VALUE" => " NAME",
		"TYPE" => "PROPERTY"
	)
);