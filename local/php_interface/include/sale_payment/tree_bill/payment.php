<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

global $APPLICATION;

$APPLICATION->RestartBuffer();

$ORDER_ID = IntVal($GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["ID"]);
if (!is_array($arOrder)) {
    $arOrder = CSaleOrder::GetByID($ORDER_ID);
}

$arOrder['DATE_INSERT_FORMATTED'] = FormatDate('j F Y', strtotime($arOrder['DATE_INSERT']));

$xpires = new DateTime($arOrder['DATE_INSERT']);
$xpires->modify("+3 days");
$arOrder['BILL_EXPIRES_DATE'] = $xpires->format('d.m.Y');

if($arOrder['PERSON_TYPE_ID'] == Constants::PERSON_TYPE_LEGAL_ID) {

    $fields = array(
        'TYPE' => '%s',
        'COMPANY' => '"%s",',
        'INN' => 'ИНН %s,',
        'KPP' => 'КПП %s,',
        'ZIP' => '%s,',
        'LOCATION_CITY' => 'г. %s,',
        'STREET' => 'ул. %s,',
        'HOME' => 'д. %s,',
        'OFFICE' => 'офис %s',
    );

    $buyer = array();
    foreach ($fields as $code => $pattern) {
        $val = $GLOBALS['SALE_INPUT_PARAMS']['PROPERTY'][$code];

        if($code == 'LOCATION_CITY' && empty($val)) {
            $val = $GLOBALS['SALE_INPUT_PARAMS']['PROPERTY']['CITY'];
        }

        if (!empty($val)) {
            if (!empty($pattern)) {
                $val = sprintf($pattern, $val);
            }
            $buyer[] = $val;
        }
    }

    $buyer = implode(' ', $buyer);

} else {
    $buyer = CSalePaySystemAction::GetParamValue("LAST_NAME") . ' ' . CSalePaySystemAction::GetParamValue("NAME");
}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title></title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width">

	<style>
		body {
			width: 660px;
			font: 12px Arial;
		}

		.clear {
			clear: both;
		}

		.notice {
			text-align: center;
			font-size: 10px;
			padding-bottom: 25px;
		}

		.sm_ttl {
			text-align: center;
			font-weight: bold;
		}

		table {
			width: 100%;
			border-top: 1px solid #000;
			border-left: 1px solid #000;
			border-collapse: collapse;
		}

		table th {
			padding: 7px 0;
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
			font-size: 13px;
		}

		table td {
			padding: 2px 5px;
			border-right: 1px solid #000;
			border-bottom: 1px solid #000;
		}

		table td b {
			font-size: 13px;
		}

		.ttl {
			font-size: 18px;
			font-weight: bold;
			padding: 30px 0 0
		}

		.hr {
			padding: 0;
			margin: 10px 0;
			width: 100%;
			height: 5px;
			border-bottom: 1px solid #000;
		}

		dl {
			padding: 15px 0 20px;
			margin: 0;
		}

		dt {
			float: left;
			width: 80px;
		}

		dd {
			margin-left: 90px;
			font-weight: bold;
			padding-bottom: 7px;
		}

		.signature {
			padding: 20px 0;
			height: 20px;
		}

		.signature_col {
			position: relative;
			float: left;
			width: 240px;
			margin-left: 40px;
			border-bottom: 1px solid #000;
			text-align: center;
		}

		.signature_col:first-child {
			margin-left: 0;
			border-bottom: 0;
			width: 80px;
			text-align: left;
		}

		.signature_col span {
			position: absolute;
			bottom: -14px;
			display: block;
			width: 100%;
			text-align: center;
			font-size: 10px;
		}

		.signature_col img {
			position: absolute;
			top: -45px;
			left: 60px;
		}
	</style>
</head>

<body>
<!--TODO - Счет действителен-->
<div class="notice">
	Внимание! Счет действителен до <?=$arOrder['BILL_EXPIRES_DATE']?>.<br> Оплата данного счета означает согласие с условиями поставки
	товара.<br> Уведомление об оплате обязательно, в противном случае не гарантируется<br>наличие товара на складе.
	Товар отпускается по факту прихода денег<br>на р/с Поставщика, самовывозом, при наличии доверенности и паспорта.
</div>

<div class="sm_ttl">Образец заполнения платежного поручения</div>
<table>
	<tr>
		<td colspan="2" style="border-bottom: 0"><?= CSalePaySystemAction::GetParamValue("PAYEE_BANK"); ?></td>
		<td>БИК</td>
		<td style="border-bottom: 0"><?= CSalePaySystemAction::GetParamValue("BIC"); ?></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 11px">Банк получателя</td>
		<td>Сч. №</td>
		<td><?= CSalePaySystemAction::GetParamValue("BIC_ACCOUNT"); ?></td>
	</tr>
	<tr>
		<td>ИНН <?= CSalePaySystemAction::GetParamValue("INN"); ?></td>
		<td>КПП <?= CSalePaySystemAction::GetParamValue("KPP"); ?></td>
		<td style="border-bottom: 0">Сч. №</td>
		<td style="border-bottom: 0"><?= CSalePaySystemAction::GetParamValue("ACCOUNT"); ?></td>
	</tr>
	<tr>
		<td colspan="2" style="border-bottom: 0"><?= CSalePaySystemAction::GetParamValue("PAYEE"); ?></td>
		<td style="border-bottom: 0"></td>
		<td style="border-bottom: 0"></td>
	</tr>
	<tr>
		<td colspan="2" style="font-size: 11px">Получатель</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td colspan="4" style="border-bottom: 0">Оплата по заказу клиента №<?= $arOrder['ACCOUNT_NUMBER'] ?></td>
	</tr>
	<tr>
		<td colspan="4" style="font-size: 11px">Назначение платежа</td>
	</tr>
</table>

<div class="ttl">Счет на оплату № SO-<?= $arOrder['ACCOUNT_NUMBER'] ?> от <?= $arOrder['DATE_INSERT_FORMATTED'] ?>г.
</div>
<div class="hr"></div>

<dl>
	<dt>Поставщик:</dt>
	<dd><?= CSalePaySystemAction::GetParamValue("PAYEE"); ?>, ИНН <?= CSalePaySystemAction::GetParamValue("INN"); ?>,
		КПП <?= CSalePaySystemAction::GetParamValue("KPP"); ?>
		, <?= CSalePaySystemAction::GetParamValue("ADDRESS_STRING"); ?></dd>
	<dt>Покупатель:</dt>
	<dd><?= $buyer ?></dd>
</dl>
<?

$arBasketItems = CSalePaySystemAction::GetParamValue("BASKET_ITEMS");

if (!is_array($arBasketItems)) {
    $arBasketItems = array();
    $dbBasket = CSaleBasket::GetList(
        array("DATE_INSERT" => "ASC", "NAME" => "ASC"),
        array("ORDER_ID" => $ORDER_ID),
        false, false,
        array("ID", "PRICE", "CURRENCY", "QUANTITY", "NAME", "VAT_RATE", "MEASURE_NAME")
    );
    while ($arBasket = $dbBasket->Fetch()) {
        // props in product basket
        $arProdProps = array();
        $dbBasketProps = CSaleBasket::GetPropsList(
            array("SORT" => "ASC", "ID" => "DESC"),
            array(
                "BASKET_ID" => $arBasket["ID"],
                "!CODE" => array("CATALOG.XML_ID", "PRODUCT.XML_ID")
            ),
            false,
            false,
            array("ID", "BASKET_ID", "NAME", "VALUE", "CODE", "SORT")
        );
        while ($arBasketProps = $dbBasketProps->GetNext()) {
            if (!empty($arBasketProps) && $arBasketProps["VALUE"] != "")
                $arProdProps[] = $arBasketProps;
        }
        $arBasket["PROPS"] = $arProdProps;

        $arBasket["QUANTITY"] = roundEx($arBasket["QUANTITY"], SALE_VALUE_PRECISION);
        $arBasket["MEASURE_NAME"] = $arBasket["MEASURE_NAME"] ? htmlspecialcharsbx($arBasket["MEASURE_NAME"]) : 'шт.';
        $arBasket["VAT_RATE"] = roundEx($arBasket["VAT_RATE"]*100, SALE_VALUE_PRECISION) . "%";

        $arBasket["PRICE_FORMATTED"] = CurrencyFormatNumber($arBasket["PRICE"], $arBasket["CURRENCY"]);
        $arBasket["PRICE_SUM_FORMATTED"] = CurrencyFormatNumber($arBasket["PRICE"] * $arBasket["QUANTITY"], $arBasket["CURRENCY"]);

        $arBasketItems[] = $arBasket;
    }
}


$arCurFormat = CCurrencyLang::GetCurrencyFormat($GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["CURRENCY"]);
$currency = trim(str_replace('#', '', $arCurFormat['FORMAT_STRING']));
?>

<table>
	<tr>
		<th width="35">№</th>
		<th width="360">Товары (работы, услуги)</th>
		<th colspan="2" width="86">Количество</th>
		<th>Цена</th>
		<th>Сумма</th>
	</tr>
    <?
    $i = 0;
    foreach($arBasketItems as &$arBasket) { ?>
        <tr>
            <td align="right"><?=++$i?></td>
            <td><?=$arBasket["NAME"]?></td>
            <td align="right" width="30"><?=$arBasket["QUANTITY"]?></td>
            <td align="right" width="30"><?=$arBasket["MEASURE_NAME"]?></td>
            <td align="right"><?=$arBasket["PRICE_FORMATTED"]?></td>
            <td align="right"><?=$arBasket["PRICE_SUM_FORMATTED"]?></td>
        </tr>
    <? } ?>
	<tr>
		<td colspan="6" style="border-bottom: 0">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="5" align="right" style="border: 0"><b>Итого:</b></td>
		<td colspan="5" align="right" style="border-bottom: 0">
            <b>
				<?=CurrencyFormatNumber($GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["SHOULD_PAY"], $GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["CURRENCY"])?>
			</b>
        </td>
	</tr>
	<tr>
		<td colspan="5" align="right" style="border-right: 0"><b>Без налога (НДС)</b></td>
		<td colspan="5" align="right"><b>-</b></td>
	</tr>

</table>
<br>

<div>
    <?=sprintf(
        "Всего наименований %s, на сумму %s {$currency}",
        count($arBasketItems),
        SaleFormatCurrency(
            $GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["CURRENCY"],
            $GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["SHOULD_PAY"],
            true
        )
    ); ?>
</div>
<div><b>
<? if (in_array($GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["CURRENCY"], array("RUR", "RUB"))) {
	echo Number2Word_Rus($GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["SHOULD_PAY"]);
} else {
	echo SaleFormatCurrency(
		$GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["SHOULD_PAY"],
		$GLOBALS["SALE_INPUT_PARAMS"]["ORDER"]["CURRENCY"]
	);
} ?>
</b></div>
<div class="hr"></div>

<div class="signature">
	<div class="signature_col">
		<b>Руководитель</b>
	</div>
	<div class="signature_col">
		<b>&nbsp;</b>
		<span>подпись</span>
		<?= CFile::ShowImage(CSalePaySystemAction::GetParamValue("PATH_TO_CHIEF_SIGN"), 0, 0, 'style="top: -40px;"'); ?>
	</div>
	<div class="signature_col">
		<b><?= CSalePaySystemAction::GetParamValue("CHIEF"); ?></b>
		<span>расшифровка подписи</span>
	</div>
</div>
<div class="clear"></div>
<div class="signature">
	<div class="signature_col">
		<b>Бухгалтер</b>
	</div>
	<div class="signature_col">
		<b>&nbsp;</b>
		<span>подпись</span>
		<?= CFile::ShowImage(CSalePaySystemAction::GetParamValue("PATH_TO_ACCOUNTANT_SIGN")); ?>
	</div>
	<div class="signature_col">
		<b><?= CSalePaySystemAction::GetParamValue("ACCOUNTANT"); ?></b>
		<span>расшифровка подписи</span>
	</div>
</div>
<div class="clear"></div>
<div class="signature">
	<div class="signature_col">
		<b>Менеджер</b>
	</div>
	<div class="signature_col">
		<b>&nbsp;</b>
		<span>подпись</span>
		<?= CFile::ShowImage(CSalePaySystemAction::GetParamValue("PATH_TO_STAMP")); ?>
	</div>
	<div class="signature_col">
		<b><?= CSalePaySystemAction::GetParamValue("MANAGER"); ?></b>
		<span>расшифровка подписи</span>
	</div>
</div>

</body>
</html><?
die;
