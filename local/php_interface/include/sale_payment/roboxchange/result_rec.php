<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?><?

use \Bitrix\Sale\Order;

CModule::IncludeModule('sale');
$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$entityId = IntVal($request->get("InvId"));
list($orderId, $paymentId) = \Bitrix\Sale\PaySystem\Manager::getIdsByPayment($entityId);
debugfile($orderId);
if ($orderId > 0)
{
	/** @var \Bitrix\Sale\Order $order */
	$order = \Bitrix\Sale\Order::load($orderId);
	if ($order)
	{
		/** @var \Bitrix\Sale\PaymentCollection $paymentCollection */
		$paymentCollection = $order->getPaymentCollection();
		if ($paymentCollection && $paymentId > 0)
		{
			/** @var \Bitrix\Sale\Payment $payment */
			$payment = $paymentCollection->getItemById($paymentId);
			if ($payment)
			{
				$data = \Bitrix\Sale\PaySystem\Manager::getById($payment->getPaymentSystemId());
				$service = new \Bitrix\Sale\PaySystem\Service($data);
				if ($service)
					$service->processRequest($request);
			}
		}

	}
?>
<section class="shopping-box">
	<div class="wrapper">
            <h1>Спасибо за заказ!</h1>
            <div class="ordering_thanks__details">
                <p>Номер вашего заказа № <?=$orderId?>.</p>
                <p>Вам отправлен e-mail и SMS с номером заказа.</p>
                <?if ($arResult['PAY_SYSTEM']['ID'] == 20) {?>
                	<p>В ближайшее время наш менеджер свяжется с вами для уточнения деталей заказа.</p>
                <?}?>
            </div>
            <div class="ordering_thanks__process">
                <div>Способ оплаты заказа: <b>ROBOKASSA</b></div>
                <div class="payment-info">
					<p>Заказ успешно оплачен</p>
                </div>
            </div>
            <!--<a href="/personal/" class="ordering_thanks__cabinet">Перейти в личный кабинет</a>-->

        </div>
    </div>
</section>

<?
}