<?
AddEventHandler('main', 'OnBeforeEventAdd','onBeforeEventAdd');
AddEventHandler('main', 'OnBeforeProlog', 'authBySecret');

function onBeforeEventAdd(&$event, &$lid,array &$fields, &$message_id)
{
    global $USER;

    if (isset($fields['ORDER_REAL_ID'])) {
		$fields['ORDER_ID'] = $fields['ORDER_REAL_ID'];
    }

    if (isset($fields['ORDER_ID'])) {
        $userID = intval(\CSaleOrder::GetByID(intval($fields['ORDER_ID']))['USER_ID']);
    } else {
        $userID = $USER->GetID();
    }

    if($currentUser = \CUser::GetByID($userID)->Fetch()) {
        $secret = md5($currentUser['ID'] . \CMain::GetServerUniqID());
        if($USER->Update($userID, array('UF_AUTH_SECRET' => $secret))) {
            $fields['AUTH_SECRET'] = $secret;
        }
    }

//OLD
//        if($currentUser = \CUser::GetByID($USER->GetID())->Fetch()) {
//            $secret = md5($currentUser['ID'] . \CMain::GetServerUniqID());
//            if($USER->Update($USER->GetID(), array('UF_AUTH_SECRET' => $secret))) {
//                $fields['AUTH_SECRET'] = $secret;
//            }
//        }

    if ($event == "SALE_ORDER_TRACKING_NUMBER") {
        //Добавляем название курьерской службы и ссылку на отслеживание
        if ($arOrder = \CSaleOrder::GetByID($fields['ORDER_ID'])) {
            $fields['ORDER_DATE'] = date("d.m.Y", strtotime($arOrder['DATE_INSERT']));
            if (is_numeric($arOrder['DELIVERY_ID'])) {
                $arDelivery = \CSaleDelivery::GetByID($arOrder['DELIVERY_ID']);
            } else {
                $arSID = explode(":", $arOrder['DELIVERY_ID']);
                $SID = $arSID[0];
                $dbDelivery = \CSaleDeliveryHandler::GetBySID($SID);
                $arDelivery = $dbDelivery->GetNext();
            }
            switch ($arDelivery['NAME']) {
                case "Pony Express":
                    $fields['DELIVERY_SERVICE'] = "<a href='http://www.ponyexpress.ru/trace.php' title='Нажмите для отслеживания доставки на сайте ".$arDelivery['NAME']."'>".$arDelivery['NAME']."</a>";
                    break;
                case "EMS":
                    $fields['DELIVERY_SERVICE'] = "<a href='http://www.emspost.ru/ru/tracking/' title='Нажмите для отслеживания доставки на сайте ".$arDelivery['NAME']."'>".$arDelivery['NAME']."</a>";
                    break;
                case "Почта России":
                    $fields['DELIVERY_SERVICE'] = "<a href='http://www.russianpost.ru/Tracking20/' title='Нажмите для отслеживания доставки на сайте ".$arDelivery['NAME']."'>".$arDelivery['NAME']."</a>";
                    break;
                default:
                    $fields['DELIVERY_SERVICE'] = $arDelivery['NAME'];
            }
        }

        //Отправляем SMS
        $phone = "";
        $dbOrderProps = \CSaleOrderPropsValue::GetList(
            array("SORT" => "ASC"),
            array(
               "ORDER_ID" => $fields['ORDER_ID'],
               "CODE" => array("PERSONAL_PHONE")
            )
        );
        if ($arOrderProps = $dbOrderProps->Fetch()) {
            $phone = $arOrderProps['VALUE'];
        }
        if(\CModule::IncludeModule("imaginweb.sms") && $phone != "") {
            $smsText = "Заказ E_".$fields['ORDER_ID']." отправлен. Служба доставки: ".$arDelivery['NAME'].". Идентификатор отправления: ".$fields['ORDER_TRACKING_NUMBER'].". Ваш Elari Store";
            \CIWebSMS::Send($phone, $smsText);
        }
    }
	//$debugData = print_r($event, true);
	// file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/test.txt", $debugData, FILE_APPEND);
    // Для заказов в 1 клик отправляем SMS со ссылкой на оплату заказа при переводе заказа в статус "Ожидается оплата заказа в 1 клик"

    if ($event == "SALE_STATUS_CHANGED_Z") {
        if ($oneclickphone = getOrderProp(intval($fields['ORDER_ID']), 'PERSONAL_PHONE')) {
            if(\CModule::IncludeModule("imaginweb.sms")) {
				$strShortUri = getShortUri($fields);
				\CIWebSMS::Send($oneclickphone," Для оплаты Вашего заказа E_".$fields['ORDER_ID']." на сайте Elari Store перейдите по ссылке: http://store.elari.net/" . $strShortUri);
            }
        }
    }

    if ($event == "REFLEKTO_ORDER_LIST") {
        $pay_id = \CSaleOrder::GetByID($fields['ORDER_ID']);

        $sendPayIds = [
        	19, // RoboKassa
        	21, // Payture
        	23, // Яндекс.Касса
        ];

        if (in_array($pay_id['PAY_SYSTEM_ID'], $sendPayIds)) {
            if ($oneclickphone = getOrderProp(intval($fields['ORDER_ID']), 'PERSONAL_PHONE')) {
	            if(\CModule::IncludeModule("imaginweb.sms")) {
					$strShortUri = getShortUri($fields);
					\CIWebSMS::Send($oneclickphone," Для оплаты Вашего заказа E_" . $fields['ORDER_ID'] . " на сайте Elari Store перейдите по ссылке: http://store.elari.net/" . $strShortUri);
	            }
            }

	        $dbOrderProps = \CSaleOrderPropsValue::GetList(
	            array("SORT" => "ASC"),
	            array("ORDER_ID" => $fields['ORDER_ID'], "CODE" => array("EMAIL"))
	        );
	        while ($arOrderProps = $dbOrderProps->GetNext()) {
	                $email = $arOrderProps['VALUE'];
			}

			$to = $email;
			$subject = "Оплата Вашего заказа на store.elari.net";
			$message = '
			<html xmlns="http://www.w3.org/1999/xhtml"><head>
				  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
				  <meta name="viewport" content="width=device-width, initial-scale=1.0">
				  <title>Ссылка для оплаты заказа</title>
			</head>

			<body>
			Для оплаты Вашего заказа E_' . $fields["ORDER_ID"] . ' на сайте store.elari.net перейдите по ссылке:<br><a href="http://store.elari.net/personal/order/payment/?ORDER_ID=' . $fields["ORDER_ID"] . '&auth_secret=' . $fields["AUTH_SECRET"] . '"><b>Оплатить</b></a>
			<br><br>
			После оплаты наш менеджер свяжется с Вами и согласует дату доставки заказа.

			</body>
			</html>';

			/* Для отправки HTML-почты вы можете установить шапку Content-type. */
			$headers= "MIME-Version: 1.0\r\n";
			$headers .= "Content-type: text/html; charset=utf-8\r\n";

			/* дополнительные шапки */
			$headers .= "From: orders@elari.net <orders@elari.net>\r\n";

			/* и теперь отправим из */
			mail($to, $subject, $message, $headers);
        }
    }
}

function authBySecret()
{
    global $APPLICATION, $USER;

    if(!$USER->IsAuthorized() && isset($_REQUEST['auth_secret'])) {
        $db = \CUser::GetList($by = 'ID', $order = 'ASC', array('ACTIVE' => 'Y', 'UF_AUTH_SECRET' => $_REQUEST['auth_secret']));
        if($auth = $db->Fetch()) {
            $USER->Authorize($auth['ID']);
            LocalRedirect($APPLICATION->GetCurPageParam("", array("auth_secret")));
        }
    }
}

AddEventHandler("search", "BeforeIndex", "BeforeIndexHandler");
function BeforeIndexHandler($arFields)
{
	if($arFields["MODULE_ID"] == "iblock" && $arFields["PARAM2"] == 5) {
      	$resProp = CIBlockElement::GetProperty(
                                    $arFields["PARAM2"],
                                    $arFields["ITEM_ID"],
                                    array("sort" => "asc"),
                                    Array("CODE" => "ELARI_ACTIVE"));
      	if ($arProp = $resProp->Fetch()) {
      		if ($arProp['VALUE_XML_ID'] != 'Y') {
				$arFields["BODY"] = '';
				$arFields["TITLE"] = '';
			} else {
				$resProp = CIBlockElement::GetProperty(
                            $arFields["PARAM2"],
                            $arFields["ITEM_ID"],
                            array("sort" => "asc"),
                            Array("CODE" => "ELARI_TITLE"));
				if ($arProp = $resProp->Fetch()) {
					$arFields["TITLE"] = $arProp['VALUE'];
				}

				$resProp = CIBlockElement::GetProperty(
                            $arFields["PARAM2"],
                            $arFields["ITEM_ID"],
                            array("sort" => "asc"),
                            Array("CODE" => "ELARI_SUB_TITLE"));
				if ($arProp = $resProp->Fetch()) {
					$arFields["BODY"] = $arProp['VALUE'];
				}

				$resProp = CIBlockElement::GetProperty(
                            $arFields["PARAM2"],
                            $arFields["ITEM_ID"],
                            array("sort" => "asc"),
                            Array("CODE" => "TAGS"));
				while ($arProp = $resProp->Fetch()) {
					if ($arProp['VALUE'] > 0) {
						$arFields["TAGS"] .= $arProp['VALUE_ENUM'] . ', ';
					}
				}
			}
		} else {
			$arFields["BODY"] = '';
			$arFields["TITLE"] = '';
		}
	} else {
		$arFields["BODY"] = '';
		$arFields["TITLE"] = '';
	}
	return $arFields;
}
AddEventHandler("sale", "OnOrderAdd", "SendSaleToTelegramm");

function SendSaleToTelegramm ($ID, $arFields) {

    $price = $arFields["PRICE"];



CModule::IncludeModule('sale');
$order_id = $ID;


 //пробежимся по св-вам заказа, ища e-mail
      $res = CSaleOrderPropsValue::GetOrderProps($order_id);



      while ($row = $res->fetch()) {


             if ($row['CODE']=='EMAIL') {
                $email = $row['VALUE'];
             }
             if ($row['CODE']=='PERSONAL_PHONE') {
                $phone = $row['VALUE'];
             }
             if ($row['CODE']=='NAME') {
                $name = $row['VALUE'];
             }
      }

       $email = $arFields["EMAIL"];
        $phone = $arFields["PERSONAL_PHONE"];
         $name = $arFields["NAME"];







   // $comma_separated = implode(",", $arFields);
   // mail('killbaal@yandex.ru', $comma_separated.'Привет от ККМ','это проверка обработчика'.$ID);

    $url = "https://api.telegram.org/bot669938046:AAFsJCnW--6UvJrKABimBI942pTh9Vo8c3M/sendMessage?chat_id=-1001141395294&parse_mode=HTML&text=Новый заказ STORE.ELARI.ru - ".$ID." | На сумму: <b>".$price."</b>руб. Заказчик: ".$phone." | ".$email." | ".$name;
    $handle = curl_init($url);
    curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($handle, CURLOPT_CONNECTTIMEOUT, 5);
    curl_setopt($handle, CURLOPT_TIMEOUT, 60);
    curl_exec($handle);
    curl_close($handle);

}