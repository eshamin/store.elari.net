<?php
/**
 * Created by ADV/web-engineering co.
 */

namespace Adv\Entity;

use Bitrix\Main\Entity\DataManager;
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Entity\Event;

class PreordersTable extends DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'adv_preorders';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => 'ИД',
            ),
            'UF_TIMESTAMP_X' => array(
                'data_type' => 'datetime',
                'title' => 'Дата изменения',
                'default_value' => new DateTime(),
            ),
            'UF_USER_EMAIL' => array(
                'data_type' => 'string',
                'title' => 'Email',
                'required' => true,
                'validation' => function() {
                    return array(
                        function($value) {
                            if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                return 'Укажите корректный Email!';
                            }
                            return true;
                        }
                    );
                }
            ),
            'UF_USER_NAME' => array(
                'data_type' => 'string',
                'title' => 'Имя пользователя',
                'required' => true
            ),
            'UF_USER_ID' => array(
                'data_type' => 'integer',
                'title' => 'Пользователь',
            ),
            'UF_PRODUCT_ID' => array(
                'data_type' => 'integer',
                'title' => 'Товар',
                'required' => true
            ),
            'UF_PRODUCT_QNT' => array(
                'data_type' => 'integer',
                'title' => 'Количество'
            ),
            'UF_USER_PHONE' => array(
                'data_type' => 'string',
                'title' => 'Телефон',
                'required' => true
            ),
            'UF_USER_CITY' => array(
                'data_type' => 'string',
                'title' => 'Город',
            ),
        );
    }

    public static function onProductUpdate($id, array $fields)
    {
        if($fields['OLD_QUANTITY'] == 0 && $fields['QUANTITY'] > 0) {
            $product = \CIBlockElement::GetByID($id)->GetNext();

            $db = self::getList(array('filter' => array('UF_PRODUCT_ID' => $id)));
            while($subscribe = $db->fetch()) {
                self::delete($subscribe['ID']);
            }

            $productBrandId = \CIBlockElement::GetProperty(5, $id, array('sort' => 'asc'), array('CODE' => 'BRAND'))->GetNext()['VALUE'];
            if ($productBrandId != 24056) {
                file_put_contents($_SERVER['DOCUMENT_ROOT']."/upload/test.txt", "productBrandId = ".$productBrandId);
                \CEvent::Send('ADV_PREORDER', 's2', array(
                        'PRODUCT_NAME' => $product['NAME'],
                        'PRODUCT_URL' => "http://".$_SERVER['SERVER_NAME'].$product['DETAIL_PAGE_URL']
                ));
            }
        }
    }

    public static function onAfterAdd(Event $event)
    {
        global $SERVER_NAME;

        $catalogIblockId = 5;
        $fields = $event->getParameter('fields');

        $product = \CIBlockElement::GetList(array(), array(
            'IBLOCK_ID' => $catalogIblockId,
            'ID' => $fields['UF_PRODUCT_ID']),
            false, false, array('NAME', 'PROPERTY_BRAND'))->Fetch();

        if($product) {

            $bcc = '';
            if(!empty($product['PROPERTY_BRAND_VALUE'])) {
                $brand = \CIBlockElement::GetByID($product['PROPERTY_BRAND_VALUE'])->Fetch();

                if($brand) {
                    $code = sprintf('editors-%s', strtolower($brand['CODE']));
                    $brandUserGroup = \CGroup::GetList ($by = 'ID', $order = 'ASC', array ('STRING_ID' => $code))->Fetch();
                    if($brandUserGroup) {
                        $dbUser = \CUser::GetList($by="id", $order="desc", array('GROUPS_ID' => array($brandUserGroup['ID'])));
                        $spamList = array();
                        while($user = $dbUser->Fetch()) {
                            $spamList[] = $user['EMAIL'];
                        }

                        if(sizeof($spamList)>0) {
                            $bcc = join(', ', $spamList);
                        }
                    }
                }
            }

            if(empty($bcc)) {
                $bcc = \COption::GetOptionString("sale", "order_email", "order@".$SERVER_NAME);
            }


            \CEvent::Send('PREORDER_REQUEST', 's1', array(
                'USER_NAME' => $fields['UF_USER_NAME'],
                'USER_EMAIL' => $fields['UF_USER_EMAIL'],
                'PRODUCT_NAME' => $product['NAME'],
                'BCC' => $bcc
            ));

        }
    }
}