<?php
namespace Phasotron\Sended;

use Bitrix\Main,
    Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

/**
 * Class EmailTable
 *
 * Fields:
 * <ul>
 * <li> id int mandatory
 * <li> order_id int mandatory
 * </ul>
 *
 * @package Bitrix\Sended
 **/

class OrderEmailTable extends Main\Entity\DataManager
{
    /**
     * Returns DB table name for entity.
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'order_sended_email';
    }

    /**
     * Returns entity map definition.
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'id' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true,
                'title' => Loc::getMessage('EMAIL_ENTITY_ID_FIELD'),
            ),
            'order_id' => array(
                'data_type' => 'integer',
                'required' => true,
                'title' => Loc::getMessage('EMAIL_ENTITY_ORDER_ID_FIELD'),
            ),
        );
    }
}