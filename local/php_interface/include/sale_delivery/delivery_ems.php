<?php
/**
 * Created by ADV/web-engineering co.
 */

namespace Adv;

require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/sale/delivery/delivery_ems.php');

class DeliveryEMS extends \CDeliveryEMS
{
    function Init()
    {
        $result = parent::Init();

        return array_merge($result, array(
            'SID' => 'adv_ems',
            'HANDLER' => __FILE__,

            /* Handler methods */
            'DBGETSETTINGS' => array(__CLASS__, 'GetSettings'),
            'DBSETSETTINGS' => array(__CLASS__, 'SetSettings'),
            'GETCONFIG' => array(__CLASS__, 'GetConfig'),

            'COMPABILITY' => array(__CLASS__, 'Compability'),
            'CALCULATOR' => array(__CLASS__, 'Calculate'),
        ));
    }

    function GetConfig()
    {
        $config = parent::GetConfig();

        $config['CONFIG']['IGNORED_GROUPS'] = array(
            'TYPE' => 'MULTISELECT',
            'DEFAULT' => '',
            'TITLE' => 'Группы для которых доставка отключена',
            'VALUES' => array(),
            'GROUP' => 'all',
        );

        $db = \CSaleLocationGroup::GetList();
        while ($group = $db->Fetch()) {
            $config['CONFIG']['IGNORED_GROUPS']['VALUES'][$group['ID']] = $group['NAME'];
        }

        return $config;
    }

    function GetSettings($strSettings)
    {
        $settings = unserialize($strSettings);
        return $settings;
    }

    function SetSettings($arSettings)
    {
        foreach($arSettings['IGNORED_GROUPS'] as $k => $id) {
            if(intval($id) <= 0) {
                unset($arSettings['IGNORED_GROUPS'][$k]);
            } else {
                $arSettings['IGNORED_GROUPS'][$k] = intval($id);
            }
        }

        return serialize($arSettings);
    }

    function Compability($arOrder, $arConfig)
    {
        if(self::isLocationIgnored($arOrder['LOCATION_TO'], $arConfig['IGNORED_GROUPS']['VALUE'])) {
            return array();
        }

        if($profiles = parent::Compability($arOrder, $arConfig)) {
            $result = self::Calculate(reset($profiles), $arConfig, $arOrder, 0);

            if($result['RESULT'] != 'ERROR') {
                return $profiles;
            }
        }

        return array();
    }

    function Calculate($profile, $arConfig, $arOrder, $STEP, $TEMP = false)
    {
        if ($STEP >= 4) {
            return array(
                "RESULT" => "ERROR",
                "TEXT" => GetMessage('SALE_DH_EMS_ERROR_CONNECT'),
            );
        }

        $arLocationFrom = array();

        if ($arOrder["WEIGHT"] <= 0) $arOrder["WEIGHT"] = 1;

        $arLocationTo = self::__GetLocation($arOrder["LOCATION_TO"]);

        if ($arLocationTo['IS_RUSSIAN'] == 'Y')
            $arLocationFrom = self::__GetLocation($arOrder["LOCATION_FROM"]);

        if (isset($arLocationTo['EMS_CITIES_NOT_LOADED'])) {
            // get cities and proceed to next step

            $data = self::__EMSQuery('ems.get.locations', array('type' => 'russia', 'plain' => 'true'));

            if (!is_array($data) || $data['rsp']['stat'] != 'ok' || !is_array($data['rsp']['locations'])) {
                return array(
                    "RESULT" => "ERROR",
                    "TEXT" => GetMessage('SALE_DH_EMS_ERROR_CONNECT'),
                );
            }

            $arCitiesList = array();
            foreach ($data['rsp']['locations'] as $arLocation) {
                $arCitiesList[$arLocation['name']] = $arLocation['value'];
            }

            CheckDirPath(dirname(__FILE__) . "/ems/");
            if ($fp = fopen(dirname(__FILE__) . "/ems/city.php", "w")) {
                fwrite($fp, '<' . "?\r\n");
                fwrite($fp, '$' . "arEMSCityList = array();\r\n");
                foreach ($arCitiesList as $key => $value) {
                    fwrite($fp, '$' . "arEMSCityList['" . addslashes($key) . "'] = '" . htmlspecialcharsbx(trim($value)) . "';\r\n");
                }
                fwrite($fp, '?' . '>');
                fclose($fp);
            }

            return array(
                "RESULT" => "NEXT_STEP",
                "TEXT" => GetMessage('SALE_DH_EMS_CORRECT_CITIES'),
            );
        }

        if (!$arLocationTo['EMS_ID']) {
            if ($arLocationTo['IS_RUSSIAN'] == 'Y')
                $text = str_replace('#CITY#', $arLocationTo['CITY_NAME_ORIG'], GetMessage('SALE_DH_EMS_ERROR_NO_CITY_TO'));
            else
                $text = str_replace('#COUNTRY#', $arLocationTo['COUNTRY_NAME_ORIG'], GetMessage('SALE_DH_EMS_ERROR_NO_COUNTRY_TO'));

            return array(
                "RESULT" => "ERROR",
                "TEXT" => $text,
            );
        }

        if ($arLocationTo['IS_RUSSIAN'] == 'Y' && !$arLocationFrom['EMS_ID']) {
            $text = str_replace('#CITY#', $arLocationFrom['CITY_NAME_ORIG'], GetMessage('SALE_DH_EMS_ERROR_NO_CITY_FROM'));

            return array(
                "RESULT" => "ERROR",
                "TEXT" => $text,
            );
        }


        $cache_id = "sale|8.0.3|ems|" . $profile . "|" . $arConfig["category"]["VALUE"] . "|" . $arOrder["LOCATION_FROM"] . "|" . $arOrder["LOCATION_TO"];

        // 0-0.1,0.1-0.5,0.5-1,1-1.5,1.5-2,2-3....30-31,31-31.5

        if ($arOrder['WEIGHT'] < 100)
            $cache_id .= '|weight_0';
        elseif ($arOrder['WEIGHT'] < 2000)
            $cache_id .= '|weight_half_' . (ceil($arOrder['WEIGHT'] / 1000) * 2);
        elseif ($arOrder['WEIGHT'] < 31000)
            $cache_id .= '|weight_' . (ceil($arOrder['WEIGHT'] / 1000));
        else
            $cache_id .= '|weight_max';

        $obCache = new \CPHPCache();
        if ($obCache->InitCache(DELIVERY_EMS_CACHE_LIFETIME, $cache_id, "/")) {
            $vars = $obCache->GetVars();
            $result = $vars["RESULT"];
            $transit = $vars["TRANSIT"];

            if ($arLocationTo['IS_RUSSIAN'] == 'Y')
                $result += $arOrder["PRICE"] * DELIVERY_EMS_PRICE_TARIFF;

            return array(
                "RESULT" => "OK",
                "VALUE" => $result,
                'TRANSIT' => $transit,
            );
        }

        $arParams = array();

        if ($arLocationTo['IS_RUSSIAN'] != 'Y') {
            $arParams['type'] = $arConfig["category"]["VALUE"];
        } else {
            $arParams['from'] = $arLocationFrom['EMS_ID'];
        }

        $arParams['to'] = $arLocationTo['EMS_ID'];
        $arParams['weight'] = $arOrder['WEIGHT'] / 1000;

        $arParams['plain'] = 'true';

        $data = self::__EMSQuery('ems.calculate', $arParams);

        if (is_array($data) && $data['rsp']['stat'] == 'ok') {
            $obCache->StartDataCache(86400, $cache_id);

            $result = doubleval($data['rsp']['price']);
            $transit = '';
            if ($data['rsp']['term'])
                $transit = $data['rsp']['term']['min'] . '-' . $data['rsp']['term']['max'];

            $obCache->EndDataCache(
                array(
                    "RESULT" => $result,
                    "TRANSIT" => $transit,
                )
            );

            return array(
                "RESULT" => "OK",
                "VALUE" => $data['rsp']['price'],
                'TRANSIT' => $data['rsp']['term']['min'] . '-' . $data['rsp']['term']['max']
            );
        }

        return array(
            "RESULT" => "ERROR",
            "TEXT" => GetMessage('SALE_DH_EMS_ERROR_RESPONSE') . (is_array($data) ? ' (' . $data['rsp']['err']['msg'] . ')' : ''),
        );
    }

    function __GetLocation($location)
    {
        $arLocation = \CSaleLocation::GetByID($location);

        $arLocation["IS_RUSSIAN"] = self::__IsRussian($arLocation) ? "Y" : "N";

        if ($arLocation["IS_RUSSIAN"] == 'Y') {
            static $arEMSCityList;

            if (!is_array($arEMSCityList)) {
                if (file_exists(dirname(__FILE__) . '/ems/city.php'))
                    require_once(dirname(__FILE__) . '/ems/city.php');
            }

            $arLocation['CITY_NAME_ORIG'] = ToUpper($arLocation['CITY_NAME_ORIG']);
            $arLocation['CITY_SHORT_NAME'] = ToUpper($arLocation['CITY_SHORT_NAME']);
            $arLocation['CITY_NAME_LANG'] = ToUpper($arLocation['CITY_NAME_LANG']);
            $arLocation['CITY_NAME'] = ToUpper($arLocation['CITY_NAME']);

            if (is_array($arEMSCityList)) {
                $arrNames = array(ToUpper($arLocation['CITY_NAME_ORIG']),
                    ToUpper($arLocation['CITY_SHORT_NAME']),
                    ToUpper($arLocation['CITY_NAME_LANG']),
                    ToUpper($arLocation['CITY_NAME']),
                    ToUpper($arLocation['REGION_NAME_ORIG']),
                    ToUpper($arLocation['REGION_SHORT_NAME']),
                    ToUpper($arLocation['REGION_NAME_LANG']),
                    ToUpper($arLocation['REGION_NAME'])
                );

                $intNamesCou = count($arrNames);
                for ($i = 0; $i < $intNamesCou; $i++) {
                    if (array_key_exists($arrNames[$i], $arEMSCityList)) {
                        $arLocation['EMS_ID'] = $arEMSCityList[$arrNames[$i]];
                        break;
                    }
                }

                $arLocation['EMS_TYPE'] = 'russia';
            } else {
                $arLocation['EMS_CITIES_NOT_LOADED'] = true;
            }
        }

        return $arLocation;
    }

    protected static function isLocationIgnored($locationId, array $groups)
    {
        $ignoredLocation = Utils::getCached(function() use ($groups) {
            $locations = array();

            foreach($groups as $gid) {
                $db = \CSaleLocationGroup::GetLocationList(array('LOCATION_GROUP_ID' => $gid));
                while($location = $db->Fetch()) {
                    $locations[] = intval($location['LOCATION_ID']);
                }
            }

            return $locations;
        }, array('CACHE_TIME' => 86400, 'CACHE_ID' => serialize($groups), 'CACHE_PATH' => '/adv/ems'));

        if(in_array($locationId, $ignoredLocation)) {
            return true;
        }

        return false;
    }
}