<?
date_default_timezone_set('Europe/Moscow');
COption::SetOptionString("catalog", "DEFAULT_SKIP_SOURCE_CHECK", "Y");
COption::SetOptionString("sale", "secure_1c_exchange", "N");

use Bitrix\Main\Application;
use Bitrix\Main\ArgumentException;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\LoaderException;

require_once('include/debug.php');
require_once('include/handlers.php');

\Bitrix\Main\Loader::registerAutoLoadClasses(
    null,
    array(
        '\\Phasotron\\Sended\\OrderEmailTable' => '/local/php_interface/include/OrderSendedEmail.php',
        '\\Adv\\Entity\\PreordersTable' => '/local/php_interface/include/Preorders.php',
        '\\Adv\\BasketHelper' => '/local/php_interface/include/BasketHelper.php',
    )
);

global $arrFilter;
$arrFilter['PROPERTY_BRAND.NAME'] = 'Elari';
$arrFilter['PROPERTY_ELARI_ACTIVE_VALUE'] = 'Y';

function format($number, $dec = 0)
{
    return number_format($number, $dec, ',', ' ');
}

if (!function_exists('getShortUri')) {
	function getShortUri($fields)
	{
		$strShortUri = CBXShortUri::GenerateShortUri();
		$arFields = Array(
			"URI" => "/personal/order/payment/?ORDER_ID=" . $fields['ORDER_ID'] . "&auth_secret=" . $fields['AUTH_SECRET'],
			"SHORT_URI" => $strShortUri,
			"STATUS" => "301",
		);
		$ID = CBXShortUri::Add($arFields);
		return $strShortUri;
	}
}

function getCached($callback, array $params = array(), $refreshCache = false)
{
    $result = null;
    $cache = Application::getInstance()->getCache();

    if(!isset($params['CACHE_TIME'])) {
        $params['CACHE_TIME'] = 86400 * 31;
    }

    if(!isset($params['CACHE_ID'])) {
        $ref = new \ReflectionFunction($callback);
        $params['CACHE_ID'] = md5($ref->getFileName() . $ref->getStartLine() . $ref->getEndLine());
    }

    if(!isset($params['CACHE_PATH'])) {
        $params['CACHE_PATH'] = '/adv/' . $params['CACHE_ID'];
    }

    if($refreshCache) {
        $cache->clean($params['CACHE_ID'], $params['CACHE_PATH']);
    }

    if($cache->startDataCache($params['CACHE_TIME'], $params['CACHE_ID'], $params['CACHE_PATH']) || $refreshCache) {
        if($params['TAGGED'] === true) {
            Application::getInstance()->getTaggedCache()->startTagCache($params['CACHE_PATH']);
        }

        try {

            if(!$result = $callback()) {
                $cache->abortDataCache();
            } else {
                $cache->endDataCache($result);
            }

        } catch (\Exception $e) {
            $cache->abortDataCache();
            throw $e;
        }

        if($params['TAGGED'] === true) {
            Application::getInstance()->getTaggedCache()->endTagCache();
        }
    } else {
        $result = $cache->getVars();
    }

    return $result;
}

function loadOrderProps($personaTypeId, $code = null, $useCache = true)
{
    $properties = getCached(function() use($personaTypeId) {
        $result = array();

        if(!Loader::includeModule('sale')) {
            throw new LoaderException('Module sale not installed');
        }

        $db = CSaleOrderProps::GetList(
            array('SORT' => 'ASC'),
            array('PERSON_TYPE_ID' => $personaTypeId)
        );

        while($property = $db->Fetch()) {
            $result[$property['CODE']] = $property;
        }

        return $result;

    }, array('CACHE_ID' => __METHOD__ . $personaTypeId, 'CACHE_PATH' => '/adv/order_properties'), !$useCache);

    if(isset($code) && !$properties[$code]) {
        throw new ArgumentException(sprintf('Order property with code "%s" not found!', $code));
    }

    return $code ? $properties[$code]:$properties;
}

function getOrderProp($orderId, $code)
{
    if(!Loader::includeModule('sale')) {
        throw new LoaderException('Module sale not installed');
    }

    if(!$order = \CSaleOrder::GetByID($orderId)) {
        throw new ArgumentException(sprintf('Order #%d not found!', $orderId), 'ORDER');
    }

    $property = loadOrderProps($order['PERSON_TYPE_ID'], $code);

    $db = \CSaleOrderPropsValue::GetList(
        array('SORT' => 'ASC'),
        array('ORDER_ID' => $orderId, 'ORDER_PROPS_ID' => $property['ID']
        )
    );
    if($value = $db->Fetch()) {
        return $value['VALUE'];
    }

    return null;
}

function getDefaultCityId($useGeoIp = true)
{
    global $APPLICATION;

    $cityHash = md5(Application::getInstance()->getContext()->getServer()->get('REMOTE_ADDR'));

    if(!($userCityId = $APPLICATION->get_cookie('USER_CITY_ID')) || $cityHash != $APPLICATION->get_cookie('USER_CITY_HASH')) {
        if(!Loader::includeModule('sale')) {
            throw new LoaderException('Module sale not installed');
        }

        if($useGeoIp && $userCityName = getUserCityByIp()) {
            $db = \CSaleLocation::GetList(
                array('SORT' => 'ASC'),
                array('LID' => LANGUAGE_ID, 'CITY_NAME' => $userCityName)
            );
            if($city = $db->Fetch()) {
                $userCityId = $city['ID'];
            }
        }

        if(!$userCityId) {
            $db = \CSaleLocation::GetList(
                array('SORT' => 'ASC'),
                array('LID' => LANGUAGE_ID, 'LOC_DEFAULT' => 'Y')
            );
            if($city = $db->Fetch()) {
                $userCityId = $city['ID'];
            }
        }

        $APPLICATION->set_cookie('USER_CITY_HASH', $cityHash);
        $APPLICATION->set_cookie('USER_CITY_ID', $userCityId);
    }

    return intval($userCityId);
}

function getUserCityByIp($ip = null)
{
    global $APPLICATION;

    if(!$ip) {
        $ip = Application::getInstance()->getContext()->getServer()->get('REMOTE_ADDR');
    }

    $ipHash = md5($ip);

    if(!($city = $APPLICATION->get_cookie('USER_CITY_NAME')) || $ipHash != $APPLICATION->get_cookie('USER_CITY_HASH')) {
        $city = null;
        $url = sprintf('http://ipgeobase.ru:7020/geo?ip=%s', $ip);

        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_HEADER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_TIMEOUT => 2,
            CURLOPT_CONNECTTIMEOUT => 1,
            CURLOPT_FOLLOWLOCATION => true
        ));

        if($response = curl_exec($ch)) {
            $response = iconv('windows-1251', 'utf-8', $response);
            if(preg_match('#<city>(.*)</city>#is', $response, $match)) {
                $city = trim($match[1]);
                $APPLICATION->set_cookie('USER_CITY_NAME', $city);
                $APPLICATION->set_cookie('USER_CITY_HASH', $ipHash);
            }
        }
    }

    return $city;
}
?>