<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?
$arFilter = [
	'ACTIVE' => 'Y',
	'IBLOCK_ID' => 23,
	'PROPERTY_URL' => $APPLICATION->GetCurPage(),
];

$arSelect = ['NAME', 'PREVIEW_TEXT', 'PREVIEW_PICTURE', 'PROPERTY_URL'];

$arSeo = CIBlockElement::GetList([], $arFilter, false, false, $arSelect)->Fetch();
if ($arSeo) {
?>

<div class="seo-text text">
    <img src="<?=CFile::GetPath($arSeo['PREVIEW_PICTURE'])?>" />
    <?=$arSeo['PREVIEW_TEXT']?>
</div>
<?}?>