<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if (empty($arResult))
	return '<div class="breadcrumbs">
    <a href="/" class="icon-home">Главная</a>';

$strReturn = '';

$strReturn .= '<div class="breadcrumbs">
    <a href="/" class="icon-home">Главная</a>';

$itemSize = count($arResult);
for ($index = 0; $index < $itemSize; $index++) {
	$title = htmlspecialcharsex($arResult[$index]["TITLE"]);

	$arrow = '<span>/</span>';

	if ($arResult[$index]["LINK"] <> "" && $index != $itemSize-1) {
		$strReturn .= $arrow . '
				<a href="' . $arResult[$index]["LINK"] . '" title="' . $title . '">
					' . $title . '
				</a>' ;
	}
	else {
		$strReturn .= $arrow . '
				<span>' . $title . '</span>';
	}
}

$strReturn .= '</div>';

return $strReturn;
