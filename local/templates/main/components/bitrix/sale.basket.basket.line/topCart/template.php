<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
 ?>

<label>В корзине:</label>
<a href="/personal/cart/" class="link-shop"><span><?=$arResult['NUM_PRODUCTS']?></span><i> <?=$arResult['PRODUCT(S)']?></i></a>