<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @var array $arUrls */
/** @var array $arHeaders */
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult["ERROR_MESSAGE"]))
	ShowError($arResult["ERROR_MESSAGE"]);

$bDelayColumn  = false;
$bDeleteColumn = false;
$bWeightColumn = false;
$bPropsColumn  = false;
$bPriceType    = false;

if ($normalCount > 0) {
?>

<section class="shopping-box">
    <div class="wrapper">
        <div class="shopping-cart-wrapper">
	        <div class="right">
	            <div class="shopping-resume">
	                <div class="shop-w">
	                    <!--<h3>Способы доставки</h3>
	                    <div class="form-group">
	                        <label class="radio">
	                            <input type="radio" value="1" name="shipping[]" checked />
	                            <span>Самовывоз (пр. Вернадского)</span>
	                            <em>Забрать самостоятельно из шоу-рума Small Mall в Москве, проспект Вернадского, 29. Время работы: пн-пт, 9.30-18.30.</em>
	                        </label>
	                        <label class="radio">
	                            <input type="radio" value="2" name="shipping[]" />
	                            <span>Доставка курьером по Москве</span>
	                            <em>Доставка осуществляется на следующий рабочий день после оформления заказа. Стоимость 300 руб.(в пределах МКАД)</em>
	                        </label>
	                        <label class="radio">
	                            <input type="radio" value="3" name="shipping[]" />
	                            <span>Major Express</span>
	                            <em>Экспресс доставка курьерской службой Pony Express</em>
	                        </label>
	                    </div>-->
	                    <div class="buttons promo-code-box">
	                        <div>
	                            <input type="text" id="coupon" name="COUPON" placeholder="Промокод на скидку" value="<?=$arResult['COUPON']?>" />
	                        </div>
	                        <div>
	                            <a href="javascript:void(0)" class="button" onclick="enterCoupon();">Применить</a>
	                        </div>
	                    </div>
	                    <div class="shopping-total">
	                        <table>
	                            <tr>
	                                <th>Товары:</th>
	                                <td><span class="price"><?=str_replace(" ", "&nbsp;", $arResult['PRICE_WITHOUT_DISCOUNT'])?></span></td>
	                            </tr>
	                            <tr>
	                                <th>Скидка:</th>
	                                <td><span class="price"><?=str_replace(" ", "&nbsp;", $arResult['DISCOUNT_PRICE_ALL_FORMATED'])?></span></td>
	                            </tr>
	                            <tr>
	                                <th class="large">ИТОГО:</th>
	                                <td class="large"><span class="price" id="allSum_FORMATED"><?=str_replace(" ", "&nbsp;", $arResult["allSum_FORMATED"])?></span></td>
	                            </tr>
	                        </table>
	                    </div>
	                </div>
	                <div class="buttons-final">
	                    <button class="btn-order" onclick="checkOut();">Оформить заказ</button>
	                </div>
	            </div>
	        </div>
	<div id="basket_items_list" class="left">
		<div class="bx_ordercart_order_table_container">
			<table id="basket_items" class="shopping-cart-table">
				<thead>
					<tr>
						<?
						foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader):
							$arHeaders[] = $arHeader["id"];

							// remember which values should be shown not in the separate columns, but inside other columns
							if (in_array($arHeader["id"], array("TYPE")))
							{
								$bPriceType = true;
								continue;
							}
							elseif ($arHeader["id"] == "PROPS")
							{
								$bPropsColumn = true;
								continue;
							}
							elseif ($arHeader["id"] == "DELAY")
							{
								$bDelayColumn = true;
								continue;
							}
							elseif ($arHeader["id"] == "DELETE")
							{
								$bDeleteColumn = true;
								continue;
							}
							elseif ($arHeader["id"] == "WEIGHT")
							{
								$bWeightColumn = true;
							}

							if ($arHeader["id"] == "NAME") {
							?>
								<th class="item col-img" id="col_<?=$arHeader["id"];?>">Товар</th>
			                    <th class="col-title">&nbsp;</th>
							<?
							} elseif ($arHeader["id"] == "PRICE") {
							?>
								<th class="price col-price" id="col_<?=$arHeader["id"];?>">Цена</th>
							<?
							} elseif ($arHeader["id"] == "QUANTITY") {
							?>
								<th class="col-quantity" id="col_<?=$arHeader["id"];?>">Количество</th>
								<th class="col-remove">&nbsp;</th>
							<?
							} elseif ($arHeader["id"] == "DISCOUNT") {
							?>
								<th class="col-amount" id="col_<?=$arHeader["id"];?>">Скидка</th>
							<?
							} elseif ($arHeader["id"] == "SUM") {
							?>
								<th class="col-amount" id="col_<?=$arHeader["id"];?>">Итого</th>
							<?
							}
							?>
						<?
						endforeach;
						?>

					</tr>
				</thead>

				<tbody>
					<?
					$skipHeaders = array('PROPS', 'DELAY', 'TYPE');

					foreach ($arResult["GRID"]["ROWS"] as $k => $arItem):

						if ($arItem["DELAY"] == "N" && $arItem["CAN_BUY"] == "Y"):
						?>
						<tr id="<?=$arItem["ID"]?>"
							 data-item-name="<?=$arItem["NAME"]?>"
							 data-item-brand="<?=$arItem[$arParams['BRAND_PROPERTY']."_VALUE"]?>"
							 data-item-price="<?=$arItem["PRICE"]?>"
							 data-item-currency="<?=$arItem["CURRENCY"]?>"
						>
							<?
							foreach ($arResult["GRID"]["HEADERS"] as $id => $arHeader) {

								if (in_array($arHeader["id"], $skipHeaders)) // some values are not shown in the columns in this template
									continue;

								if ($arHeader["name"] == '') {
									$arHeader["name"] = GetMessage("SALE_".$arHeader["id"]);
								}

								if ($arHeader["id"] == "NAME") {
								?>
									<td class="col-img">
										<?
										if (strlen($arItem["PREVIEW_PICTURE_SRC"]) > 0):
											$url = $arItem["PREVIEW_PICTURE_SRC"];
										elseif (strlen($arItem["DETAIL_PICTURE_SRC"]) > 0):
											$url = $arItem["DETAIL_PICTURE_SRC"];
										else:
											$url = $templateFolder."/images/no_photo.png";
										endif;

										if (strlen($arItem["DETAIL_PAGE_URL"]) > 0) {?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?}?>
						                <img src="<?=$url?>" alt="<?=$arItem["NAME"]?>" />
										<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0) {?></a><?}?>
										<?
										if (!empty($arItem["BRAND"])):
										?>
										<div class="bx_ordercart_brand">
											<img alt="" src="<?=$arItem["BRAND"]?>" />
										</div>
										<?
										endif;
										?>
									</td>
									<td class="col-title">
										<h3 class="bx_ordercart_itemtitle">
											<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0) {?><a href="<?=$arItem["DETAIL_PAGE_URL"] ?>"><?}?>
												<?=$arItem["NAME"]?>
											<?if (strlen($arItem["DETAIL_PAGE_URL"]) > 0) {?></a><?}?>
										</h3>
									</td>
								<?
								} elseif ($arHeader["id"] == "QUANTITY") {
								?>
									<td class="col-quantity">
										<div class="counter">

											<?
											$ratio = isset($arItem["MEASURE_RATIO"]) ? $arItem["MEASURE_RATIO"] : 0;
											$useFloatQuantity = ($arParams["QUANTITY_FLOAT"] == "Y") ? true : false;
											$useFloatQuantityJS = ($useFloatQuantity ? "true" : "false");
											?>
											<span class="counter-index"><em data-count="<?=$arItem['QUANTITY']?>"><?=$arItem['QUANTITY']?></em></span>
											<input type="hidden" id="QUANTITY_INPUT_<?=$arItem["ID"]?>" name="QUANTITY_INPUT_<?=$arItem["ID"]?>" value="<?=$arItem["QUANTITY"]?>" />
											<?
											if (!isset($arItem["MEASURE_RATIO"])) {
												$arItem["MEASURE_RATIO"] = 1;
											}

											?>
											<a href="javascript:void(0);" class="minus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'down', <?=$useFloatQuantityJS?>);">&ndash;</a>
											<a href="javascript:void(0);" class="plus" onclick="setQuantity(<?=$arItem["ID"]?>, <?=$arItem["MEASURE_RATIO"]?>, 'up', <?=$useFloatQuantityJS?>);">+</a>

										</div>
										<input type="hidden" id="QUANTITY_<?=$arItem['ID']?>" name="QUANTITY_<?=$arItem['ID']?>" value="<?=$arItem["QUANTITY"]?>" />
									</td>
								<?
								} elseif ($arHeader["id"] == "PRICE") {
								?>
									<td class="col-price">
										<span class="price" data-price="" id="current_price_<?=$arItem["ID"]?>">
											<?=$arItem["FULL_PRICE_FORMATED"]?>
										</span>

										<?if ($bPriceType && strlen($arItem["NOTES"]) > 0):?>
											<div class="type_price"><?=GetMessage("SALE_TYPE")?></div>
											<div class="type_price_value"><?=$arItem["NOTES"]?></div>
										<?endif;?>
									</td>
								<?
								} elseif ($arHeader["id"] == "DISCOUNT") {
								?>
									<td class="col-amount">
										<span class="price" id="discount_value_<?=$arItem["ID"]?>"><?=$arItem["DISCOUNT_PRICE"]?> руб.</span>
									</td>
								<?
								} elseif ($arHeader["id"] == "DELETE") {
								?>
				                    <td class="col-remove">
				                        <a href="<?=str_replace("#ID#", $arItem["ID"], $arUrls["delete"])?>" onclick="return deleteProductRow(this)" class="btn-remove"></a>
				                    </td>
								<?
								} elseif ($arHeader["id"] == "SUM") {
								?>
				                    <td class="col-amount"><span id="sum_<?=$arItem["ID"]?>" class="price" data-price=""><?=str_replace(" ", "&nbsp;", $arItem["SUM"])?></span></td>
								<?
								} elseif ($arHeader["id"] == "WEIGHT") {
								?>
									<td class="custom">
										<span><?=$arHeader["name"]; ?>:</span>
										<?=$arItem["WEIGHT_FORMATED"]?>
									</td>
								<?
								}
							}?>

						</tr>
						<?
						endif;
					endforeach;
					?>
				</tbody>
			</table>
	    </div>
	    </div>
	    <div class="clear"></div>
	    </div>
	    </div>
	</section>

	<input type="hidden" id="column_headers" value="<?=htmlspecialcharsbx(implode($arHeaders, ","))?>" />
	<input type="hidden" id="offers_props" value="<?=htmlspecialcharsbx(implode($arParams["OFFERS_PROPS"], ","))?>" />
	<input type="hidden" id="action_var" value="<?=htmlspecialcharsbx($arParams["ACTION_VARIABLE"])?>" />
	<input type="hidden" id="quantity_float" value="<?=($arParams["QUANTITY_FLOAT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="price_vat_show_value" value="<?=($arParams["PRICE_VAT_SHOW_VALUE"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="hide_coupon" value="<?=($arParams["HIDE_COUPON"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="use_prepayment" value="<?=($arParams["USE_PREPAYMENT"] == "Y") ? "Y" : "N"?>" />
	<input type="hidden" id="auto_calculation" value="<?=($arParams["AUTO_CALCULATION"] == "N") ? "N" : "Y"?>" />
<?
} else {
?>
<div class="shopping-cart-empty" id="basket_items_list"><?=GetMessage("SALE_NO_ITEMS");?></div>

<?
}
