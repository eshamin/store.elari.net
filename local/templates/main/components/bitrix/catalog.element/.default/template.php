<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use \Bitrix\Main\Localization\Loc;

/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 * @var CatalogSectionComponent $component
 * @var CBitrixComponentTemplate $this
 * @var string $templateName
 * @var string $componentPath
 * @var string $templateFolder
 */

$this->setFrameMode(true);

$templateLibrary = array('popup', 'fx');
$currencyList = '';

if (!empty($arResult['CURRENCIES']))
{
	$templateLibrary[] = 'currency';
	$currencyList = CUtil::PhpToJSObject($arResult['CURRENCIES'], false, true, true);
}

$templateData = array(
	'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
	'TEMPLATE_LIBRARY' => $templateLibrary,
	'CURRENCIES' => $currencyList,
	'ITEM' => array(
		'ID' => $arResult['ID'],
		'IBLOCK_ID' => $arResult['IBLOCK_ID'],
		'OFFERS_SELECTED' => $arResult['OFFERS_SELECTED'],
		'JS_OFFERS' => $arResult['JS_OFFERS']
	)
);
unset($currencyList, $templateLibrary);

$mainId = $this->GetEditAreaId($arResult['ID']);
$itemIds = array(
	'ID' => $mainId,
	'DISCOUNT_PERCENT_ID' => $mainId.'_dsc_pict',
	'STICKER_ID' => $mainId.'_sticker',
	'BIG_SLIDER_ID' => $mainId.'_big_slider',
	'BIG_IMG_CONT_ID' => $mainId.'_bigimg_cont',
	'SLIDER_CONT_ID' => $mainId.'_slider_cont',
	'OLD_PRICE_ID' => $mainId.'_old_price',
	'PRICE_ID' => $mainId.'_price',
	'DISCOUNT_PRICE_ID' => $mainId.'_price_discount',
	'PRICE_TOTAL' => $mainId.'_price_total',
	'SLIDER_CONT_OF_ID' => $mainId.'_slider_cont_',
	'QUANTITY_ID' => $mainId.'_quantity',
	'QUANTITY_DOWN_ID' => $mainId.'_quant_down',
	'QUANTITY_UP_ID' => $mainId.'_quant_up',
	'QUANTITY_MEASURE' => $mainId.'_quant_measure',
	'QUANTITY_LIMIT' => $mainId.'_quant_limit',
	'BUY_LINK' => $mainId.'_buy_link',
	'ADD_BASKET_LINK' => $mainId.'_add_basket_link',
	'BASKET_ACTIONS_ID' => $mainId.'_basket_actions',
	'NOT_AVAILABLE_MESS' => $mainId.'_not_avail',
	'COMPARE_LINK' => $mainId.'_compare_link',
	'TREE_ID' => $mainId.'_skudiv',
	'DISPLAY_PROP_DIV' => $mainId.'_sku_prop',
	'DISPLAY_MAIN_PROP_DIV' => $mainId.'_main_sku_prop',
	'OFFER_GROUP' => $mainId.'_set_group_',
	'BASKET_PROP_DIV' => $mainId.'_basket_prop',
	'SUBSCRIBE_LINK' => $mainId.'_subscribe',
	'TABS_ID' => $mainId.'_tabs',
	'TAB_CONTAINERS_ID' => $mainId.'_tab_containers',
	'SMALL_CARD_PANEL_ID' => $mainId.'_small_card_panel',
	'TABS_PANEL_ID' => $mainId.'_tabs_panel'
);
$obName = $templateData['JS_OBJ'] = 'ob'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);
$name = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']
	: $arResult['NAME'];
$title = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_TITLE']
	: $arResult['NAME'];
$alt = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT'])
	? $arResult['IPROPERTY_VALUES']['ELEMENT_DETAIL_PICTURE_FILE_ALT']
	: $arResult['NAME'];

$haveOffers = !empty($arResult['OFFERS']);
if ($haveOffers)
{
	$actualItem = isset($arResult['OFFERS'][$arResult['OFFERS_SELECTED']])
		? $arResult['OFFERS'][$arResult['OFFERS_SELECTED']]
		: reset($arResult['OFFERS']);
	$showSliderControls = false;

	foreach ($arResult['OFFERS'] as $offer)
	{
		if ($offer['MORE_PHOTO_COUNT'] > 1)
		{
			$showSliderControls = true;
			break;
		}
	}
}
else
{
	$actualItem = $arResult;
	$showSliderControls = $arResult['MORE_PHOTO_COUNT'] > 1;
}
$skuProps = array();
$price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
$measureRatio = $actualItem['ITEM_MEASURE_RATIOS'][$actualItem['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'];
$showDiscount = $price['PERCENT'] > 0;

$showDescription = !empty($arResult['PREVIEW_TEXT']) || !empty($arResult['DETAIL_TEXT']);
$showBuyBtn = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION']);
$buyButtonClassName = in_array('BUY', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showAddBtn = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION']);
$showButtonClassName = in_array('ADD', $arParams['ADD_TO_BASKET_ACTION_PRIMARY']) ? 'btn-default' : 'btn-link';
$showSubscribe = $arParams['PRODUCT_SUBSCRIPTION'] === 'Y' && ($arResult['CATALOG_SUBSCRIBE'] === 'Y' || $haveOffers);

$arParams['MESS_BTN_BUY'] = $arParams['MESS_BTN_BUY'] ?: Loc::getMessage('CT_BCE_CATALOG_BUY');
$arParams['MESS_BTN_ADD_TO_BASKET'] = $arParams['MESS_BTN_ADD_TO_BASKET'] ?: Loc::getMessage('CT_BCE_CATALOG_ADD');
$arParams['MESS_NOT_AVAILABLE'] = $arParams['MESS_NOT_AVAILABLE'] ?: Loc::getMessage('CT_BCE_CATALOG_NOT_AVAILABLE');
$arParams['MESS_BTN_COMPARE'] = $arParams['MESS_BTN_COMPARE'] ?: Loc::getMessage('CT_BCE_CATALOG_COMPARE');
$arParams['MESS_PRICE_RANGES_TITLE'] = $arParams['MESS_PRICE_RANGES_TITLE'] ?: Loc::getMessage('CT_BCE_CATALOG_PRICE_RANGES_TITLE');
$arParams['MESS_DESCRIPTION_TAB'] = $arParams['MESS_DESCRIPTION_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_DESCRIPTION_TAB');
$arParams['MESS_PROPERTIES_TAB'] = $arParams['MESS_PROPERTIES_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_PROPERTIES_TAB');
$arParams['MESS_COMMENTS_TAB'] = $arParams['MESS_COMMENTS_TAB'] ?: Loc::getMessage('CT_BCE_CATALOG_COMMENTS_TAB');
$arParams['MESS_SHOW_MAX_QUANTITY'] = $arParams['MESS_SHOW_MAX_QUANTITY'] ?: Loc::getMessage('CT_BCE_CATALOG_SHOW_MAX_QUANTITY');
$arParams['MESS_RELATIVE_QUANTITY_MANY'] = $arParams['MESS_RELATIVE_QUANTITY_MANY'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_MANY');
$arParams['MESS_RELATIVE_QUANTITY_FEW'] = $arParams['MESS_RELATIVE_QUANTITY_FEW'] ?: Loc::getMessage('CT_BCE_CATALOG_RELATIVE_QUANTITY_FEW');

$positionClassMap = array(
	'left' => 'product-item-label-left',
	'center' => 'product-item-label-center',
	'right' => 'product-item-label-right',
	'bottom' => 'product-item-label-bottom',
	'middle' => 'product-item-label-middle',
	'top' => 'product-item-label-top'
);

$discountPositionClass = 'product-item-label-big';
if ($arParams['SHOW_DISCOUNT_PERCENT'] === 'Y' && !empty($arParams['DISCOUNT_PERCENT_POSITION']))
{
	foreach (explode('-', $arParams['DISCOUNT_PERCENT_POSITION']) as $pos)
	{
		$discountPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}

$labelPositionClass = 'product-item-label-big';
if (!empty($arParams['LABEL_PROP_POSITION']))
{
	foreach (explode('-', $arParams['LABEL_PROP_POSITION']) as $pos)
	{
		$labelPositionClass .= isset($positionClassMap[$pos]) ? ' '.$positionClassMap[$pos] : '';
	}
}
?>
<?

$name = $arResult['PROPERTIES']['ELARI_TITLE']['VALUE'];
$subName = $arResult['PROPERTIES']['ELARI_SUB_TITLE']['VALUE'];
?>


<div class="double-columns" id="<?=$itemIds['ID']?>">
    <div>
        <div class="product-gallery">
            <div class="images thumbnails">
                <div class="scroll">
					<?if ($arResult['DETAIL_PICTURE']['SRC']) {?>
					<?
            			$pict = $arResult['DETAIL_PICTURE']['SRC'];
						$arFileTmp = CFile::ResizeImageGet(
							$arResult['DETAIL_PICTURE']['ID'],
							array("width" => 60, "height" => 60),
							BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
							true, array(array("name" => "sharpen", "precision" => 30))
						);
					?>

					    <a class="thumb" data-standard="<?=$arResult['DETAIL_PICTURE']['SRC']?>">
							<img src="<?=$arFileTmp['src']?>" alt="<?=$name?>" />
						</a>
					<?} else {?>
					<?
            			$pict = $arResult['PREVIEW_PICTURE']['SRC'];
						$arFileTmp = CFile::ResizeImageGet(
							$arResult['PREVIEW_PICTURE']['ID'],
							array("width" => 60, "height" => 60),
							BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
							true, array(array("name" => "sharpen", "precision" => 30))
						);
					?>

						<a class="thumb" data-standard="<?=$arResult['PREVIEW_PICTURE']['SRC']?>">
							<img src="<?=$arFileTmp['src']?>" alt="<?=$name?>" />
						</a>
					<?}?>
            		<?foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $arPhoto) {?>
					<?
						$arFileTmp = CFile::ResizeImageGet(
								$arPhoto,
								array("width" => 60, "height" => 60),
								BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
								true, array(array("name" => "sharpen", "precision" => 30))
						);
					?>
					    <a class="thumb" data-standard="<?=CFile::GetPath($arPhoto)?>">
					        <img src="<?=$arFileTmp['src']?>" alt="<?=$name?>" />
					    </a>
            		<?}?>
                </div>
            </div>
            <div class="pg-r">
                <div class="product-image-large">
					<?if ($arResult['DETAIL_PICTURE']['SRC']) {?>
					<?
            			$pict = $arResult['DETAIL_PICTURE']['SRC'];
						$arFileTmp = CFile::ResizeImageGet(
								$arResult['DETAIL_PICTURE']['ID'],
								array("width" => 525, "height" => 525),
								BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
								true, array(array("name" => "sharpen", "precision" => 30))
						);
					?>
					    <a href="<?=$arResult['DETAIL_PICTURE']['SRC']?>" data-fancybox="gallery">
					        <img src="<?=$arFileTmp['src']?>" />
					    </a>
					<?} else {?>
					<?
            			$pict = $arResult['PREVIEW_PICTURE']['SRC'];
						$arFileTmp = CFile::ResizeImageGet(
								$arResult['PREVIEW_PICTURE']['ID'],
								array("width" => 525, "height" => 525),
								BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
								true, array(array("name" => "sharpen", "precision" => 30))
						);
					?>
					    <a href="<?=$arResult['PREVIEW_PICTURE']['SRC']?>" data-fancybox="gallery">
					        <img src="<?=$arFileTmp['src']?>" />
					    </a>
					<?}?>
                </div>
            </div>
        </div>
        <div class="mobile-gallery">
            <div class="bx-slider">
				<?if ($arResult['DETAIL_PICTURE']['SRC']) {?>
				<?
            		$pict = $arResult['DETAIL_PICTURE']['SRC'];
					$arFileTmp = CFile::ResizeImageGet(
						$arResult['DETAIL_PICTURE']['ID'],
						array("width" => 600, "height" => 600),
						BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
						true, array(array("name" => "sharpen", "precision" => 30))
					);
				?>

					<div class="item">
						<img src="<?=$arFileTmp['src']?>" alt="<?=$name?>" />
					</div>
				<?} else {?>
				<?
            		$pict = $arResult['PREVIEW_PICTURE']['SRC'];
					$arFileTmp = CFile::ResizeImageGet(
						$arResult['PREVIEW_PICTURE']['ID'],
						array("width" => 600, "height" => 600),
						BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
						true, array(array("name" => "sharpen", "precision" => 30))
					);
				?>

					<div class="item">
						<img src="<?=$arFileTmp['src']?>" alt="<?=$name?>" />
					</div>
				<?}?>
            	<?foreach ($arResult['PROPERTIES']['MORE_PHOTO']['VALUE'] as $arPhoto) {?>
				<?
					$arFileTmp = CFile::ResizeImageGet(
							$arPhoto,
							array("width" => 600, "height" => 600),
							BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
							true, array(array("name" => "sharpen", "precision" => 30))
					);
				?>
					<div class="item">
					    <img src="<?=$arFileTmp['src']?>" alt="<?=$name?>" />
					</div>
            	<?}?>
            </div>
        </div>
    </div>
    <div>
        <h1><?=$name?>, <?=$subName?></h1>
        <div class="tags">
        	<?foreach ($arResult['PROPERTIES']['TAGS']['VALUE'] as $tag) {
				?><a href="/search/?tags=<?=$tag?>"><?=$tag?></a><?
        	}?>
        </div>
        <div class="colors">
			<?if (count($arResult['SM_IDENTICAL']['SM_COLOR_CODE']['GOODS']) > 1) {?>
				<h4><?=($arResult['IBLOCK_SECTION_ID'] == 406) ? 'Цвет наушников' : 'Выбор цвета'?></h4>
				<ul>
				<? foreach ($arResult['SM_IDENTICAL'] as $field => $arData) {
					if ($arData['GOODS']) {?>

						<? foreach ($arData['GOODS'] as $arGood) { ?>
				            <li class="item">
				                <a href="<?= $arGood['DETAIL_PAGE_URL'] ?>" class="color-square" style="background-color:<?= $arGood['VALUE'] ?>;"></a>
				            </li>
						<?
						} ?>
					<? } ?>
				<? } ?>
        		</ul>
			<?}?>
        </div>

		<?$this->SetViewTarget("addToBasketMobile");?>

		<div class="mobile-buy buy">
		    <div class="fake-table">
		        <div class="fake-td price-col">
		            <label>Стоимость:</label>
		            <div class="price" id="<?=$itemIds['PRICE_ID']?>"><span><?=$price['PRINT_RATIO_BASE_PRICE']?></span></div>
		        </div>
		        <div class="fake-td" id="<?=$itemIds['BASKET_ACTIONS_ID']?>2">
		            <a class="button" id="<?=$itemIds['ADD_BASKET_LINK']?>2"><span><?if ($arResult['IN_CART']) {?>Добавлено в корзину<?} else {?>Добавить в корзину<?}?></span></a>
		        </div>
		    </div>
		</div>
		<?$this->EndViewTarget("addToBasketMobile");?>

		<div class="buy">
		    <div class="price" id="<?=$itemIds['PRICE_ID']?>">
                <span><nobr><?=$price['PRINT_RATIO_PRICE']?></nobr></span>
			<? if ($price['DISCOUNT'] > 0 && $arParams['SHOW_OLD_PRICE'] == 'Y'){?>
				<div class="buy-old-price"><nobr><span class=""><?=$price['PRINT_RATIO_BASE_PRICE']?></span></nobr></div>
			<?} ?>
			</div>
		    <div id="<?=$itemIds['BASKET_ACTIONS_ID']?>">
                <?if ($arResult['CAN_BUY']) {?>
		        <a class="button" id="<?=$itemIds['ADD_BASKET_LINK']?>"><span><?if ($arResult['IN_CART']) {?>Добавлено в корзину<?} else {?>Добавить в корзину<?}?></span></a>
		        <?} else {
					?>
					<div class="pre-sale">
						<?
						$APPLICATION->IncludeComponent(
							'bitrix:catalog.product.subscribe',
							'elari',
							array(
								'PRODUCT_ID' => $arResult['ID'],
								'BUTTON_ID' => $itemIds['SUBSCRIBE_LINK'],
								'BUTTON_CLASS' => 'btn btn-default product-item-detail-buy-button',
								'DEFAULT_DISPLAY' => !$actualItem['CAN_BUY'],
								'MESS_BTN_SUBSCRIBE' => "Прездаказ",
								//'MESS_BTN_SUBSCRIBE' => $arParams['~MESS_BTN_SUBSCRIBE'],
							),
							$component,
							array('HIDE_ICONS' => 'N')
						);
						?>
					</div>
					<?
				}
				?>

		    </div>
		    <!-- <a class="button jsPopup" href="#buy_one_click">Купить в один клик</a> -->

		</div>
        <div class="tabs-info">
            <ul>
                <li><a href="#tab-1">Описание</a></li>
                <?if ($arResult['PROPERTIES']['FEATURES']['VALUE']) {?><li><a href="#tab-2">Характеристики</a></li><?}?>
            </ul>
            <div id="tab-1">
            	<?
            	$desc = $arResult['PROPERTIES']['ELARI_DESCRIPTION']['~VALUE']['TEXT'];
            	/*$descriptionFirst = substr($desc, 0, strpos($desc, ' ', 1500));
            	if (strlen($desc) > 1500) {
            		$descriptionSecond = substr($desc, strpos($desc, ' ', 1500));
				}*/
            	?>
                <?=$desc//$descriptionFirst?>

                <?/*if (strlen(trim($descriptionSecond)) > 0) {?>
	                <div class="details">
	                	<?=$descriptionSecond?>
	                    <div class="less-details"><a class="link-details">Меньше</a></div>
	                </div>
	                <div class="more-details"><a class="link-details">Подробнее</a></div>
	            <?}*/?>
            </div>
            <?if ($arResult['PROPERTIES']['FEATURES']['VALUE']) {?>
            <div id="tab-2" class="tab-ch">
                <h3>Характеристики</h3>

			<?
			$featuresCount = 0;
			foreach ($arResult['PROPERTIES']['FEATURES']['VALUE'] as $feature) {
				$featuresCount++;
				list($name, $value) = explode(':', CIBlockElement::GetByID($feature)->Fetch()['NAME']);
				?>
                <dl>
                    <dt><span><?=$name?></span></dt>
                    <dd><span><?=$value?></span></dd>
                </dl>
                <?if ($featuresCount == 10) {?>
                <div class="details">
                <?}?>
			<?} ?>
			<?if ($featuresCount >= 10) {?>
                <div class="less-details"><a class="link-details">Меньше</a></div>
            </div>
            <div class="more-details"><a class="link-details">Подробнее</a></div>
            <?}?>
            </div>
            <?}?>
        </div>
    </div>
</div>

<?
	$jsParams = array(
		'CONFIG' => array(
			'USE_CATALOG' => $arResult['CATALOG'],
			'SHOW_QUANTITY' => $arParams['USE_PRODUCT_QUANTITY'],
			'SHOW_PRICE' => !empty($arResult['ITEM_PRICES']),
			'SHOW_DISCOUNT_PERCENT' => $arParams['SHOW_DISCOUNT_PERCENT'] === 'Y',
			'SHOW_OLD_PRICE' => $arParams['SHOW_OLD_PRICE'] === 'Y',
			'USE_PRICE_COUNT' => $arParams['USE_PRICE_COUNT'],
			'DISPLAY_COMPARE' => $arParams['DISPLAY_COMPARE'],
			'MAIN_PICTURE_MODE' => $arParams['DETAIL_PICTURE_MODE'],
			'ADD_TO_BASKET_ACTION' => $arParams['ADD_TO_BASKET_ACTION'],
			'SHOW_CLOSE_POPUP' => $arParams['SHOW_CLOSE_POPUP'] === 'Y',
			'SHOW_MAX_QUANTITY' => $arParams['SHOW_MAX_QUANTITY'],
			'RELATIVE_QUANTITY_FACTOR' => $arParams['RELATIVE_QUANTITY_FACTOR'],
			'TEMPLATE_THEME' => $arParams['TEMPLATE_THEME'],
			'USE_STICKERS' => true,
			'USE_SUBSCRIBE' => $showSubscribe,
			'SHOW_SLIDER' => $arParams['SHOW_SLIDER'],
			'SLIDER_INTERVAL' => $arParams['SLIDER_INTERVAL'],
			'ALT' => $alt,
			'TITLE' => $title,
			'MAGNIFIER_ZOOM_PERCENT' => 200,
			'USE_ENHANCED_ECOMMERCE' => $arParams['USE_ENHANCED_ECOMMERCE'],
			'DATA_LAYER_NAME' => $arParams['DATA_LAYER_NAME'],
			'BRAND_PROPERTY' => !empty($arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']])
				? $arResult['DISPLAY_PROPERTIES'][$arParams['BRAND_PROPERTY']]['DISPLAY_VALUE']
				: null
		),
		'VISUAL' => $itemIds,
		'PRODUCT_TYPE' => $arResult['CATALOG_TYPE'],
		'PRODUCT' => array(
			'ID' => $arResult['ID'],
			'ACTIVE' => $arResult['ACTIVE'],
			'PICT' => $arResult['PREVIEW_PICTURE'],
			'NAME' => $arResult['PROPERTIES']['ELARI_TITLE']['VALUE'],
			'SUBNAME' => $arResult['PROPERTIES']['ELARI_SUB_TITLE']['VALUE'],
			'SUBSCRIPTION' => true,
			'ITEM_PRICE_MODE' => $arResult['ITEM_PRICE_MODE'],
			'ITEM_PRICES' => $arResult['ITEM_PRICES'],
			'PRICE' => $price["RATIO_PRICE"],
			'ITEM_PRICE_SELECTED' => $arResult['ITEM_PRICE_SELECTED'],
			'ITEM_QUANTITY_RANGES' => $arResult['ITEM_QUANTITY_RANGES'],
			'ITEM_QUANTITY_RANGE_SELECTED' => $arResult['ITEM_QUANTITY_RANGE_SELECTED'],
			'ITEM_MEASURE_RATIOS' => $arResult['ITEM_MEASURE_RATIOS'],
			'ITEM_MEASURE_RATIO_SELECTED' => $arResult['ITEM_MEASURE_RATIO_SELECTED'],
			'SLIDER_COUNT' => $arResult['MORE_PHOTO_COUNT'],
			'SLIDER' => $arResult['MORE_PHOTO'],
			'CAN_BUY' => $arResult['CAN_BUY'],
			'CHECK_QUANTITY' => $arResult['CHECK_QUANTITY'],
			'QUANTITY_FLOAT' => is_float($arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO']),
			'MAX_QUANTITY' => $arResult['CATALOG_QUANTITY'],
			'STEP_QUANTITY' => $arResult['ITEM_MEASURE_RATIOS'][$arResult['ITEM_MEASURE_RATIO_SELECTED']]['RATIO'],
			'CATEGORY' => $arResult['CATEGORY_PATH']
		),
		'BASKET' => array(
			'ADD_PROPS' => $arParams['ADD_PROPERTIES_TO_BASKET'] === 'Y',
			'QUANTITY' => $arParams['PRODUCT_QUANTITY_VARIABLE'],
			'PROPS' => $arParams['PRODUCT_PROPS_VARIABLE'],
			'EMPTY_PROPS' => $emptyProductProperties,
			'BASKET_URL' => $arParams['BASKET_URL'],
			'ADD_URL_TEMPLATE' => $arResult['~ADD_URL_TEMPLATE'],
			'BUY_URL_TEMPLATE' => $arResult['~BUY_URL_TEMPLATE']
		)
	);
	unset($emptyProductProperties);

?>
<script>
	BX.message({
		ECONOMY_INFO_MESSAGE: '<?=GetMessageJS('CT_BCE_CATALOG_ECONOMY_INFO2')?>',
		TITLE_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_ERROR')?>',
		TITLE_BASKET_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_TITLE_BASKET_PROPS')?>',
		BASKET_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_BASKET_UNKNOWN_ERROR')?>',
		BTN_SEND_PROPS: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_SEND_PROPS')?>',
		BTN_MESSAGE_BASKET_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_BASKET_REDIRECT')?>',
		BTN_MESSAGE_CLOSE: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE')?>',
		BTN_MESSAGE_CLOSE_POPUP: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_CLOSE_POPUP')?>',
		TITLE_SUCCESSFUL: '<?=GetMessageJS('CT_BCE_CATALOG_ADD_TO_BASKET_OK')?>',
		COMPARE_MESSAGE_OK: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_OK')?>',
		COMPARE_UNKNOWN_ERROR: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_UNKNOWN_ERROR')?>',
		COMPARE_TITLE: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_COMPARE_TITLE')?>',
		BTN_MESSAGE_COMPARE_REDIRECT: '<?=GetMessageJS('CT_BCE_CATALOG_BTN_MESSAGE_COMPARE_REDIRECT')?>',
		PRODUCT_GIFT_LABEL: '<?=GetMessageJS('CT_BCE_CATALOG_PRODUCT_GIFT_LABEL')?>',
		PRICE_TOTAL_PREFIX: '<?=GetMessageJS('CT_BCE_CATALOG_MESS_PRICE_TOTAL_PREFIX')?>',
		RELATIVE_QUANTITY_MANY: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_MANY'])?>',
		RELATIVE_QUANTITY_FEW: '<?=CUtil::JSEscape($arParams['MESS_RELATIVE_QUANTITY_FEW'])?>',
		SITE_ID: '<?=SITE_ID?>'
	});

	var <?=$obName?> = new JCCatalogElement(<?=CUtil::PhpToJSObject($jsParams, false, true)?>);
</script>

<script>
    window.dataLayer = window.dataLayer || [];
    dataLayer.push({
        "ecommerce": {
            "detail": {
                "products": [
                    {
                        "id": '<?=$arResult["ID"]?>',
                        "name": '<?=$arResult["NAME"]?>',
                        "price": parseFloat('<?=$price["RATIO_PRICE"]?>'),
                        "category": '<?= $arResult["SECTION"]["NAME"]?>'
                    }
                ]
            }
        }
    });

	gtag('event', 'view_item', {
		"items": [
			{
				"id": "<?=$arResult["ID"]?>",
				"name": "<?=$arResult["NAME"]?>",
				"category": "<?= $arResult["SECTION"]["NAME"]?>",
				"price": parseFloat('<?=$price["RATIO_PRICE"]?>'),
				"list_position": 1
			}
		]
	});
</script>
<?
unset($actualItem, $itemIds, $jsParams);