<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
 <section class="news-box">
<div class="wrapper">
	<h1>Новости</h1>
	<div class="news-list">

        <?foreach($arResult["ITEMS"] as $arItem) {?>
        <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('NEWS_ELEMENT_DELETE_CONFIRM')));

			$arFileTmp = CFile::ResizeImageGet(
					$arItem['PREVIEW_PICTURE']['ID'],
					array("width" => 300, "height" => 300),
					BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
					true, array(array("name" => "sharpen", "precision" => 30))
			);
        ?>
            <div class="news-item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="news-title">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['NAME']?></a>
                </div>
                <div class="news-image">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>">
                        <img alt="<?=$arItem['NAME']?>" src="<?=$arItem['PREVIEW_PICTURE']['SRC']?>" />
                    </a>
                </div>
                <div class="news-excerpt">
                    <a href="<?=$arItem['DETAIL_PAGE_URL']?>"><?=$arItem['PREVIEW_TEXT']?></a>
                </div>
            </div>
        <?}?>

        </div>
    </div>
</section>
<?if($arParams["DISPLAY_BOTTOM_PAGER"]) {?>
	<?=$arResult["NAV_STRING"]?>
<?}?>