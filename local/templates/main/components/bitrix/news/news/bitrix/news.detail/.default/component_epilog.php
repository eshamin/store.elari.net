<?php
/**
 * @copyright Copyright (c) 2015, ADV/web-engineering co.
 */


if(isset($arParams['IS_ACTIONS']) && $arParams['IS_ACTIONS'] == 'Y') {
    $showActionDate = true;
    $curYear = date('Y');

    foreach (array('DATE_ACTIVE_FROM', 'DATE_ACTIVE_TO') as $key) {
        if (isset($arResult['PROPERTIES'][$key])) {
            $obData = new \DateTime($arResult['PROPERTIES'][$key]);
            if ($curYear != $obData->format('Y')) {
                $arResult['PROPERTIES'][$key] = FormatDate('d F Y', $obData->getTimestamp());
            } else {
                $arResult['PROPERTIES'][$key] = FormatDate('d F', $obData->getTimestamp());
            }
        } else {
            $showActionDate = false;
            break;
        }
    }
}

if(isset($showActionDate) && $showActionDate === true):
    $APPLICATION->AddViewContent('before_header', <<<HTML
<span class="news_list__action-date __inner">с {$arResult['PROPERTIES']['DATE_ACTIVE_FROM']} по {$arResult['PROPERTIES']['DATE_ACTIVE_TO']}</span>
HTML
    );
endif;