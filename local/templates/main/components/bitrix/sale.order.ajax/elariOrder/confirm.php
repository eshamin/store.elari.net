<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Sale\DiscountCouponsManager;

if (!empty($arResult['ORDER'])) { ?>
<section class="shopping-box">
	<div class="wrapper">
        <h1>Спасибо за заказ!</h1>
        <div class="ordering_thanks__details">
            <p>Номер вашего заказа № <?=$arResult['ORDER']['ACCOUNT_NUMBER']?>.</p>
            <p>Вам отправлен e-mail и SMS с номером заказа.</p>
            <?if ($arResult['PAY_SYSTEM']['ID'] == 20) {?>
                <p>В ближайшее время наш менеджер свяжется с вами для уточнения деталей заказа.</p>
            <?}?>
        </div>
        <? if (!empty($arResult['PAY_SYSTEM'])) { ?>
            <div class="ordering_thanks__process">
                <div>Способ оплаты заказа: <b><?= $arResult["PAY_SYSTEM"]["NAME"] ?></b></div>
                <? if (strlen($arResult["PAY_SYSTEM"]["ACTION_FILE"]) > 0) { ?>
                    <div class="payment-info payButton2">
                        <? if ($arResult["PAY_SYSTEM"]["NEW_WINDOW"] == "Y") { ?>
                            <script language="JavaScript">
                                window.open('<?=$arParams["PATH_TO_PAYMENT"]?>?ORDER_ID=<?=urlencode(urlencode($arResult["ORDER"]["ID"]))?>');
                            </script>
                            <?= GetMessage("SOA_TEMPL_PAY_LINK", array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ID"])))) ?>
                            <? if (CSalePdf::isPdfAvailable() && CSalePaySystemsHelper::isPSActionAffordPdf($arResult['PAY_SYSTEM']['ACTION_FILE'])) { ?><br/>
                                <?= GetMessage("SOA_TEMPL_PAY_PDF", array("#LINK#" => $arParams["PATH_TO_PAYMENT"] . "?ORDER_ID=" . urlencode(urlencode($arResult["ORDER"]["ID"])) . "&pdf=1&DOWNLOAD=Y")) ?>
                            <? }
                        } else {
                            if (strlen($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]) > 0) {
                                include($arResult["PAY_SYSTEM"]["PATH_TO_ACTION"]);
                            }
                        } ?>
                    </div>
                <? } ?>
            </div>
        <? } ?>
        <!--<a href="<?=$arParams['PATH_TO_PERSONAL']?>" class="ordering_thanks__cabinet">Перейти в личный кабинет</a>-->

    </div>
</section>
    <script type="text/javascript">

        /*(function(w, p) {
            var a, s;
            (w[p] = w[p] || []).push({
                counter_id: 423693735,
                tag: '00baed38d362212d641c65de765a5cf7'
            });
            a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
            a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
            s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
        })(window, 'begun_analytics_params');

        $(function(){
        	// Google Analytics
        	var ga_order_id = 'ga_order_<?=$arResult["ORDER"]["ACCOUNT_NUMBER"]?>';
        	if (typeof(Storage) === 'undefined' || !localStorage.getItem(ga_order_id))
        	{
				if (typeof(Storage) !== 'undefined')
					localStorage.setItem(ga_order_id, 'Y');
				ga('send', 'event', 'confirm_order', 'confirm_order_send');
                ga('ec:setAction','checkout', {'step': 4,'option': 'Thank_you_page'});console.log('Thank_you_page');

<?
/*function getTransactionJs(&$trans) {
  return <<<HTML
ga('ecommerce:addTransaction', {
  'id': '{$trans['id']}',
  'revenue': '{$trans['revenue']}',
  'shipping': '{$trans['shipping']}',
});
HTML;
}

function getItemJs(&$transId, &$item) {
  return <<<HTML
ga('ecommerce:addItem', {
  'id': '$transId',
  'name': '{$item['name']}',
  'sku': '{$item['sku']}',
  'price': '{$item['price']}',
  'quantity': '{$item['quantity']}'
});
HTML;
}

$trans = array(
	'id' => $arResult["ORDER"]["ACCOUNT_NUMBER"],
	'revenue' => $arResult["ORDER"]["PRICE"],
	'shipping' => $arResult["ORDER"]["PRICE_DELIVERY"]
);

$items = array();
$ecomm_prodid = '[';
$dbBasketItems = CSaleBasket::GetList(
	array(),
	array("ORDER_ID" => $arResult["ORDER"]["ACCOUNT_NUMBER"]),
	false,
	false,
	array("NAME", "PRODUCT_ID", "PRICE", "QUANTITY")
);
while ($arItem = $dbBasketItems->Fetch())
{
	$items[] = array(
		'name' => $arItem['NAME'],
		'sku' => $arItem['PRODUCT_ID'],
		'price' => $arItem['PRICE'],
		'quantity' => $arItem['QUANTITY']
	);
	$ecomm_prodid .= $arItem['PRODUCT_ID'] . ', ';
}
$ecomm_prodid = substr($ecomm_prodid, 0, -2) . ']';

//echo getTransactionJs($trans);
foreach ($items as &$item)
	//echo getItemJs($trans['id'], $item);
*/?>

				ga('ecommerce:send');
				var google_tag_params = {
					ecomm_prodid: <?=$ecomm_prodid?>,
					ecomm_pagetype: 'purchase',
					ecomm_totalvalue: <?=$arResult["ORDER"]["PRICE"]?>
				};
			}

			// Facebook
			fbq('track', 'Purchase', {value: '<?=$arResult["ORDER"]["PRICE"]?>', currency: 'RUB'});
		});*/

		dataLayer.push({
		    "ecommerce": {
		        "purchase": {
		            "actionField": {
		                "id" : "<?=$arResult['ORDER']['ID']?>"
		            },
		            <??>
		            "products": [
					<?$dbBasketItems = CSaleBasket::GetList(
						array(),
						array("ORDER_ID" => $arResult["ORDER"]["ID"]),
						false,
						false,
						array("NAME", "PRODUCT_ID", "PRICE", "QUANTITY")
					);
					while ($arItem = $dbBasketItems->Fetch())
					{?>
		                {
		                    "id": "<?=$arItem['PRODUCT_ID']?>",
		                    "name": "<?=$arItem['NAME']?>",
		                    "price": parseFloat(<?=$arItem['PRICE']?>),
		                    "quantity": "<?=$arItem['QUANTITY']?>"
		                },
		            <?}?>
		            ]
		        }
		    }
		});

		gtag('event', 'purchase', {
			"transaction_id": "<?=$arResult['ORDER']['ID']?>",
			"value": "<?=$arResult["ORDER"]["PRICE"]?>",
			"currency": "RUB",
			"shipping": "<?=$arResult["ORDER"]["PRICE_DELIVERY"]?>",
			"items": [
				<?$dbBasketItems = CSaleBasket::GetList(
					array(),
					array("ORDER_ID" => $arResult["ORDER"]["ID"]),
					false,
					false,
					array("NAME", "PRODUCT_ID", "PRICE", "QUANTITY")
				);
				$totalCount = 0;
				while ($arItem = $dbBasketItems->Fetch())
				{
					$totalCount++;?>
				    {
				        "id": "<?=$arItem['PRODUCT_ID']?>",
				        "name": "<?=$arItem['NAME']?>",
				        "price": parseFloat(<?=$arItem['PRICE']?>),
				        "quantity": "<?=$arItem['QUANTITY']?>",
				        "list_position": "<?=$totalCount?>"
				    },
				<?}?>

		  	]
		});
		console.log(window['dataLayer']);
    </script>
<? } else { ?>
    <div class="order-confirm">
        <h3><?= GetMessage("SOA_TEMPL_ERROR_ORDER") ?></h3>
    </div>
    <div class="order-confirm pay-system">
        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST", array("#ORDER_ID#" => intval($_REQUEST['ORDER_ID']))) ?><br />
        <?= GetMessage("SOA_TEMPL_ERROR_ORDER_LOST1") ?>
    </div>
<? }?>

<?\Bitrix\Sale\DiscountCouponsManager::clear(true);?>
