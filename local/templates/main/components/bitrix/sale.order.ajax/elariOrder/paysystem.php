<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if(!$arResult['USER_VALS']['DELIVERY_ID']) {
    return;
} ?>
<div class="box-sh-2">
    <h2>Оплата</h2>
    <div class="form-group">

    <?$totalCount = 0;?>

    <? foreach($arResult['PAY_SYSTEM'] as $paySystem) {
    	if ($paySystem['ID'] == 23) {
			continue;
    	}
		if ($paySystem['ID'] != 2) {
			?>
            <label class="radio radio-bordered <?if ($paySystem['CHECKED'] == 'Y') {echo 'on';}?>">
                <input type="radio"
                       class="radiobutton_control"
                       id="ID_PAY_SYSTEM_ID_<?=$paySystem['ID']?>"
                       name="PAY_SYSTEM_ID"
                       value="<?=$paySystem['ID']?>"
                       onclick="COrder.changePaySystem();"
                    <?=$paySystem['CHECKED'] == 'Y' && !($arParams['ONLY_FULL_PAY_FROM_ACCOUNT'] == 'Y' && $arResult['USER_VALS']['PAY_CURRENT_ACCOUNT'] == 'Y') ? ' checked="checked"':'';?> />
                <span><?=$paySystem['NAME']?></span>
                <em><?=$paySystem['DESCRIPTION']?></em>
            </label>
        <?
			$totalCount++;
        }?>
    <?}?>
    </div>
</div>
<div class="box-sh-2">
	<h2>Комментарии к заказу</h2>
	<div class="space">
	    <div class="form-group comments">
	        <textarea name="ORDER_DESCRIPTION" id="orderingComment" cols="30" rows="10" class="ordering_comment"><?=$arResult['USER_VALS']['ORDER_DESCRIPTION']?></textarea>
	    </div>
        <div class="form-group">
            <select value="<?=$arResult['USER_VALS']['ORDER_PROP_62']?>" name="ORDER_PROP_67">
                <option value="">Откуда вы узнали о нас?</option>
                <option value="radio">Радио</option>
                <option value="advertising">Реклама в ТЦ</option>
                <option value="blogger">Блогер на Youtube</option>
                <option value="social">Социальные сети</option>
                <option value="internet">Реклама в интернете</option>
                <option value="other">Другое</option>
            </select>
            <div class="message error">Обязательное поле</div>
        </div>
    </div>
</div>
