<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @VAR $templateFolder string
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/*if(count($arResult["ORDER_PROP"]["USER_PROFILES"])) {?>
    <div class="reg-form_row order-profile">
        <label class="reg-form_label" for="o_name">Выберите профиль</label>
        <select name="PROFILE_ID" id="ID_PROFILE_ID" onChange="COrder.setContact()" class="reg-form_select">
            <? if ($arParams["ALLOW_NEW_PROFILE"] == "Y") { ?>
                <option value="0"><?=GetMessage("SOA_TEMPL_PROP_NEW_PROFILE")?></option>
            <? }
            foreach($arResult["ORDER_PROP"]["USER_PROFILES"] as $arUserProfiles) { ?>
                <option value="<?= $arUserProfiles["ID"] ?>"<?if ($arUserProfiles["CHECKED"]=="Y") echo " selected";?>><?=$arUserProfiles["NAME"]?></option>
            <? } ?>
        </select>

        <span class="reg-form_desc">Выбор ранее использованных данных</span>
    </div>
<? } else { ?>
    <input type="hidden" name="PROFILE_ID" value="0" />
<? }*/
?>

<?
PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_N"], $arParams["TEMPLATE_LOCATION"], $arResult['USER_VALS']);
?>
<div class="box-sh-2">
    <h2>Ваши личные данные</h2>
    <div class="space">
<?
PrintPropsForm($arResult["ORDER_PROP"]["USER_PROPS_Y"], $arParams["TEMPLATE_LOCATION"], $arResult['USER_VALS']);
?>
	</div>
</div>