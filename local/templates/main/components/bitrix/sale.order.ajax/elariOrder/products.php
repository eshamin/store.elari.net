<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="shopping-resume">
    <div class="shop-w">
        <a href="/personal/cart/" class="link-sh-change">изменить</a>
        <h3>Ваш заказ:</h3>
        <div class="mini-cart">
            <table>
                <? foreach($arResult['BASKET_ITEMS'] as $item) {?>
                <tr>
                    <td><a href="<?=$item['DETAIL_PAGE_URL']?>"><img src="<?=$item['PREVIEW_PICTURE_SRC']?>" /></a></td>
                    <td>
                        <div class="title"><a href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a></div>
                        <div class="price"><?=format($item['PRICE'])?></div>
                        <div class="quantity"><span><?=$item['QUANTITY']?></span> шт.</div>
                    </td>
                </tr>
                <?}?>
            </table>
        </div>
        <!--<div class="buttons promo-code-box">
            <div>
                <input type="text" id="coupon" name="COUPON" placeholder="Промокод на скидку" value="" />
            </div>
            <div>
                <a class="button" onclick="enterCoupon();">Применить</a>
            </div>
        </div>-->
        <div class="shopping-total">
            <table>
                <tr>
                    <th>Товары:</th>
                    <td><span class="price"><?=format($arResult['ORDER_PRICE'] + $arResult['DISCOUNT_PRICE'])?></span></td>
                </tr>
                <tr>
                    <th>Доставка:</th>
                    <td><span class="price"><?=format($arResult['DELIVERY_PRICE'])?></span></td>
                </tr>
                <?if ($arResult['PRODUCT_DISCOUNT'] > 0) {?>
                <tr>
                    <th>Скидка 20%:</th>
                    <td><span class="price green">-<?=$arResult['PRODUCT_DISCOUNT']?></span></td>
                </tr>
                <?}?>
                <tr>
                    <th class="large">ИТОГО:</th>
                    <td class="large"><span class="price"><?=format($arResult['ORDER_PRICE'] + $arResult['DELIVERY_PRICE'] + $arResult['TAX_PRICE'])?></span></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="buttons-final">
        <button class="btn-order" onclick="COrder.submitForm('Y');">Подтвердить заказ</button>
    </div>
</div>
