<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @VAR $templateFolder string
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

if (!function_exists('PrintPropsForm')) {
    function PrintPropsForm($source = array(), $locationTemplate = '', array $userVals = array())
    {
        foreach ($source as $property) {
	        if ($property['CODE'] == 'utm_source' || $property['CODE'] == 'ABOUT_US') {
	            if ($property['CODE'] == 'utm_source') {?>
	                <input type="hidden" name="<?=$property["FIELD_NAME"]?>" value="<?=$_COOKIE['utm_source']?>" id="PROPERTY_<?= $property["CODE"] ?>" />
	            <?}
				continue;
	        }?>
            <div class="double-columns">
                <div>
                	<div class="form-group form-relative">

                <? if ($property["TYPE"] == "CHECKBOX") {?>
                    <input type="hidden" name="<?= $property["FIELD_NAME"] ?>" value="" />
                    <div class="bx_block r1x3 pt8">
                        <input type="checkbox" name="<?= $property["FIELD_NAME"] ?>"
                           id="PROPERTY_<?= $property["CODE"] ?>"
                           value="Y"<?=$property["CHECKED"] == "Y" ? " checked":""; ?> />
                    </div>
                <? } elseif ($property["TYPE"] == "TEXT") { ?>
                    <input type="text" maxlength="250" size="<?= $property["SIZE1"] ?>"
                        value="<?= $property["VALUE"] ?>" name="<?= $property["FIELD_NAME"] ?>"
                        placeholder="<?=$property['NAME']?>*"
                        id="PROPERTY_<?= $property["CODE"] ?>"<?=$property["REQUIED_FORMATED"] == "Y" ? ' data-required="1"':''?><?=$property["CODE"] == "EMAIL" ? ' onblur="var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;if(regex.test(this.value)) { try {rrApi.setEmail(this.value);}catch(e){}}"':''?> />

                    <?if ($property['CODE'] == 'PERSONAL_PHONE') {

                    	/*global $USER;
						if(!$USER->IsAuthorized()) { ?>
                            <label class="checkbox">
							    <input type="hidden" value="N" name="DO_REGISTRATION" />
							    <input type="checkbox" id="reg_on_site" class="checkbox_control" value="Y" name="DO_REGISTRATION"<?=$_POST['DO_REGISTRATION'] == 'Y' || !isset($_POST['DO_REGISTRATION']) ? ' checked':''?> />
                                <em>Я хочу зарегистрироваться на сайте</em>
                                <em>Получайте бонусы, экономьте деньги и время при следующих покупках</em>
                            </label>
						<? }*/
                    }
                } elseif ($property["TYPE"] == "SELECT") { ?>
                    <select name="<?= $property["FIELD_NAME"] ?>" id="PROPERTY_<?= $property["CODE"] ?>" size="<?= $property["SIZE1"] ?>" class="reg-form_select">
                        <? foreach ($property["VARIANTS"] as $arVariants) { ?>
                            <option value="<?= $arVariants["VALUE"] ?>"<?=$arVariants["SELECTED"] == "Y" ? " selected":""; ?>><?= $arVariants["NAME"] ?></option>
                        <? } ?>
                    </select>
                <? } elseif ($property["TYPE"] == "MULTISELECT") { ?>
                    <select multiple name="<?= $property["FIELD_NAME"] ?>" id="PROPERTY_<?= $property["CODE"] ?>" size="<?= $property["SIZE1"] ?>">
                        <? foreach ($property["VARIANTS"] as $arVariants) { ?>
                            <option value="<?= $arVariants["VALUE"] ?>"<?=$arVariants["SELECTED"] == "Y" ? " selected":""; ?>><?= $arVariants["NAME"] ?></option>
                        <? } ?>
                    </select>
                <? } elseif ($property["TYPE"] == "TEXTAREA") {
                    $rows = ($property["SIZE2"] > 10) ? 4 : $property["SIZE2"]; ?>
                    <textarea rows="<?= $rows ?>" cols="<?= $property["SIZE1"] ?>"
                        name="<?= $property["FIELD_NAME"] ?>"
                        id="PROPERTY_<?= $property["CODE"] ?>"><?= $property["VALUE"] ?></textarea>
                <? } elseif ($property["TYPE"] == "LOCATION") {
                    $value = 0;
                    if (is_array($property["VARIANTS"]) && count($property["VARIANTS"]) > 0) {
                        foreach ($property["VARIANTS"] as $arVariant) {
                            if ($arVariant["SELECTED"] == "Y") {
                                $value = $arVariant["ID"];
                                break;
                            }
                        }
                    }

                    $GLOBALS["APPLICATION"]->IncludeComponent(
                        "elari:ajax.locations",
                        $locationTemplate,
                        array(
                            'MANUAL_INPUT_ID' => $userVals['MANUAL_INPUT_ID'],
                            'MANUAL_INPUT_NAME' => $userVals['MANUAL_INPUT_NAME'],
                            'REGION_SELECT_FLAG' => $userVals['USE_REGION_SELECT'] ? 'Y':'N',
                            "INPUT_ID" => 'PROPERTY_' . $property["CODE"],
                            "INPUT_NAME" => $property["FIELD_NAME"],
                            "LOCATION_VALUE" => $value ? $value:getDefaultCityId(),
                            "SIZE1" => $property["SIZE1"],
                            "ONCITYCHANGE" => ($property["IS_LOCATION"] == "Y" || $property["IS_LOCATION4TAX"] == "Y") ? "COrder.submitForm();" : "",
                            'LOCATION_STRING' => $property['LOCATION_STRING'] ? $property['LOCATION_STRING']:'',
                            'MANUAL_INPUT' => 'Y'
                        ),
                        null,
                        array('HIDE_ICONS' => 'Y')
                    );
                } elseif ($property["TYPE"] == "RADIO") {
                    if (is_array($property["VARIANTS"])) {
                        foreach ($property["VARIANTS"] as $arVariants) {?>
                            <input
                                type="radio"
                                name="<?= $property["FIELD_NAME"] ?>"
                                id="PROPERTY_<?= $property["CODE"] ?>_<?= $arVariants["VALUE"] ?>"
                                value="<?= $arVariants["VALUE"] ?>" <? if ($arVariants["CHECKED"] == "Y") echo " checked"; ?> />

                            <label for="<?= $property["FIELD_NAME"] ?>_<?= $arVariants["VALUE"] ?>"><?= $arVariants["NAME"] ?></label>
                        <? }
                    }
                }?>
                        </div>
                    </div>
                <?if (strlen(trim($property["DESCRIPTION"])) > 0) {?>
					<div class="hide-1024">
                        <div class="hint"><?=$property["DESCRIPTION"] ?></div>
                    </div>
                <? } ?>
            </div>
        <? }
    }
}

if (!function_exists('PrintDeliveryPropsForm')) {
    function PrintDeliveryPropsForm(array $source = array(), array $groups = array(), array $userVals = array())
    {
        $properties = array();
        foreach ($source as $property) {
            $properties[$property['CODE']] = $property;
        } ?>

        <? foreach($groups as $group) {?>
        <div class="shipping-form">
            <table cellpadding="0" cellspacing="0" class="shipping-addr">
            	<tr>
                    <?
                    foreach ($group['PROPERTIES'] as $code) {
                        $property = $properties[$code];

                        switch ($property['TYPE']) {
                            case 'TEXT':
                            	if ($property['CODE'] == 'HOUSE' || $property['CODE'] == 'STREET') {?>
                            	</tr>
                            	<tr>
                            	<?}
                                if ($property['CODE'] == 'STREET') {?>
                                    <td colspan="2">
                                <? } else {?>
                                	<td>
                                <?} ?>

                                <?if ($property['CODE'] == 'LOCATION') {
				                    $GLOBALS["APPLICATION"]->IncludeComponent(
				                        "elari:ajax.locations",
				                        '',
				                        array(
				                            'MANUAL_INPUT_ID' => $userVals['MANUAL_INPUT_ID'],
				                            'MANUAL_INPUT_NAME' => $userVals['MANUAL_INPUT_NAME'],
				                            'REGION_SELECT_FLAG' => $userVals['USE_REGION_SELECT'] ? 'Y':'N',
				                            "INPUT_ID" => 'PROPERTY_' . $property["CODE"],
				                            "INPUT_NAME" => $property["FIELD_NAME"],
				                            "LOCATION_VALUE" => 0,//$value ? $value:getDefaultCityId(),
				                            "SIZE1" => $property["SIZE1"],
				                            "ONCITYCHANGE" => ($property["IS_LOCATION"] == "Y" || $property["IS_LOCATION4TAX"] == "Y") ? "COrder.submitForm();" : "",
				                            'LOCATION_STRING' => $property['LOCATION_STRING'] ? $property['LOCATION_STRING']:'',
				                            'MANUAL_INPUT' => 'Y'
				                        ),
				                        null,
				                        array('HIDE_ICONS' => 'Y')
				                    );
                                } else {?>
	                                <div class="form-group">
		                                <input type="text" maxlength="250" size="<?= $property["SIZE1"] ?>"
		                                    value="<?= $property['VALUE'] ?>" name="<?= $property['FIELD_NAME'] ?>"
		                                    id="PROPERTY_<?= $property["CODE"] ?>"<?=$property['REQUIED_FORMATED'] == 'Y' ? ' data-required="1"':''?> placeholder="<?=$property['NAME']?>" />
		                            </div>
	                            <?}?>
                                </td>
                            <?break;
                            default:
                            break;
                        }
                    } ?>
                </tr>
            </table>
        </div>
    <? }
    }
}
