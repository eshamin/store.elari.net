<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @VAR $templateFolder string
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props_format.php");
?>
<div class="box-sh-2">
    <h2>Доставка</h2>
	<input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult['BUYER_STORE']?>" />
	<?if ($arResult['DELIVERY']) { ?>
		<div class="form-group">
		    <?
		    $i = 0;
		    $count = count($arResult['DELIVERY']);
		    foreach ($arResult['DELIVERY'] as $delivery_id => $delivery) { ?>
		        <?if ($delivery['SID'] == 'adv_ems') {
					continue;
		        }?>
				<?
				//Данная служба доставки в разработке (12.08.18)
				// if ($delivery_id == '68' && !$USER->IsAdmin()) {
					// continue;
		        // }?>
		        <label class="radio radio-bordered <?=$delivery['CHECKED'] == 'Y' ? 'on' : '';?>">
		            <input type="radio"
		                class="radiobutton_control"
		                id="ID_DELIVERY_ID_<?=$delivery['ID'] ?>"
		                name="<?=htmlspecialcharsbx($delivery['FIELD_NAME'])?>"
		                value="<?=$delivery['ID']?>" <?=$delivery['CHECKED'] == 'Y' ? ' checked':'';?>
		                onclick="COrder.changeDelivery();" />
		            <span><?=$delivery['NAME']?></span>
		            <em><?=$delivery['DESCRIPTION']?></em>
		        </label>
		    <? } ?>
		</div>

		<?foreach ($arResult['DELIVERY'] as $delivery_id => $delivery) {?>
	        <?if ($delivery['CHECKED'] == 'Y' && $arResult['ORDER_PROP']['RELATED']) {?>
				<?PrintDeliveryPropsForm($arResult['ORDER_PROP']['RELATED'], $arResult['RELATED_TEMPLATE'], $arResult['USER_VALS']);?>
	        <?}?>
		<?}?>
	<? } ?>
</div>
