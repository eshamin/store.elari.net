<?
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @VAR $templateFolder string
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if($USER->IsAuthorized() || $arParams["ALLOW_AUTO_REGISTER"] == "Y") {
	if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
		if(strlen($arResult["REDIRECT_URL"]) > 0) {
			$APPLICATION->RestartBuffer(); ?>
			<script type="text/javascript">
				window.top.location.href='<?=CUtil::JSEscape($arResult["REDIRECT_URL"])?>';
			</script>
			<?
			die();
		}
	}
} ?>
<? if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
        if(strlen($arResult["REDIRECT_URL"]) == 0) {
            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
        }
    } else  { ?>
<section class="shopping-box">
	<div class="wrapper">
		<?$APPLICATION->IncludeComponent(
			"bitrix:breadcrumb",
			"elari",
			Array(
				"PATH" => "",
				"SITE_ID" => "s1",
				"START_FROM" => "0"
			)
		);?>
		<div class="shopping-cart-wrapper" id="order_form_content">
			<? if($_POST["is_ajax_post"] == "Y") {
				$APPLICATION->RestartBuffer();
			}?>
			<div class="right">
				<?include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/products.php");?>
			</div>
		    <? if($arResult["USER_VALS"]["CONFIRM_ORDER"] == "Y" || $arResult["NEED_REDIRECT"] == "Y") {
		        if(strlen($arResult["REDIRECT_URL"]) == 0) {
		            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/confirm.php");
		        }
		    } else  { ?>
		            <form class="left" action="<?=$APPLICATION->GetCurPage();?>" method="POST" name="ORDER_FORM" id="ORDER_FORM" enctype="multipart/form-data">
		            <?=bitrix_sessid_post()?>
		        <?

		        if(!empty($arResult["ERROR"]) && $arResult["USER_VALS"]["FINAL_STEP"] == "Y") {
		            foreach($arResult["ERROR"] as $v)
		                ShowError($v);
		            ?>
		            <script type="text/javascript">
		                top.BX.scrollToNode(top.BX('ORDER_FORM'));
		            </script>
		        <?
		        }

		        ?>
		            <?
		            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/delivery.php");
		            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/person_type.php");
		            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/props.php");

		            include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/paysystem.php");
		            //include($_SERVER["DOCUMENT_ROOT"].$templateFolder."/summary.php");

		            if(strlen($arResult["PREPAY_ADIT_FIELDS"]) > 0)
		                echo $arResult["PREPAY_ADIT_FIELDS"];
		            ?>

		                <input type="hidden" name="confirmorder" id="confirmorder" value="Y" />
		                <input type="hidden" name="profile_change" id="profile_change" value="N" />
		                <input type="hidden" name="is_ajax_post" id="is_ajax_post" value="Y" />
		                <input type="hidden" name="json" value="Y" />
		            </form>
		            <div class="clear"></div>
		        <? if($_POST["is_ajax_post"] == "Y") { ?>
		            <script type="text/javascript">
		                top.BX('confirmorder').value = 'Y';
		                top.BX('profile_change').value = 'N';
		            </script>
		            <?
		            die();
		        }
		    } ?>
		</div>
	</div>
</section>
<?}?>
