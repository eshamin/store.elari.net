<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;
?>

<ul>
    <li class="h">Аксессуары:</li>

<?foreach($arResult as $itemIdex => $arItem):?>
	<li>
    	<a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
    </li>
<?endforeach;?>

</ul>