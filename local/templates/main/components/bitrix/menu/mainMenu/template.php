<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if (empty($arResult))
	return;
?>
<ul class="main-menu">

<?foreach($arResult as $itemIdex => $arItem):?>
	<li>
    	<a href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
    </li>
<?endforeach;?>
	<li class="has-submenu"><a data-menu="1"><span>Аксессуары</span></a></li>
    <li class="search">
        <a class="btn-open-search"></a>
        <div class="search-box">
            <form action="/search/" method="get">
                <input type="text" name="q" />
                <button type="submit"></button>
            </form>
        </div>
    </li>
</ul>