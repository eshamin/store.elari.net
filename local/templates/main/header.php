<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
<!DOCTYPE html>
<html>
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <meta name="viewport" content="width=device-width, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="yandex-verification" content="4d78cb10249143f2" />
    <link rel="shortcut icon" href="/favicon.ico" />
    <link rel="shortcut icon" href="<?=SITE_TEMPLATE_PATH?>/img/none.gif" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/jquery.bxslider.css">
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/easyzoom.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.6/dist/jquery.fancybox.min.css" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/style.css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/newstyle.css" rel="stylesheet" />
    <link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/media.css" />
    <link href="<?=SITE_TEMPLATE_PATH?>/css/newmedia.css" rel="stylesheet" />
    <!--[if lt IE 10]><link rel="stylesheet" type="text/css" href="<?=SITE_TEMPLATE_PATH?>/css/styleOld.css" /><![endif]-->

    <?$APPLICATION->ShowHead();?>
    <?if (!$USER->IsAuthorized()) {
        CJSCore::Init(array('ajax', 'json', 'ls', 'session', 'popup', 'pull'));
    }?>

<script type="text/javascript">
    window.heap=window.heap||[],heap.load=function(e,t){window.heap.appid=e,window.heap.config=t=t||{};var r=t.forceSSL||"https:"===document.location.protocol,a=document.createElement("script");a.type="text/javascript",a.async=!0,a.src=(r?"https:":"http:")+"//cdn.heapanalytics.com/js/heap-"+e+".js";var n=document.getElementsByTagName("script")[0];n.parentNode.insertBefore(a,n);for(var o=function(e){return function(){heap.push([e].concat(Array.prototype.slice.call(arguments,0)))}},p=["addEventProperties","addUserProperties","clearEventProperties","identify","resetIdentity","removeEventProperty","setEventProperties","track","unsetEventProperty"],c=0;c<p.length;c++)heap[p[c]]=o(p[c])};
    heap.load("2957173165");
</script>

<!-- Facebook Pixel Code -->
<script>
!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
document,'script','https://connect.facebook.net/en_US/fbevents.js');

fbq('init', '579342912223087');
fbq('track', "PageView");</script>
<noscript><img height="1" width="1" style="display:none"
src="https://www.facebook.com/tr?id=579342912223087&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code -->
<meta name="yandex-verification" content="e4ecef9e139b1f49" />
</head>
    <body>
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");

   ym(49805371, "init", {
        id:49805371,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49805371" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

			<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-56846814-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-56846814-1');
</script>
	<!-- Global site tag (gtag.js) - Google Analytics -->

    	<?$APPLICATION->ShowPanel();?>
        <!--[if lt IE 10]><div class="support">Sorry, we don't support this version of Internet Explorer.<br/><a href="http://windows.microsoft.com/en-US/internet-explorer/download-ie">Please, update your browser</a> or use another.</div><![endif]-->
        <header>
            <div class="top-header">
                <div class="left">
                    <a href="/" class="logo">
                        <img alt="Elari Smart Gadgets" src="<?=SITE_TEMPLATE_PATH?>/img/double-logo.png" />
                    </a>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "mainMenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "top",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
						),
						false
					);?>
                </div>
                <div class="right">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "miniMenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "top_right",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
						),
						false
					);?>

                    <div class="work">
                        <div class="phone"><a href="tel:+78005005600">8 800 500 56 00</a></div>
                        <div class="shedule">пн-пт: с 9.30 до 18.30</div>
                    </div>
                    <div class="sh-cart">
						<?$APPLICATION->IncludeComponent(
							"bitrix:sale.basket.basket.line",
							"topCart",
							Array(
								"HIDE_ON_BASKET_PAGES" => "N",
								"PATH_TO_AUTHORIZE" => "",
								"PATH_TO_BASKET" => "/personal/basket/",
								"PATH_TO_ORDER" => "/personal/order/make/",
								"PATH_TO_PERSONAL" => "/personal/",
								"PATH_TO_PROFILE" => SITE_DIR."personal/",
								"PATH_TO_REGISTER" => SITE_DIR."login/",
								"POSITION_FIXED" => "N",
								"SHOW_AUTHOR" => "N",
								"SHOW_EMPTY_VALUES" => "Y",
								"SHOW_NUM_PRODUCTS" => "Y",
								"SHOW_PERSONAL_LINK" => "Y",
								"SHOW_PRODUCTS" => "N",
								"SHOW_TOTAL_PRICE" => "Y",
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
							)
						);?>
                    </div>
                </div>
            </div>
            <div class="bottom-header">
                <div class="submenu" data-menu="1">
					<?$APPLICATION->IncludeComponent("bitrix:menu", "accessoriesMenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "accessories",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
							"CACHE_TIME" => "36000000",
							"CACHE_TYPE" => "A",
						),
						false
					);?>
                </div>
            </div>
            <div class="header-mini">
				<?$APPLICATION->IncludeComponent(
					"bitrix:sale.basket.basket.line",
					"mobileCart",
					Array(
						"HIDE_ON_BASKET_PAGES" => "N",
						"PATH_TO_AUTHORIZE" => "",
						"PATH_TO_BASKET" => "/personal/basket/",
						"PATH_TO_ORDER" => "/personal/order/make/",
						"PATH_TO_PERSONAL" => "/personal/",
						"PATH_TO_PROFILE" => SITE_DIR."personal/",
						"PATH_TO_REGISTER" => SITE_DIR."login/",
						"POSITION_FIXED" => "N",
						"SHOW_AUTHOR" => "N",
						"SHOW_EMPTY_VALUES" => "Y",
						"SHOW_NUM_PRODUCTS" => "Y",
						"SHOW_PERSONAL_LINK" => "Y",
						"SHOW_PRODUCTS" => "N",
						"SHOW_TOTAL_PRICE" => "Y",
						"CACHE_TIME" => "36000000",
						"CACHE_TYPE" => "A",
					)
				);?>
                <a class="btn-mobile-menu"></a>
                <a href="/" class="logo"><img alt="Elari Smart Gadgets" src="<?=SITE_TEMPLATE_PATH?>/img/double-logo.png" /></a>
            </div>
            <div class="mobile-menu">
                <div class="mm-wrapper">
                    <div class="columns">
                        <div class="col-1">
                            <div class="search-box">
                                <form action="/search/" method="get">
                                    <input type="text" name="q" />
                                    <button type="submit"></button>
                                </form>
                            </div>
                        </div>
                        <div class="col-2">
                            <div class="work">
                                <div class="phone">8 800 500 56 00</div>
                                <div class="shedule">пн-пт: с 9.30 до 18.30</div>
                            </div>
                        </div>
                    </div>
                    <nav>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "mobileMainMenu", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "A",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "mobile",	// Тип меню для первого уровня
								"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
							),
							false
						);?>
						<?$APPLICATION->IncludeComponent("bitrix:menu", "miniMenu", Array(
							"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
								"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
								"DELAY" => "N",	// Откладывать выполнение шаблона меню
								"MAX_LEVEL" => "1",	// Уровень вложенности меню
								"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
									0 => "",
								),
								"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
								"MENU_CACHE_TYPE" => "A",	// Тип кеширования
								"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
								"ROOT_MENU_TYPE" => "top_right",	// Тип меню для первого уровня
								"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
								"CACHE_TIME" => "36000000",
								"CACHE_TYPE" => "A",
							),
							false
						);?>
                    </nav>
                </div>
            </div>
        </header>