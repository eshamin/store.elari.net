var COrder = {

    lock: false,

    validation: {
        rules: {
            '#PROPERTY_NAME': 'required',
            '#PROPERTY_EMAIL': {
                email: true,
                required: true
            },
            '#PROPERTY_PERSONAL_PHONE': {
                phone: true,
                required: true
            },
            /*'#PROPERTY_LOCATION_val': {
                required: true
            },
            '#ORDER_PROP_CITY': {
                required: true
            },
            '#PROPERTY_STREET': 'required',
            '#PROPERTY_HOUSE': 'required',
            '#PROPERTY_HOME': 'required',
            '#PROPERTY_COMPANY': 'required',
            '#PROPERTY_DELIVERY_DATE': 'required',
            '#region-id': {
                required: true,
                min: function() {
                    return $('#region-id:visible').data('selectric') ? 1:0;
                }
            }*/
        },
        messages: {
            3: {
                '#PROPERTY_STREET': 'Обязательное поле',
                '#PROPERTY_HOUSE': 'Обязательное поле',
                '#PROPERTY_HOME': 'Обязательное поле',
                '#PROPERTY_DELIVERY_DATE': 'Обязательное поле',
                '#region-id': {
                    min: 'Это поле необходимо заполнить.'
                }
            },
            2: {}
        }
    },

    submitForm: function(val) {
        var _this = this;

        if (this.lock === true) {
            return true;
        }

        var $form = $('#ORDER_FORM');

        if (val != 'Y') {
            $('#confirmorder').val('N');
        } else if(val == 'Y') {

            var validation = {rules: {}, messages: {}},
                personTypeId = 3,
                name = '';

            for (var k in this.validation.rules) {
                if(this.validation.rules.hasOwnProperty(k)) {
                    if(name = $(k).attr('name')) {
                        validation.rules[name] = this.validation.rules[k];
                        if(this.validation.messages[personTypeId][k]) {
                            validation.messages[name] = this.validation.messages[personTypeId][k];
                        }
                    }
                }
            }

            var validator = $form.validate(validation);
            if(!validator.form()) {
                $form.find('input.error:first').focus();
                return false;
            }
        }

        this.lock = true;

        this.showWait();

        $.post(window.location.href, $form.serialize(), function(result) {

            _this.lock = false;
            _this.hideWait();

            try {
                var json = JSON.parse(result);

                if (json.error)
                    return false;
                else
                {
	                if (json.redirect)
						window.top.location.href = json.redirect;
                }
            } catch (e) {
                $('#order_form_content').html(result);
                $('#order_form_content input').placeholder();
                _this.init();
            }

            return true;
        });

        this.lock = false;
        this.hideWait();
        return true;
    },

    init: function() {

        var $container = $('#order_form_content');

        $('#ORDER_FORM').on('submit', function(e) {
            e.preventDefault();
            return false;
        }).removeData('validator');

        $container.find('select#PROPERTY_TYPE').on('change', function() {
            $container.find('.reg-form_label.__kpp').find('.required').remove();

            if($(this).val() != 'IP') {
                $container.find('.reg-form_label.__kpp').append('<span class="required">*</span>');
            }
        }).trigger('change');

        $('#PROPERTY_PERSONAL_PHONE').mask('+7 (999) 999-99-99');
        $('.phone').mask('+7 (999) 999-99-99');

        var $deliveryDate = $('#PROPERTY_DELIVERY_DATE');

        $deliveryDate.attr('readonly', true);
        $deliveryDate.mask(CSite.getMask('date'));
        $deliveryDate.datepicker({
            showOn: 'button',
            buttonImage: '/static/img/datepicker.png',
            buttonImageOnly: true,
            buttonText: 'Выберите дату',
            dateFormat: 'dd.mm.yy',
            minDate: 0
        });

        var $orderingShowHideLink = $('#orderingShowHideLink'),
            $orderingShowHideItems = $('#orderingShowHideItems');

        $orderingShowHideLink.on('click', function (e){
            e.preventDefault();

            if ($(this).hasClass('__opened') ) {
                $(this).removeClass('__opened');
                $(this).find('span').text($(this).data('down'));
            } else {
                $(this).addClass('__opened');
                $(this).find('span').text($(this).data('up'));
            }

            $orderingShowHideItems.slideToggle();
        });

        var $deliveryTypeWrap = $('#deliveryType'),
            $deliveryTypeItems = $deliveryTypeWrap.find('.ordering_delivery__type'),
            $paymentTypeWrap = $('#paymentType'),
            $paymentTypeItems = $paymentTypeWrap.find('.ordering_payment__type');

        for (var i = $deliveryTypeItems.length - 1; i >= 0; i--) {
            $deliveryTypeItems.eq(i).height($deliveryTypeItems.eq(i).height());
        }

        $deliveryTypeWrap.find('input').on('change', function () {
            var $this = $(this);

            $deliveryTypeItems.removeClass('__checked');
            $this.closest('.ordering_delivery__type').addClass('__checked');
        });

        $paymentTypeItems.find('input').on('change', function() {
            var $this = $(this);

            $paymentTypeItems.removeClass('__checked');
            $this.closest('.ordering_payment__type').addClass('__checked');
        });

        var $comment = $('#orderingComment');
        $('.ordering_delivery_add_comment').on('click', function() {
            $('html, body').animate({
                scrollTop: $comment.offset().top
            }, 1000);

            $comment.focus();
        });
    },

    changeDelivery: function() {
        this.submitForm('N');
    },

    changePaySystem: function() {
        this.submitForm('N');
    },

    setContact: function() {
        $('#region-flag').remove();
        $('#profile_change').val('Y');
        $('#PROPERTY_LOCATION_val').val('');
        $('#ORDER_PROP_CITY').val('');

        this.submitForm('N');
    },

    showWait: function() {
        CSite.showShadow();
        BX.showWait(BX('order-shadow'));
    },

    hideWait: function() {
        CSite.hideShadow();
        BX.closeWait();
    }
};

$(function() {
   COrder.init();
});