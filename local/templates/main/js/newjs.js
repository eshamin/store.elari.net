﻿$(function () {
    var isOldSafari = false;
    var sw = getScrollWidth();
    var bxp = null;
    if (bowser) {
        var bn = bowser.name;
        var vv = parseFloat(bowser.version);
        if (bn == "Safari" && vv < 6) {
            $("body").addClass("old-safari");
            isOldSafari = true;
        }
    }
    $('.main-menu .search-box input').click(function () {
        $(this).parents('.search').addClass('on');
    });
    $('.main-menu .search-box input').blur(function () {
        var v = $(this).val();
        if (v.length == 0) {
            $(this).parents('.search').removeClass('on');
        }
    });
    $('.main-menu .has-submenu a').click(function () {
        var menu_id = parseInt($(this).attr('data-menu'));
        if (isNaN(menu_id)) return false;
        if ($('header').hasClass('expanded')) {
            $('header').removeClass('expanded');
            $('li.has-submenu').removeClass('on');
            $('.bottom-header .submenu').hide();
        } else {
            $('li.has-submenu').removeClass('on');
            $(this).parent().addClass('on');
            $('.bottom-header .submenu').hide();
            $('.bottom-header .submenu[data-menu=' + menu_id + ']').show();
            $('header').addClass('expanded');
        }
    });
    $('.btn-mobile-menu').click(function () {
        $('.mobile-menu').toggle();
        $('body').toggleClass('is-mobile');
        if ($('body').hasClass('is-mobile')) {
            $('.header-mini').css('marginRight', sw);
        } else {
            $('.header-mini').css('marginRight', 0);
        }
    });
    $('input[name="shipping[]"]').change(function () {
        var i = parseInt($(this).val());
        $('.shipping-form').hide();
        $('.shipping-form[data-index='+i+']').show();
    });
    if (!isOldSafari) {
        bxp = $('.products-list-home').bxSlider({
            nextText: '›',
            prevText: '‹',
            minSlides: 2,
            maxSlides: 4,
            pager: false,
            slideWidth: 290,
        });
        $('.buy-too .products-list').bxSlider({
            nextText: '›',
            prevText: '‹',
            minSlides: 1,
            maxSlides: 4,
            pager: true,
            slideWidth: 290,
        });
        
    }
    $('.tabs-info').tabs();
    $('.tabs-info .more-details a, .tabs-info .less-details a').click(function () {
        $(this).parents('.ui-tabs-panel').toggleClass('expanded');
    });

    adjustProducts(bxp, isOldSafari);
    $(window).on("orientationchange resize", function (events) {
        adjustProducts(bxp, isOldSafari);
    });
    $('.mobile-show-more.more-buttons a').click(function () {
        $('.products-list-home').toggleClass('expanded');
        if ($('.products-list-home').hasClass('expanded')) {
            $('.mobile-show-more.more-buttons a').html('Свернуть');
        } else {
            $('.mobile-show-more.more-buttons a').html('Показать все');
        }
    });
    $('.bx-slider').bxSlider({
        mode: 'fade',
        captions: false
    });
});
function adjustProducts(bxp, isOldSafari) {
    if (!isOldSafari && $('.products-list-home').length > 0) {
        var w = $('header').width();
        if (w <= 480 && bxp != null) {
            bxp.destroySlider();
            bxp = null;
        } else {
            if (bxp == null) {
                bxp = $('.products-list-home').bxSlider({
                    nextText: '›',
                    prevText: '‹',
                    minSlides: 2,
                    maxSlides: 4,
                    pager: false,
                    slideWidth: 290,
                });
            } else {
                bxp.reloadSlider();
            }

        }
    }
    if ($('.product-image-large').length < 1) return false;
    var h = parseInt($('.product-image-large').height());
    $('.product-gallery .images').css('height', h + "px");
}
function getScrollWidth() {
    var div = document.createElement('div');

    div.style.overflowY = 'scroll';
    div.style.width = '50px';
    div.style.height = '50px';

    // при display:none размеры нельзя узнать
    // нужно, чтобы элемент был видим,
    // visibility:hidden - можно, т.к. сохраняет геометрию
    div.style.visibility = 'hidden';

    document.body.appendChild(div);
    var scrollWidth = div.offsetWidth - div.clientWidth;
    document.body.removeChild(div);

    return scrollWidth;
}