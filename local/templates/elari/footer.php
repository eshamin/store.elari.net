<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
        <footer>
            <div class="wrapper">
                <div class="bottom">
                    <div class="column">
                        <p>Юридическая информация: ООО «Соним» <br>ОГРН: 1117746941476 <br>Юр. адрес: 119331, г. Москва, проспект Вернадского, д. 29, помещение  I, этаж 1, комната 1Д.</p>
						<p>Телефон: 8 800 500 56 00, +7 (495) 120 56 00<br>Адрес офиса: 119331, Москва, Проспект Вернадского, д. 29, БЦ "Лето", 1 этаж, офис 1/29<br>Режим работы офиса: пн-пт с 9.30 до 18.30.</p>
                    </div>
					<?$APPLICATION->IncludeComponent("bitrix:menu", "bottomMenu", Array(
						"ALLOW_MULTI_SELECT" => "N",	// Разрешить несколько активных пунктов одновременно
							"CHILD_MENU_TYPE" => "left",	// Тип меню для остальных уровней
							"DELAY" => "N",	// Откладывать выполнение шаблона меню
							"MAX_LEVEL" => "1",	// Уровень вложенности меню
							"MENU_CACHE_GET_VARS" => array(	// Значимые переменные запроса
								0 => "",
							),
							"MENU_CACHE_TIME" => "36000",	// Время кеширования (сек.)
							"MENU_CACHE_TYPE" => "A",	// Тип кеширования
							"MENU_CACHE_USE_GROUPS" => "Y",	// Учитывать права доступа
							"ROOT_MENU_TYPE" => "bottom",	// Тип меню для первого уровня
							"USE_EXT" => "Y",	// Подключать файлы с именами вида .тип_меню.menu_ext.php
						),
						false
					);?>
                    <div class="column">
                        <h3>Подписаться</h3>
                        <form>
                            <div class="subscribe-form">
                                <button class="subscribe-button"></button>
                                <input type="text" class="subscribe-email" placeholder="введите свой email" />
                            </div>
                            <!--<div class="message error">Пожалуйста, заполните корректный email!</div>-->
                            <!--<div class="message success" style="display:none;">Вы подписаны!</div>-->
                        </form>
                        <!-- https://simpleicons.org/ -->
                        <ul class="social">
                            <li>
                                <a href="https://www.facebook.com/ElariRus/" target="_blank">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/facebook.svg" />
                                </a>
                            </li>
                            <li>
                                <a href="https://vk.com/elari" target="_blank">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/vk.svg" />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.instagram.com/elari_official/" target="_blank">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/instagram.svg" />
                                </a>
                            </li>
                            <li>
                                <a href="https://www.youtube.com/channel/UC2x-ItXNANNiFUJkPvutV_w" target="_blank">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/youtube.svg" />
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="copyrights">
                    <p><?=date('Y')?> &copy; Elari Store. Умные гаджеты для счастливой жизни</p>
                    <img alt="We accept VISA, MASTERCARD, AMEX, PayPal, Discover" src="<?=SITE_TEMPLATE_PATH?>/img/payment.png" />
                </div>
            </div>
        </footer>



        <div class="b_popup mfp-hide zoom-anim-dialog" id="buy_one_click">
    <div class="_title _name"></div>
    <div class="_title">Форма в разработке</div>
</div>

<script type="text/x-tmpl" id="popup-block">
    <div class="tf-popup {%=o.type ? o.type:'__info'%}">
        <div class="tf-popup_wrapper">
            <span class="tf-popup_title">{%=o.title%}</span>
            <div class="tf-popup_body">{%#o.body%}</div>
        </div>
    </div>
</script>
<?
	$APPLICATION->IncludeFile(
		SITE_TEMPLATE_PATH . '/include/counters.php',
		[],
		['mode' => 'text']
	);
?>
    </body>
</html>

