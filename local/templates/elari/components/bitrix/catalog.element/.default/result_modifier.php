<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

/**
 * @var CBitrixComponentTemplate $this
 * @var CatalogElementComponent $component
 */


$arResult['SM_IDENTICAL'] = array();
if(is_array($arResult['PROPERTIES']['SM_IDENTICAL']['VALUE']) && sizeof($arResult['PROPERTIES']['SM_IDENTICAL']['VALUE']) > 0) {
    //отличающиеся поля
    $arDiffFields = array(
        'SM_COLOR_CODE' => 'Цвет',
        'SM_ROM' => 'Память'
    );
    //добавляем поля в выборку
    $select = array('ID', 'DETAIL_PAGE_URL', 'XML_ID', 'PROPERTY_COLOR');
    foreach($arDiffFields as $field => $title) {
        $select[] = 'PROPERTY_'. $field;
    }
    //сохраняем текущие значения полей
    foreach($arDiffFields as $field => $title) {
        //добавляем текущие параметры
        $arResult['SM_IDENTICAL'][$field]['TITLE'] = $title;
        if(!empty($arResult['PROPERTIES'][$field]['VALUE'])) {
            $arResult['SM_IDENTICAL'][$field]['GOODS'][] = array(
                'XML_ID' => $arResult['XML_ID'],
                'DETAIL_PAGE_URL' => $arResult['DETAIL_PAGE_URL'],
                'VALUE' => $arResult['PROPERTIES'][$field]['VALUE'],
                'ACTIVE' => 'Y'
            );
        }
    }
    //получаем идентичные товары
    $db = \CIBlockElement::GetList(
        array('ID' => 'ASC'),
        array(
            'IBLOCK_ID' => $arResult['IBLOCK_ID'],
            '=XML_ID' => $arResult['PROPERTIES']['SM_IDENTICAL']['VALUE'],
            'ACTIVE' => 'Y'
        ),
        false,
        false,
        $select
    );

    while ($data = $db->GetNext(true, false)) {
        //заполняем отличающиеся поля товаров
        foreach($arDiffFields as $field => $title) {
            if(!empty($data["PROPERTY_{$field}_VALUE"])) {
                if($data["PROPERTY_{$field}_VALUE"] !== $arResult['PROPERTIES'][$field]['VALUE']) {
                    $arResult['SM_IDENTICAL'][$field]['GOODS'][] = array(
                        'XML_ID' => $data['XML_ID'],
                        'DETAIL_PAGE_URL' => $data['DETAIL_PAGE_URL'],
                        'VALUE' => $data["PROPERTY_{$field}_VALUE"],
                        'TITLE' => $field == 'SM_COLOR_CODE' ? $data["PROPERTY_COLOR_VALUE"]:'',
                        'ACTIVE' => 'N'
                    );
                }
            }
        }
    }
    //сортируем по значениям
    foreach($arResult['SM_IDENTICAL'] as &$arSorted) {
        usort($arSorted['GOODS'], function($a, $b) {
            return $a['VALUE'] - $b['VALUE'];
        });
    }
}


$component = $this->getComponent();
$arParams = $component->applyTemplateModifications();