<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<section class="text">
    <div class="wrapper">
        <h1><?=$arResult['NAME']?></h1>
        <img src="<?=$arResult["DETAIL_PICTURE"]["SRC"]?>" />
        <?=$arResult['DETAIL_TEXT']?>
    </div>
</section>