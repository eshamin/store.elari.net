<?php
/**
 * @copyright Copyright (c) 2015, ADV/web-engineering co.
 */

$arResult['hasGoods'] = false;
if(isset($arParams['IS_ACTIONS']) && $arParams['IS_ACTIONS'] == 'Y') {
    $catalogIblockId = \Adv\Utils::getIBlockIdByCode('catalog', '1c_catalog');
    $arResult['hasGoods'] = CIBlockElement::GetList(array(), array('IBLOCK_ID' => $catalogIblockId, 'PROPERTY_ACTIONS' => $arResult['ID']), array());
}

$arResult['PROPERTIES'] = $arResult['FIELDS'];