<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
?>
<section class="catalog-box">
    <div class="wrapper">
    <h1>Поиск</h1>
	<? if(count($arResult["SEARCH"]) > 0) {?>
	<div class="products-list ajaxResult">
		<?foreach($arResult["SEARCH"] as $arItem) {
			//debugfile($arItem);
			$arElement = CIBlockElement::GetByID($arItem['ITEM_ID'])->Fetch();
			$arFileTmp = CFile::ResizeImageGet(
					$arElement['PREVIEW_PICTURE'],
					array("width" => 490, "height" => 490),
					BX_RESIZE_IMAGE_PROPORTIONAL_ALT,
					true, array(array("name" => "sharpen", "precision" => 30))
			);
		?>

		    <div class="product-item">
		        <div class="product-image">
		            <a href="<?echo $arItem["URL"]?>">
		                <img alt="<?=strip_tags($arItem["TITLE_FORMATED"])?>" title="<?=strip_tags($arItem["TITLE_FORMATED"])?>" src="<?=$arFileTmp['src']?>" />
		            </a>
		        </div>
		        <div class="product-title">
		            <a href="<?=$arItem["URL"]?>"><?=$arItem["TITLE"]?></a>
		        </div>
		        <div class="product-excerpt">
        			<?=strip_tags(strip_tags($arItem["BODY_FORMATED"]))?>
		        </div>
		    </div>

	<?	}?>
	</div>
	<? echo $arResult["NAV_STRING"]; ?>
	<?} else {?>
		По Вашему запросу ничего не найдено
	<?}?>
	</div>
</section>