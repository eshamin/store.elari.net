<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>

<div class="box-wrapper-mini">
    <h2>Оформить заказ</h2>
    <div class="double-columns">
        <div>
            <table class="pricing">
                <tr>
                    <th>Заказ:</th>
                    <td><span><?=format($arResult['ORDER_PRICE'] + $arResult['DISCOUNT_PRICE']/* + $arResult['PRODUCT_DISCOUNT']*/)?></span>&nbsp;<span>Р</span></td>
                </tr>
                <? if($arResult['PRODUCT_DISCOUNT'] > 0) { ?>
                <tr>
                    <th>Скидка:</th>
                    <td><?=$arResult['PRODUCT_DISCOUNT']?> <span>Р</span></td>
                </tr>
                <?} ?>
                <tr>
                    <th>Доставка:</th>
                    <td><span><?=format($arResult['DELIVERY_PRICE'])?></span>&nbsp;<span>Р</span></td>
                </tr>
                <tr>
                    <th>Полная сумма заказа</th>
                    <td><div class="big"><span><?=format($arResult['ORDER_PRICE'] + $arResult['DELIVERY_PRICE'] + $arResult['TAX_PRICE']/* - $arResult['DISCOUNT_PRICE']*/)?></span>&nbsp;<span>Р</span></div></td>
                </tr>
            </table>
        </div>
        <div>
            <button class="button btn-right" onclick="COrder.submitForm('Y');">Оформить заказ</button>
        </div>
    </div>
</div>