<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="ordering_summary">
    <div class="ordering_summary__title">
        Ваш заказ: <?=\Adv\Utils::declension($arResult['TOTAL_QUANTITY'], array('товар', 'товара', 'товаров'))?> на сумму <b><?=\Adv\Utils::format($arResult['ORDER_PRICE'])?> <i class="rouble">Р</i> </b>

        <a class="ordering_summary__showhide" id="orderingShowHideLink" href="#" data-up="Свернуть" data-down="Развернуть">
            <span>Развернуть</span>
            <i class="ordering_summary__showhide_arrow"></i>
        </a>

        <a class="ordering_summary__link" href="<?=$arParams['PATH_TO_BASKET']?>">Перейти в корзину</a>
    </div>
    <div class="ordering_summary__items" id="orderingShowHideItems">
        <div class="cart_items __ordering">
            <table>
                <tbody>
                <? foreach($arResult['BASKET_ITEMS'] as $item) {?>
                    <tr class="cart_item __ordering" data-summ="<?=$item['PRICE'] * $item['QUANTITY']?>">
                        <td class="cart_item__img">
                            <a href="<?=$item['DETAIL_PAGE_URL']?>"><?=\Adv\Utils::showImage($item['PREVIEW_PICTURE'] ? $item['PREVIEW_PICTURE']:'/static/img/no_photo.png', 96, 80)?></a>
                        </td>
                        <td>
                            <div><a class="best_price__item-ttl" href="<?=$item['DETAIL_PAGE_URL']?>"><?=$item['NAME']?></a></div>
                            <div><span class="best_price__item-cat"><?=\Adv\Helper::getCatalogParentSectionName($item['CATALOG']['SECTION_ID'][0])?></span></div>
                        </td>

                        <td class="cart_item__price __single">
                            <div class="best_price__item-price">
                                <i class="subtotal_price"><?=\Adv\Utils::format($item['PRICE'] * $item['QUANTITY'])?></i> <i class="rouble">Р</i>
                            </div>
                            <div class="cart_item__multiply <?=$item['QUANTITY'] > 1 ? ' __show':''?>">
                                <i class="subtotal_count"><?=$item['QUANTITY']?></i> x <?=\Adv\Utils::format($item['PRICE'])?> <i class="rouble __lite">Р</i>
                            </div>
                        </td>
                    </tr>
                    <? if($item['GIFT']) { ?>
                        <tr data-summ="0" class="cart_item __ordering __no-border">
                            <td class="cart_item__img">
                                <a href="<?=$item['GIFT']['URL']?>">
                                    <?=\Adv\Utils::showImage($item['GIFT']['IMAGE'] ? $item['GIFT']['IMAGE']:'/static/img/no_photo.png', 96, 80)?>
                                </a>
                            </td>
                            <td>
                                <div>
                                    <a href="<?=$item['GIFT']['URL']?>" class="best_price__item-ttl">
                                        <?=$item['GIFT']['NAME']?>
                                    </a>
                                </div>
                                <div class="cart_item__gift">Подарок</div>
                            </td>
                            <td class="cart_item__price __single">
                                <div class="best_price__item-price">
                                    <i class="best_price__item-price __free">Бесплатно</i>
                                </div>
                                <div class="cart_item__multiply __show">
                                    &nbsp;
                                </div>
                            </td>
                        </tr>
                    <? } ?>
                <? } ?>
                </tbody>
            </table>
        </div>
    </div>
</div>