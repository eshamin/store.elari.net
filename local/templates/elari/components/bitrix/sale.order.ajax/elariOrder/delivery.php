<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 * @var $USER CUser
 * @VAR $templateFolder string
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<div class="box-wrapper-mini">
    <h2>Доставка</h2>
	<div class="double-columns">
		<div>
	    <input type="hidden" name="BUYER_STORE" id="BUYER_STORE" value="<?=$arResult['BUYER_STORE']?>" />

	    <? if($arResult['USER_VALS']['PERSON_TYPE_ID'] != 2) { ?>
	        <? $APPLICATION->IncludeComponent(
	            'elari:ajax.locations',
	            '',
	            array(
	                'MANUAL_INPUT_ID' => $arResult['USER_VALS']['MANUAL_INPUT_ID'],
	                'MANUAL_INPUT_NAME' => $arResult['USER_VALS']['MANUAL_INPUT_NAME'],
	                'REGION_SELECT_FLAG' => $arResult['USER_VALS']['USE_REGION_SELECT'] ? 'Y':'N',
	                'INPUT_ID' => 'PROPERTY_' . $arResult['LOCATION_PROPERTY']['CODE'],
	                'INPUT_NAME' => $arResult['LOCATION_PROPERTY']['FIELD_NAME'],
	                'LOCATION_VALUE' => $arResult['LOCATION_PROPERTY']['VALUE'] ? $arResult['LOCATION_PROPERTY']['VALUE']:getDefaultCityId(),
	                'SIZE1' => $arResult['LOCATION_PROPERTY']['SIZE1'],
	                'ONCITYCHANGE' => ($arResult['LOCATION_PROPERTY']['IS_LOCATION'] == "Y" || $arResult['LOCATION_PROPERTY']['IS_LOCATION4TAX'] == 'Y') ? 'COrder.submitForm();':'',
	                'LOCATION_STRING' => $arResult['USER_VALS']['LOCATION_STRING'] ? $arResult['USER_VALS']['LOCATION_STRING']:'',
	                'MANUAL_INPUT' => 'Y'
	            ),
	            null,
	            array('HIDE_ICONS' => 'Y')
	        );?>
	    <? }?>
		    <div class="form-group pt20px">
		        <label>Способы доставки:</label>
		    </div>
	    </div>
	    <div class="hide-1024">
	        <div class="hint">Начните вводить название и выберите нужный вариант</div>
	    </div>
	</div>

	<?if ($arResult['DELIVERY']) { ?>
	<div class="double-columns pt20px">
	    <div>
		    <div class="form-group">
		        <?
		        $i = 0;
		        $count = count($arResult['DELIVERY']);
		        foreach ($arResult['DELIVERY'] as $delivery_id => $delivery) { ?>
		            <?if ($delivery['SID'] == 'adv_ems') {
						continue;
		            }?>
					<?
					//Данная служба доставки в разработке (12.08.18)
					// if ($delivery_id == '68' && !$USER->IsAdmin()) {
						// continue;
		            // }?>
		            <label class="radio radio-bordered">
		                <input type="radio"
		                    class="radiobutton_control"
		                    id="ID_DELIVERY_ID_<?=$delivery['ID'] ?>"
		                    name="<?=htmlspecialcharsbx($delivery['FIELD_NAME'])?>"
		                    value="<?=$delivery['ID']?>" <?=$delivery['CHECKED'] == 'Y' ? ' checked':'';?>
		                    onclick="COrder.changeDelivery();" />
		                <span><?=$delivery['NAME']?></span>
		                <em><?=$delivery['DESCRIPTION']?></em>
		            </label>
		        <? } ?>
		    </div>
		</div>
		<div>
		<?foreach ($arResult['DELIVERY'] as $delivery_id => $delivery) {?>
	        <?if($delivery['CHECKED'] == 'Y' && $arResult['ORDER_PROP']['RELATED']) {?>
				<?PrintDeliveryPropsForm($arResult['ORDER_PROP']['RELATED'], $arResult['RELATED_TEMPLATE']);?>
	        <?}?>
		<?}?>
		</div>
	</div>
	<? } ?>
</div>
