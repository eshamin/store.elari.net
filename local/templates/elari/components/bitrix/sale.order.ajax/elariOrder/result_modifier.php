<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $USER CUser
 */

$useCredit = false;
$userData = CUser::GetByID($USER->GetID())->Fetch();

$arResult['TOTAL_QUANTITY'] = 0;
$arResult['PRODUCT_DISCOUNT'] = 0;
$arResult['ADDITIONAL_PRODUCTS'] = array();

foreach($arResult['BASKET_ITEMS'] as &$item) {

    $arResult['TOTAL_QUANTITY'] += $item['QUANTITY'];
    $arResult['PRODUCT_DISCOUNT'] += $item['DISCOUNT_PRICE'] * $item['QUANTITY'];

    if($item['CATALOG']['PROPERTIES']['SM_CREDIT']['VALUE'] == 1) {
        $useCredit = true;
    }

    /*if($gift = \Adv\BasketHelper::getBasketGift($item['ID'])) {
        ++$arResult['TOTAL_QUANTITY'];
        $item['GIFT'] = $gift;
    }*/
}
unset($item);

$arResult['USE_CREDIT'] = $arResult['USE_CREDIT'] && $useCredit;

if($arResult['USE_CREDIT']) {
    unset($arResult['PERSON_TYPE'][2]);
}

/*$discounts = \Adv\Utils::getCached(function() {
    $result = array();

    $db = CSaleDiscount::GetList(array('SORT' => 'ASC'), array(), false, false, array(
        'LID',
        'NAME',
        'CURRENCY',
        'DISCOUNT_VALUE',
        'DISCOUNT_TYPE',
        'ACTIVE',
        'SORT',
        'ACTIVE_FROM',
        'ACTIVE_TO',
        'PRIORITY',
        'LAST_DISCOUNT',
        'CONDITIONS',
        'XML_ID',
        'ACTIONS'
    ));
    while($discount = $db->Fetch()) {
        $discount['ACTIONS'] = unserialize($discount['ACTIONS']);
        $discount['CONDITIONS'] = unserialize($discount['CONDITIONS']);

        $result[] = $discount;
    }

    return $result;
}, array('CACHE_TIME' => 3600, 'CACHE_ID' => __METHOD__, 'CACHE_PATH' => '/adv/order/discounts'));


// для каждой платежной системы проверим наличие скидок, необходимо для вывода плашки с информацией о скидке
foreach($arResult['PAY_SYSTEM'] as $k => &$paySystem) {
    $isCredit = \Adv\Helper::isPaymentSystemOf(\Constants::PAYMENT_SYSTEM_CREDIT, $paySystem['ID']);

    if(!$arResult['USE_CREDIT'] && $isCredit) {
        unset($arResult['PAY_SYSTEM'][$k]);
    } elseif($arResult['USE_CREDIT'] && !$isCredit) {
        unset($arResult['PAY_SYSTEM'][$k]);
    }

    if($arResult['USE_CREDIT'] && $isCredit) {
        $arResult['PAY_SYSTEM'][$k]['CHECKED'] = 'Y';
        $arResult['USER_VALS']['PAY_SYSTEM_ID'] = $paySystem['ID'];
    }

    foreach($discounts as $discount) {

        if($paySystem['DISCOUNT']) {
            continue;
        }

        foreach($discount['CONDITIONS']['CHILDREN'] as $condition) {
            if($condition['CLASS_ID'] == 'CondSalePaySystem' && in_array($paySystem['ID'], $condition['DATA']['value'])) {
                foreach($discount['ACTIONS']['CHILDREN'] as $action) {
                    if($action['CLASS_ID'] == 'ActSaleBsktGrp' && $action['DATA']['Type'] == 'Discount' && $action['DATA']['Value'] > 0 && $action['DATA']['Unit'] == 'Perc') {
                        $paySystem['DISCOUNT'] = array(
                            'VALUE' => $action['DATA']['Value'],
                            'UNIT' => $action['DATA']['Unit'],
                        );

                        break;
                    }
                }
            }
        }
    }
}*/

// переформируем массив с доставками
/*if ($USER->IsAdmin()) {
	echo "<pre>", print_R($arResult["DELIVERY"]), "</pre>";
}*/

$allDelivery = array();
foreach($arResult['DELIVERY'] as $delivery_id => $delivery) {
    if($delivery_id > 0) {
        $allDelivery[$delivery_id] = $delivery;
    } else {
        foreach($delivery['PROFILES'] as $profile) {
            $allDelivery[$delivery_id . ':' . $profile['SID']] = array_merge($delivery, array(
                'ID' => $delivery_id . ':' . $profile['SID'],
                'NAME' => $delivery['TITLE'] . ': ' . $profile['TITLE'],
                'FIELD_NAME' => $profile['FIELD_NAME'],
                'CHECKED' => $profile['CHECKED'],
                'PROFILE_ID' => $profile['SID'],
            ));
        }
    }
}
$arResult['DELIVERY'] = $allDelivery;

// если для выбранного горада нет кредита, а пользователь в корзине выбрал покупку в кредит
$arResult['CREDIT_NOT_APPLIED'] = $arResult['USE_CREDIT'] && empty($arResult['DELIVERY']);

// инициализируем свойства данными из пользовательский полей
// вырезаем местоположение из общего списка свойства, будем выводить отдельно
foreach($arResult['ORDER_PROP']['USER_PROPS_Y'] as $k => $property) {
    if($property['TYPE'] == 'LOCATION') {
        if($arResult['USER_VALS']['PERSON_TYPE_ID'] != 2) {
            unset($arResult['ORDER_PROP']['USER_PROPS_Y'][$k]);
        }

        if (is_array($property['VARIANTS']) && count($property['VARIANTS']) > 0) {
            foreach ($property['VARIANTS'] as $variant) {
                if ($variant['SELECTED'] == 'Y') {

                    $fullName = array();
                    if($property['LOCATION_STRING'] && $arResult['USER_VALS']['USE_REGION_SELECT']) {
                        $fullName[] = $property['LOCATION_STRING'];
                    } elseif($variant['CITY_NAME']) {
                        $fullName[] = $variant['CITY_NAME'];
                    }
                    if($variant['REGION_NAME']) {
                        $fullName[] = $variant['REGION_NAME'];
                    }
                    if($variant['COUNTRY_NAME']) {
                        $fullName[] = $variant['COUNTRY_NAME'];
                    }


                    $property['VALUE_FORMATTED'] = implode(', ', $fullName);
                    break;
                }
            }
        }

        $arResult['LOCATION_PROPERTY'] = $property;
    } elseif($property['TYPE'] == 'TEXT') {
        if(empty($property['VALUE'])) {
            if(isset($userData[$property['CODE']])) {
                $arResult['ORDER_PROP']['USER_PROPS_Y'][$k]['VALUE'] = $userData[$property['CODE']];
            }
        }
    }

    if($property['CODE'] == 'CITY') {
        unset($arResult['ORDER_PROP']['USER_PROPS_Y'][$k]);
    }
}

// fix bitrix error on initialize related property values from profile
if($arResult['ORDER_PROP']['RELATED']) {
    $relatedProps = array();
    $relatedProfileValues = array();

    $db = CSaleOrderUserPropsValue::GetList(
        array('SORT' => 'ASC'),
        array(
            'USER_PROPS_ID' => $arResult['USER_VALS']['PROFILE_ID'],
            'USER_ID' => intval($USER->GetID()),
        ),
        false,
        false,
        array('VALUE', 'PROP_TYPE', 'VARIANT_NAME', 'SORT', 'ORDER_PROPS_ID')
    );
    while ($value = $db->Fetch()) {
        $relatedProfileValues[$value['ORDER_PROPS_ID']] = $value['VALUE'];
    }
    foreach($arResult['ORDER_PROP']['RELATED'] as &$property) {
        $relatedProps[] = $property['CODE'];
        if(empty($property['VALUE']) && !isset($_REQUEST['ORDER_PROP_' . $property['ID']]) && $relatedProfileValues[$property['ID']]) {
            $property['VALUE'] = $relatedProfileValues[$property['ID']];
        }
    }

    // related props template
    $arResult['RELATED_TEMPLATE'] = array(
        array(
            'TITLE' => $arResult['LOCATION_PROPERTY']['VALUE_FORMATTED'],
            'PROPERTIES' => array('STREET', 'HOUSE', 'PORCH', 'FLOOR', 'APARTMENTS_NUMBER')
        )
    );

    if(!array_diff(array('DELIVERY_DATE', /*'DELIVERY_TIME'*/), $relatedProps)) {
        $arResult['RELATED_TEMPLATE'][] = array(
            'TITLE' => 'Когда удобно?',
            'PROPERTIES' => array('DELIVERY_DATE', /*'DELIVERY_TIME'*/)
        );
    }

    if(!array_diff(array('ZIP'), $relatedProps)) {
        $arResult['RELATED_TEMPLATE'][] = array(
            'TITLE' => 'Ваш индекс',
            'PROPERTIES' => array('ZIP')
        );
    }
}

//Корректируем доступные способы оплаты для платных предзаказов
$preorderProducts = array(888888);
$bwCardphones = array(999999);
$bwCardphonesInBasket = false;
$preorderProductIsInBasket = false;
foreach($arResult['BASKET_ITEMS'] as $item) {
    if (in_array($item['PRODUCT_ID'], $preorderProducts)){
        $preorderProductIsInBasket = true;
    } elseif (in_array($item['PRODUCT_ID'], $bwCardphones)) {
        $bwCardphonesInBasket = true;
    }
}

if ($preorderProductIsInBasket && isset($arResult['PERSON_TYPE'][1]['CHECKED'])) {
    if (isset($arResult['PAY_SYSTEM'][1]['CHECKED']) || isset($arResult['PAY_SYSTEM'][11]['CHECKED']) || isset($arResult['PAY_SYSTEM'][12]['CHECKED'])) {
	$arResult['PAY_SYSTEM'][4]['CHECKED'] = 'Y'; // по умолчанию выбирает карты онлайн
	$arResult['USER_VALS']['PAY_SYSTEM_ID'] = 4; // если пользователь уже зарегистрирован, то предопределяет выбранную плат. систему
	$arResult['DISCOUNT_PRICE'] = 0; // на всякий случай, убираем скидки
    }
    unset($arResult['PAY_SYSTEM'][11]); //Наличные курьеру
    unset($arResult['PAY_SYSTEM'][1]); //Наличные
    unset($arResult['PAY_SYSTEM'][12]); //Банковские карты при самовывозе
} elseif ($bwCardphonesInBasket && isset($arResult['PERSON_TYPE'][1]['CHECKED'])) {
    unset($arResult['DELIVERY'][32]);
    if (isset($arResult['DELIVERY'][31]['CHECKED'])) {
        if (isset($arResult['PAY_SYSTEM'][4]['CHECKED'])) {
            $arResult['PAY_SYSTEM'][1]['CHECKED'] = 'Y'; // если самовывоз, то выбираем наличные, иначе останутся невидимые Банковские карты
            $arResult['USER_VALS']['PAY_SYSTEM_ID'] = 1;
        }
        unset($arResult['PAY_SYSTEM'][4]['CHECKED']);
        unset($arResult['PAY_SYSTEM'][4]); //Банковские карты онлайн
    }
} elseif(!$preorderProductIsInBasket && (isset($arResult['DELIVERY'][31]['CHECKED']) || isset($arResult['DELIVERY'][32]['CHECKED'])) && isset($arResult['PERSON_TYPE'][1]['CHECKED'])) {
    if (isset($arResult['PAY_SYSTEM'][4]['CHECKED'])) {
        $arResult['PAY_SYSTEM'][1]['CHECKED'] = 'Y'; // если самовывоз, то выбираем наличные, иначе останутся невидимые Банковские карты
        $arResult['USER_VALS']['PAY_SYSTEM_ID'] = 1;
    }
    unset($arResult['PAY_SYSTEM'][4]['CHECKED']);
    unset($arResult['PAY_SYSTEM'][4]); //Банковские карты онлайн
}