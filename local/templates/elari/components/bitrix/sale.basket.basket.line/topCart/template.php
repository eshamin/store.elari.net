<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
 ?>
<div class="tools-box">
    <a href="/personal/cart/" class="shopping-badge"><?=$arResult['NUM_PRODUCTS']?></a>
    <a href="/personal/cart/" class="shopping-label"><?=$arResult['PRODUCT(S)']?></a>
    <!--<div class="shopping-currency">
        <select>
            <option value="usd">usd</option>
            <option value="eur">eur</option>
            <option value="rur">rur</option>
        </select>
    </div>-->
</div>