<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/**
 * @global string $componentPath
 * @global string $templateName
 * @var CBitrixComponentTemplate $this
 */
 ?>
<div class="mobile-tools">
    <ul>
        <li>
            <a href="#" class="mobile-search"></a>
        </li>
        <li>
            <a href="/personal/cart/" class="mobile-bag"><?=$arResult['NUM_PRODUCTS']?></a>
        </li>
        <li>
            <a href="#" class="mobile-humburger"></a>
        </li>
    </ul>
</div>