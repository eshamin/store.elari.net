<? if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */

use Bitrix\Main\Localization\Loc;

$this->setFrameMode(true);

$strMainId = $this->getEditAreaId($arResult['PRODUCT_ID']);
$jsObject = 'ob'.preg_replace("/[^a-zA-Z0-9_]/", "x", $strMainId);
$paramsForJs = array(
	'buttonId' => $arResult['BUTTON_ID'],
	'jsObject' => $jsObject,
	'alreadySubscribed' => $arResult['ALREADY_SUBSCRIBED'],
	'productId' => $arResult['PRODUCT_ID'],
	'buttonClass' => htmlspecialcharsbx($arResult['BUTTON_CLASS']),
	'urlListSubscriptions' => '/',
);

$templateData = $paramsForJs;
$showSubscribe = true;

/* Compatibility with the sale subscribe option */
$saleNotifyOption = Bitrix\Main\Config\Option::get('sale', 'subscribe_prod');
if(strlen($saleNotifyOption) > 0)
	$saleNotifyOption = unserialize($saleNotifyOption);
$saleNotifyOption = is_array($saleNotifyOption) ? $saleNotifyOption : array();
foreach($saleNotifyOption as $siteId => $data)
{
	if($siteId == SITE_ID && $data['use'] != 'Y')
		$showSubscribe = false;
}
?>

<button class="button open-presale-popup"
	style="<?=($arResult['DEFAULT_DISPLAY']?'':'display: none;')?>">
	Предзаказ
	<!--<?=Loc::getMessage('CPST_SUBSCRIBE_BUTTON_NAME')?>-->
</button>
<input type="hidden" id="<?=htmlspecialcharsbx($arResult['BUTTON_ID'])?>_hidden">

<div class="overlay"></div>
<div class="popup-wrapper" id="popup-presale">
	<div class="ft">
	    <div class="ftd">
	        <div class="popup popup-contact">
	            <a class="popup-close"></a>
	            <h3>Предзаказ</h3>
	            <p></p>
	            <div class="form two-col">
	                <div class="row">
	                    <input type="text" id="subscribeName" required name="name" data-required="1" placeholder="Ваше имя *" class="input" />
	                </div>
	                <div class="row">
	                    <input type="text" id="subscribeEmail" required name="email" data-required="1" data-type="email" placeholder="E-mail *" class="input" />
	                </div>
	            </div>
	            <div class="form">
	                <div class="row">
	                    <input type="text" id="subscribePhone" required name="phone" data-required="1" placeholder="Телефон *" class="input phone" />
	                </div>
	                <div class="row">
	                    <input type="text" id="subscribeCity" name="city" data-required="0" placeholder="Город" class="input" />
	                </div>
	            </div>
	            <div class="buttons">
	                <button class="button" onclick="CBasket.presale(<?=$arResult['PRODUCT_ID']?>)">Сделать предзаказ</button>
	            </div>
	        </div>
	    </div>
	</div>
</div>

<div class="popup-wrapper" id="popup-preorder-result">
	<div class="ft">
	    <div class="ftd">
	        <div class="popup popup-contact">
	            <a class="popup-close"></a>
	            <h3>Предзаказ</h3>
	            <p></p>
				<div class="message success">
				    Спасибо! Как только товар появится на складе, мы Вам обязательно сообщим!
				</div>
			</div>
		</div>
	</div>
</div>