<!-- MarketGid Sensor -->

<script type="text/javascript">

    (function() {

        var d = document, w = window;

        w.MgSensorData = w.MgSensorData || [];

        w.MgSensorData.push({

            cid:362507,

            lng:"ru",

            nosafari:true,

            project: "a.marketgid.com"

        });

        var l = "a.marketgid.com";

        var n = d.getElementsByTagName("script")[0];

        var s = d.createElement("script");

        s.type = "text/javascript";

        s.async = true;

        var dt = !Date.now?new Date().valueOf():Date.now();

        s.src = "//" + l + "/mgsensor.js?d=" + dt;

        n.parentNode.insertBefore(s, n);

    })();

</script>

<!-- /MarketGid Sensor -->