/* HTML5 Placeholder jQuery Plugin - v2.1.0
 * Copyright (c) 2014 Mathias Bynens
 * 2014-12-29
 */
!function(a){"function"==typeof define&&define.amd?define(["jquery"],a):a(jQuery)}(function(a){function b(b){var c={},d=/^jQuery\d+$/;return a.each(b.attributes,function(a,b){b.specified&&!d.test(b.name)&&(c[b.name]=b.value)}),c}function c(b,c){var d=this,f=a(d);if(d.value==f.attr("placeholder")&&f.hasClass(m.customClass))if(f.data("placeholder-password")){if(f=f.hide().nextAll('input[type="password"]:first').show().attr("id",f.removeAttr("id").data("placeholder-id")),b===!0)return f[0].value=c;f.focus()}else d.value="",f.removeClass(m.customClass),d==e()&&d.select()}function d(){var d,e=this,f=a(e),g=this.id;if(""===e.value){if("password"===e.type){if(!f.data("placeholder-textinput")){try{d=f.clone().attr({type:"text"})}catch(h){d=a("<input>").attr(a.extend(b(this),{type:"text"}))}d.removeAttr("name").data({"placeholder-password":f,"placeholder-id":g}).bind("focus.placeholder",c),f.data({"placeholder-textinput":d,"placeholder-id":g}).before(d)}f=f.removeAttr("id").hide().prevAll('input[type="text"]:first').attr("id",g).show()}f.addClass(m.customClass),f[0].value=f.attr("placeholder")}else f.removeClass(m.customClass)}function e(){try{return document.activeElement}catch(a){}}var f,g,h="[object OperaMini]"==Object.prototype.toString.call(window.operamini),i="placeholder"in document.createElement("input")&&!h,j="placeholder"in document.createElement("textarea")&&!h,k=a.valHooks,l=a.propHooks;if(i&&j)g=a.fn.placeholder=function(){return this},g.input=g.textarea=!0;else{var m={};g=a.fn.placeholder=function(b){var e={customClass:"placeholder"};m=a.extend({},e,b);var f=this;return f.filter((i?"textarea":":input")+"[placeholder]").not("."+m.customClass).bind({"focus.placeholder":c,"blur.placeholder":d}).data("placeholder-enabled",!0).trigger("blur.placeholder"),f},g.input=i,g.textarea=j,f={get:function(b){var c=a(b),d=c.data("placeholder-password");return d?d[0].value:c.data("placeholder-enabled")&&c.hasClass("placeholder")?"":b.value},set:function(b,f){var g=a(b),h=g.data("placeholder-password");return h?h[0].value=f:g.data("placeholder-enabled")?(""===f?(b.value=f,b!=e()&&d.call(b)):g.hasClass(m.customClass)?c.call(b,!0,f)||(b.value=f):b.value=f,g):b.value=f}},i||(k.input=f,l.value=f),j||(k.textarea=f,l.value=f),a(function(){a(document).delegate("form","submit.placeholder",function(){var b=a("."+m.customClass,this).each(c);setTimeout(function(){b.each(d)},10)})}),a(window).bind("beforeunload.placeholder",function(){a("."+m.customClass).each(function(){this.value=""})})}});
var isMobile = false; //initiate as false
// device detection
if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
    || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

$(function () {
    var isOldSafari = false;
    if (bowser) {
        var bn = bowser.name;
        var vv = parseFloat(bowser.version);
        if (bn == "Safari" && vv < 6) {
            $("body").addClass("old-safari");
            isOldSafari = true;
        }
    }
    $(".video").click(function (event) {
        if ($(this).hasClass("on")) return false;
        $(".video").addClass("on");
        var ifrm = $(this).find("iframe:first");
        var src = ifrm.attr("src") + "?autoplay=1";
        ifrm.attr("src", src);
    });
    if (!isOldSafari) {
        $('.intro').bxSlider({
            nextText: '›',
            prevText: '‹',
            useCSS: false,
            infiniteLoop: true,
            easing: 'jswing',
            pager: true
        });
         $('.intro2').bxSlider({
           // nextText: '›',
           // prevText: '‹',
            controls: false,
              infiniteLoop: false
        });
        $('#colors').bxSlider({
            nextText: '›',
            prevText: '‹',
            minSlides: 2,
            maxSlides: 2,
            slideWidth: 52,
            pager: false
        });
    }
    $(".mobile-search").click(function () {
        $("header nav").removeClass("on");
        $("header .search-box").toggleClass("on");
    });
    $(".mobile-humburger").click(function () {
        $("header nav").toggleClass("on");
        $("header .search-box").removeClass("on");
    });
    $(".sidebar .menu-tail").click(function () {
        $(this).parents(".sidebar").toggleClass("on");
    });

    $(".popup .popup-close").click(function () {
        $(".overlay").fadeOut(600);
        $(this).parents(".popup-wrapper").removeClass("on");
    });
    $(".open-presale-popup").click(function () {
        $(".overlay").fadeIn(300);
        $("#popup-presale").addClass("on");
    });

    $("input[name='payment[]']:checked").parent().addClass("on");
    $("input[name='payment[]']").change(function () {
        var is_checked = $(this).prop("checked");
        $("input[name='payment[]']").parent().removeClass("on");
        if (is_checked) {
            $(this).parent().addClass("on");
        }
    });

    $("input[name='shipping[]']:checked").parent().addClass("on");
    $("input[name='shipping[]']").change(function () {
        var is_checked = $(this).prop("checked");
        $("input[name='shipping[]']").parent().removeClass("on");
        if (is_checked) {
            $(this).parent().addClass("on");
        }
    });
    /* counter for shopping cart */

    /* increase count */
    $(".counter .plus").click(function () {
        var val = parseInt($(this).parent().find(".counter-index em").attr("data-count"));
        if (isNaN(val)) {
            $(this).parent().find(".counter-index em").attr("data-count", 1);
            $(this).parent().find(".counter-index em").html(1);
        }
        else {
            $(this).parent().find(".counter-index em").attr("data-count", val + 1);
            $(this).parent().find(".counter-index em").html(val + 1);
        }
    });

    /* decrease count */
    $(".counter a.minus").click(function () {
        var val = parseInt($(this).parent().find(".counter-index em").attr("data-count"));
        if (isNaN(val)) {
            $(this).parent().find(".counter-index em").attr("data-count", 1);
            $(this).parent().find(".counter-index em").html(1);
        }
        else {
            if (val > 1) {
                $(this).parent().find(".counter-index em").attr("data-count", val - 1);
                $(this).parent().find(".counter-index em").html(val - 1);
            }
            if (val <= 1) {
                $(this).parent().find(".counter-index em").attr("data-count", 1);
                $(this).parent().find(".counter-index em").html(1);
            }
        }
    });

    /* recalculate price if counter is changed */
    $(".shopping-cart-table .counter a.plus, .shopping-cart-table .counter a.minus").click(function () {
        var count = parseInt($(this).parent().find(".counter-index em").attr("data-count"));

        var str_per_one = $(this).parent().parent().parent().find(".col-price .price").attr("data-price");
        var per_one = parseFloat(str_per_one);
        var sum = 0;
        if (!isNaN(count) && !isNaN(per_one)) {
            sum = count * per_one;
            sum = sum.toFixed(2);
        }
        $(this).parent().parent().parent().find(".col-amount .price").attr("data-price", sum);
        $(this).parent().parent().parent().find(".col-amount .price").html(formatPrice(sum));
        var total = recalculateTotalPrice();
        $(".shopping-cart-table .amout-payment").attr("data-price", total[0]);
        $(".shopping-cart-table .amout-payment").html(total[1]);
    });

    /* remove item from shopping cart */
    $(".shopping-cart-table .btn-remove").click(function () {
        var obj = $(this).parent().parent();
        obj.fadeOut(500, function () {
            obj.remove();
            var total = recalculateTotalPrice();
            $(".shopping-box .amout-payment").attr("data-price", total[0]);
            $(".shopping-box .amout-payment").html(total[1]);
            if (total[0] == 0) {
                $(".shopping-cart-empty").show();
                $(".shopping-cart-table").hide();
            }
        });
    });

    if (!isOldSafari && !isMobile) {
        var $easyzoom = $('.easyzoom').easyZoom();
        var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');
        $('.thumbnails').on('click', 'a', function (e) {
            var $this = $(this);
            e.preventDefault();
            api1.swap($this.data('standard'), $this.attr('href'));
        });
    } else {
        $(".product-image-large a").removeAttr("href");
        $(".images.thumbnails a").each(function () {
            var href = $(this).attr("href");
            $(this).removeAttr("href");
            $(this).attr("data-large", href);
        });
        $(".images.thumbnails a").click(function () {
            var url = $(this).attr("data-large");
            $(".product-image-large a").removeAttr("href");
            $(".product-image-large img:first").attr("src", url);
        });
    }

	$('body').on('click', '.btn_load_more', function (e) {
		e.preventDefault();

		$.get($(this).attr('href'))
			.done(function (result) {
			$('._load_more').remove();

			$('.ajaxResult').append($(result).find('.ajaxResult').html());

			if ($(result).find('._load_more').length > 0) {
				$('.ajaxResult').after($(result).find('._load_more'));
			}
		});
	});

	$('body').on('click', '.btn_load_all', function (e) {
		e.preventDefault();
		$('.paar').removeClass('hidden');
		$(this).addClass('hidden');
		$('._load_more_categories').addClass('hidden');
	});

    $('.order-select').on('change', function (e) {
        window.location = $(this).val();
    });
    $('#PROPERTY_PERSONAL_PHONE').mask('+7 (999) 999-99-99');

	$('.jsPopup').magnificPopup({
		type: 'inline',
		fixedContentPos: false,
		fixedBgPos: true,
		overflowY: 'auto',
		closeBtnInside: true,
		preloader: false,
		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});
});

/* recalculate price
 * return array [float format, string format] */
function recalculateTotalPrice() {
    var amount = 0;
    if ($(".shopping-cart-table tbody tr").length) {
        var i = 0;
        $(".shopping-cart-table tbody tr").each(function () {
            amount = amount + parseFloat($(this).find(".col-amount .price:first").attr("data-price"));
            i++;
        });
        amount1 = formatPrice(amount);
        $("#shop-cart-amount-price").html(amount1);
        return [amount, amount1];
    } else {
        $("#shop-cart-amount-price").html(0);
        return [0, 0];
    }
}

/* function for money preformating (float) */
function formatPrice(float) {
    var string = float.toString();
    var floatPart = "";
    var intPart = "";
    var dot = string.indexOf(".");
    if (dot != -1) {
        if (string.indexOf(".00") == -1) {
            floatPart = string.substr(dot, string.length - 1);
        }
        intPart = string.substr(0, dot);
        intPart = formatMoney(intPart, "", "", " ", " ");
    } else {
        intPart = formatMoney(string, "", "", " ", " ");
    }
    return intPart + floatPart;
}

/* function for money format (integer) */
function formatMoney(number, places, symbol, thousand, decimal) {
    number = number || 0;
    places = !isNaN(places = Math.abs(places)) ? places : 2;
    symbol = symbol !== undefined ? symbol : "$";
    thousand = thousand || ",";
    decimal = decimal || ".";
    var negative = number < 0 ? "-" : "",
        i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + "",
        j = (j = i.length) > 3 ? j % 3 : 0;
    return symbol + negative + (j ? i.substr(0, j) + thousand : "") + i.substr(j).replace(/(\d{3})(?=\d)/g, "$1" + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : "");
}





    $( document ).ready(function() {
    $('.bx-controls-direction:eq(1)').css('display','none');
     $('.bx-default-pager:eq(0)').addClass('slider_control_lg');

});
