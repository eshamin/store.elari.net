var CBasket = {

    gifts: {},
    accessories: {},

    productsInBasket: [],

    action: function(action, params) {

        if(!CSite.lock()) {
            return false;
        }

        var deferred = $.Deferred();

        $.post('/ajax/basket.php', {action: action, params: params, sessid: BX.bitrix_sessid()}, function(data) {
            CSite.unLock();

            if(data.status == 'ERR') {
                CSite.showMessage('Ошибка', data.message);
                deferred.reject(data);
                return false;
            }

            deferred.resolve(data);

            return true;
        }, 'json').always(function() {
            CSite.unLock();
        });

        return deferred.promise();
    },

    presale: function(id) {

        var _this = this;
		_this.action('get.product', {product_id: id}).done(function(data) {
	        var $name = $('#subscribeName');
	        var $email = $('#subscribeEmail');
			var $phone = $('#subscribePhone');
			var $city = $('#subscribeCity');

	        /*var validator = $(this).validate({
	            rules: {
	                name: 'required',
	                email: {
	                    required: true,
	                    email: true
	                }
	            }
	        });

	        if(!validator.form()) {
	            return false;
	        }*/

	        _this.action('pre.order', {
	            product_id: id,
	            name: $name.val(),
	            email: $email.val(),
	            phone: $phone.val(),
	            city: $city.val()
	        }).done(function() {
	        	$('#popup-presale').removeClass('on');
	            $('#popup-preorder-result').addClass('on');
	        });

	        return false;
		});
    },

    buyInOneClick: function(id, available) {

        var _this = this;

        if(available) {
            return _this.action('get.product', {product_id: id}).done(function(data) {
                CSite.showMessage('Купить в один клик', tmpl('popup-oneclick-form', data.result));

                //begun tracker
                (function(w, p) {
                    var a, s;
                    (w[p] = w[p] || []).push({
                        counter_id: 423693735,
                        tag: 'abb06b31a6b5b804ff7b73fca587e744'
                    });
                    a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
                    a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
                    s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
                })(window, 'begun_analytics_params');

//                $('#singlePhone').mask(CSite.getMask('phone'));

                $('#one-click-order').on('submit', function() {

                    var validator = $(this).validate({
                        rules: {
                            'name': 'required',
                            'phone': {
                                required: true,
//                                phone: true
                           },
                            'email': {
                                email: {
                                    required: true,
                                    email:true,
                                    // depends: function(element) {
                                    //     return $("#singleEmail").val();
                                    // }
                                }
                            }
                        }
                    });

                    if(!validator.form()) {
                        return false;
                    }

                    var $name = $('#singleName');
                    var $phone = $('#singlePhone');
                    var $email = $('#singleEmail');

                    _this.action('order', {
                        product_id: data.result.product.id,
                        name: $name.val(),
                        phone: $phone.val(),
                        email: $email.val()
                    }).done(function(data) {
                        CSite.showMessage('Купить в один клик', tmpl('popup-oneclick-result', data.result));

                        //begun tracker
                        (function(w, p) {
                            var a, s;
                            (w[p] = w[p] || []).push({
                                counter_id: 423693735,
                                tag: '00baed38d362212d641c65de765a5cf7'
                            });
                            a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
                            a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
                            s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
                        })(window, 'begun_analytics_params');

                        if (window.ga) {
                        	window.ga('send', 'event', 'buy_one_click_form', 'buy_one_click_form_send');

                            window.ga('require', 'ecommerce', 'ecommerce.js');

                            window.ga('ecommerce:addTransaction', {
                                'id': data.result.order.id,
                                'affiliation': data.result.order.site,
                                'revenue': data.result.order.price,
                                'shipping': data.result.order.price_delivery,
                                'tax': '0'
                            });

                            for (var item in data.result.items) {
                                if (data.result.items.hasOwnProperty(item)) {
                                    window.ga('ecommerce:addItem', {
                                        'id': data.result.order.id,
                                        'name': data.result.items[item].name,
                                        'sku': data.result.items[item].product_id,
                                        'category': '',
                                        'price': data.result.items[item].price,
                                        'quantity': data.result.items[item].quantity
                                    });
                                }
                            }

                            window.ga('ecommerce:send');
                        }

                        if (window.fbq)
							window.fbq('track', 'Purchase', {value: data.result.order.price, currency: 'RUB'});

                        rrApiOnReady.push(function() {
                            try {
                                rrApi.order({
                                    transaction: data.result.order.id,
                                    items: [
                                        {id: data.result.items[0].product_id, qnt: data.result.items[0].quantity, price: data.result.items[0].price}
                                    ]
                                });
                            } catch(e) {}
                        });

                    });

                    return false;
                });
            });
        }

        return new $.Deferred().promise();
    },

    buySet: function(productId, setItemsId) {
        if(setItemsId.length <= 0) {
            CSite.showMessage('Ошибка', 'Вы не выбрали ни одного товара');
            return false;
        }

        return CBasket.action('buy.set', {product_id: productId, items: setItemsId}).done(function(data) {
            if(data.status == 'ERR') {
                CSite.showMessage('Ошибка', data.message);
                return false;
            }

            CSite.showMessage('Комплект добавлен в корзину', tmpl('popup-buy-set', data.result));

            return true;
        });
    },

    setInBasket: function(productsInBasket) {
        this.productsInBasket = productsInBasket;

        for(var i = 0, k = this.productsInBasket.length; i < k; ++i) {
            $('.best_price__oncart[data-product_id="' + this.productsInBasket[i] + '"]').addClass('in-basket').data('bought', 1);
        }
    },

    setGifts: function(gifts) {
        this.gifts = gifts;
    },

    getGifts: function(productId) {
        return this.gifts[productId];
    },

    setAccessories: function(accessories) {
        this.accessories = accessories;
    },

    getAccessories: function(productId) {
        return this.accessories[productId];
    }
};