var CSite = {

    hasLock: false,

    masks: {
        phone: '+7 (000) 000-00-00',
        kpp: '000000000',
        inn: '000000000000',
        zip: '000000',
        date: '00.00.0000'
    },

    getMask: function(code) {
        return this.masks[code];
    },

    lock: function() {
        if(this.hasLock) {
            return false;
        }

        this.hasLock = true;
        return true;
    },

    unLock: function() {
        this.hasLock = false;
        return true;
    },

    showMessage: function(title, body, type) {
        title = title || 'Уведомление';
        type = type || '';

        $.magnificPopup.open({
            items: {
                src: tmpl('popup-block', {title: title, body: body, type: type}),
                type: 'inline'
            },
            callbacks: {
                close: function () {
                    $('body').trigger('adv.closeMessage', {type: type});
                }
            },
            preloader: false,
            closeBtnInside: true,
            focus: 'input[autofocus]'
        });
    },

    hideMessage: function() {
        $.magnificPopup.close();
        return false;
    },

    showShadow: function(){
        $('#page-wrapper').append('<div id="shadow" class="order-shadow" style="display: block;" />');
    },

    hideShadow: function() {
        $('#shadow').remove();
    },

    setSystemParam: function(action, value) {
        if(!CSite.lock()) {
            return false;
        }

        $.post('/ajax/system.php', {action: action, value: value, sessid: BX.bitrix_sessid()}, function(data) {
            if(data.status == 'OK') {
                window.location.reload();
            }
        }, 'json');

        return false;
    },

    declension: function (num, expressions) {
        var result;
        var count = num % 100;

        if(!$.isArray(expressions)) {
            expressions = expressions.split(' ');
        }

        if (count >= 5 && count <= 20) {
            result = expressions['2'];
        } else {
            count = count % 10;
            if (count == 1) {
                result = expressions['0'];
            } else if (count >= 2 && count <= 4) {
                result = expressions['1'];
            } else {
                result = expressions['2'];
            }
        }
        return result;
    },

    checkEmail: function (email) {
        return /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(email);
    },

    trim : function(str, charlist){
        charlist = !charlist ? ' \s\xA0' : charlist.replace(/([\[\]\(\)\.\?\/\*\{\}\+\$\^\:])/g, '\$1');
        var re = new RegExp('^[' + charlist + ']+|[' + charlist + ']+$', 'g');
        return str.replace(re, '');
    },

    validate: function(form, callback) {
        var errors = [];

        $(form).find('input[data-required="1"]').each(function() {
            var $this = $(this);

            $this.removeClass('error');
            $this.parent().removeClass('error');

            switch($this.data('type')) {
                case 'email':
                    if($this.val().length <= 0 || !CSite.checkEmail($this.val())) {
                        $this.addClass('error');
                        $this.parent().addClass('error');
                        errors.push('Укажите корректный Email адрес!');
                    }
                    break;

                case 'equal':
                    if($this.val() != $(form).find('input[name="' + $this.data('equal') + '"]').val()) {
                        $this.addClass('error');
                        $this.parent().addClass('error');
                        errors.push($(this).attr('placeholder'));
                    }
                    break;

                default:
                    if($this.val().length <= 0) {
                        $this.addClass('error');
                        $this.parent().addClass('error');
                        errors.push('Не заполнено поле ' + $(this).attr('placeholder'));
                    }
                    break;
            }
        });

        if(typeof callback == 'function') {
            callback(form, errors);
        }

        return errors.length <= 0;
    },

    openWindow: function (url, winWidth, winHeight) {
        var winTop = (screen.height / 2) - (winHeight / 2);
        var winLeft = (screen.width / 2) - (winWidth / 2);
        window.open(url, '', 'top=' + winTop + ',left=' + winLeft + ',toolbar=0,status=0,width=' + winWidth + ',height=' + winHeight);
    },

    getCookie: function(name) {
        name = BX.message('COOKIE_PREFIX') + '_' + name;

        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]):undefined;
    },

    setCookie: function(name, value, options) {
        name = BX.message('COOKIE_PREFIX') + '_' + name;
        options = options || {};

        var expires = options.expires;

        if (typeof expires == 'number' && expires) {
            var d = new Date();
            d.setTime(d.getTime() + expires*1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }

        value = encodeURIComponent(value);

        var updatedCookie = name + '=' + value;

        for(var propName in options) {
            if(options.hasOwnProperty(propName)) {
                updatedCookie += '; ' + propName;
                var propValue = options[propName];
                if (propValue !== true) {
                    updatedCookie += '=' + propValue;
                }
            }
        }

        document.cookie = updatedCookie;
    }
};

$(function() {

    var $body = $('body');

    $('.best_price__oncart[data-product_id]').on('click', function() {

        var _this = this;
        var product_id = $(this).data('product_id');
        var available = $(this).data('available') == 1;
        var gift = $(this).data('gift');

        CBasket.add2Basket(product_id, available, gift).done(function() {
            if(available) {
                $(_this).data('bought', 1);
                $(_this).addClass('in-basket');

                $('body').trigger('adv.afterAdd2Basket');

                //begun tracker
                (function(w, p) {
                    var a, s;
                    (w[p] = w[p] || []).push({
                        counter_id: 423693735,
                        tag: 'f3b2b675376e11faef67a9331690c0bc'
                    });
                    a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
                    a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
                    s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
                })(window, 'begun_analytics_params');

            }
        });

        // GA tracker
        if (available)
        	ga('send', 'event', 'add_to_cart', 'add_to_cart_send');
        else
        	ga('send', 'event', 'preorder', 'preorder_send');

        return false;
    });

    $('a.best_price__oneclick[data-product_id]').on('click', function() {

        var product_id = $(this).data('product_id');
        var available = $(this).data('available') == 1;

        CBasket.buyInOneClick(product_id, available);

        //begun tracker
        (function(w, p) {
            var a, s;
            (w[p] = w[p] || []).push({
                counter_id: 423693735,
                tag: 'f3b2b675376e11faef67a9331690c0bc'
            });
            a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
            a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
            s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
        })(window, 'begun_analytics_params');

        // GA tracker
        ga('send', 'event', 'buy_one_click', 'buy_one_click_send');

        return false;
    });

    $('a.best_price__credit[data-product_id]').on('click', function(e) {
        e.preventDefault();

        var product_id = $(this).data('product_id');
        var available = $(this).data('available') == 1;

        CSite.showMessage('Купить в кредит', tmpl('popup-credit-block', {product_id: product_id, available: available}), '__credit');

        $('#buy-in-credit').on('click', function() {
            CBasket.add2Basket($(this).data('product_id'), $(this).data('available')).done(function() {
                if(available) {
                    $('.best_price__oncart[data-product_id="' + product_id + '"]').data('bought', 1).addClass('in-basket');
                }
            });
        })
    });

    $('#set-buy-button').on('click', function() {
        var setItemsId = [];

        var productId = parseInt($(this).data('product_id'), 10);
        var $items = $('input[name="set_item_id"]:checked');

        $items.each(function(){
            setItemsId.push(parseInt($(this).data('product_id'), 10));
        });
        setItemsId = setItemsId.splice(1,setItemsId.length);
        CBasket.buySet(productId, setItemsId);

        //begun tracker
        (function(w, p) {
            var a, s;
            (w[p] = w[p] || []).push({
                counter_id: 423693735,
                tag: 'f3b2b675376e11faef67a9331690c0bc'
            });
            a = document.createElement('script'); a.type = 'text/javascript'; a.async = true;
            a.src = ('https:' == document.location.protocol ? 'https://' : 'http://') + 'autocontext.begun.ru/analytics.js';
            s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(a, s);
        })(window, 'begun_analytics_params');

        return false;
    });

    $('.item_combo__present').find('input[name="gift"]').on('change', function() {
        var $this = $(this),
            $buyButton = $('#gift-buy-button');

        $buyButton.data('gift', $this.val());

        var attr = $buyButton.attr('disabled');
        if (typeof attr !== typeof undefined && attr !== false) {
            $buyButton.removeAttr('disabled');
        }
    });

    // Показать popup добавления отзыва
    $body.on('click', '#add_review_detail',function() {

        CSite.showMessage($('#item-page').data('item_name'), tmpl('popup-addReview', {user_id: BX.message('USER_ID')}), '__review');

        var $form = $('#popup-review-form');

        $form.find('.js-rating').raty({
            path: '/static/img/',
            starOff: 'star_off.png',
            starOn: 'star_on.png',
            scoreName: $(this).data('name'),
            readOnly: $(this).data('readonly'),
            score: function() {
                return $(this).attr('data-score');
            }
        });

        var validator = $form.validate({
            rules: {
                score: 'required',
                user_name: 'required',
                comment: {
                    required: true,
                    maxlength: 400
                }
            },
            messages: {
                score: 'Пожалуйста, оцените товар.'
            }
        });

        // Добавить отзыв
        $form.on('submit', function() {
            if(validator.form()) {

                if(!CSite.lock()) {
                    return false;
                }

                var post = $form.serializeArray();

                post.push({name: 'action', value: 'add'});
                post.push({name: 'sessid', value: BX.bitrix_sessid()});

                $.post('/ajax/review.php', post, function(data) {
                    if(data.status == 'OK'){
                        CSite.showMessage('Спасибо за отзыв!', 'Ваш отзыв будет опубликован через некоторое время после проверки модератором.');
                    } else if(data.status == 'ERR'){
                        CSite.showMessage('Ошибка', data.message);
                    }
                }, 'json').always(function() {
                    CSite.unLock();
                });

            }

            return false;
        });

        return false;
    });

    $('#content').on('click', '.personal_order__cancel[data-order_id]', function() {

        var id = $(this).data('order_id');
        var number = $(this).data('account_number');
        var button = this;

        CSite.showMessage('Отмена заказа №' + number, tmpl('order-cancel', {id: id, number: number}), '__cancel');

        $('#order-cancel-form').on('submit', function() {
            $(button).remove();
            $('#order-' + id).find('.personal_order__status').addClass('__cancel').removeClass('__wait').html('Отменен');

            CSite.hideMessage();

            CBasket.action('cancel.order', {order_id: id, reason: $(this).find('textarea[name="reason"]').val()});
            return false;
        });

        return false;
    });

    $('a.system-set').on('click', function() {
        CSite.setSystemParam($(this).data('action'), $(this).data('value'));
        return false;
    });

    $('select.system-set').on('change', function() {
        CSite.setSystemParam($(this).data('action'), $(this).val());
        return false;
    });

    var $subscribeField = $('.subscribe-email');
    var $subscribeButton = $('.subscribe-button');
    var defaultEmail = CSite.getCookie('LOGIN');

    if(defaultEmail) {
        $subscribeField.val(defaultEmail);
        $subscribeButton.prop('disabled', false);
    }

    $subscribeField.on('change keyup', function() {
        var $this = $(this);

        if($this.val()) {
            $this.parent().find('.subscribe-button').prop('disabled', false);
        } else {
            $this.parent().find('.subscribe-button').prop('disabled', true);
        }
    });

    $subscribeButton.on('click', function() {
        if(!CSite.lock()) {
            return false;
        }

        var postParams = {
                email: $(this).parent().find('.subscribe-email').val(),
                sessid: BX.bitrix_sessid()
            };

        $.post('/ajax/subscribe.php', postParams, function(data) {
            CSite.unLock();
            //CSite.showMessage(data.status == 'ERR' ? 'Ошибка':null, data.message);
            alert(data.message);
			//rrApiOnReady.push(function () { rrApi.setEmail(postParams.email); });
        }, 'json');

        return false;
    });

    $('.floor_menu_link').on('click', function() {
        if(window.location.pathname == '/' && parseInt($(this).data('brand'), 10) <= 0) {
            var id = $(this).data('floor_id');

            $('video').each(function() {
                $(this).get(0).pause();
            });


            $('div[id^="floor_"]').hide();
            $('.banner_inner__slider-inner').hide().slick('slickPlay');
            $('#floor_' + id).show();

            $('.banner_inner__slider-inner').fadeIn(100, function(){
                $('.banner_inner__slider-inner').slick('slickPause');
            });

            var timer = setTimeout(function(){
                var block = $('#floor_' + id);
                block.find('.owl-item.active video').each(function(){
                    $(this).get(0).play();
                });
            }, 1000);

            $('.header_menu .header_menu__item.__act').removeClass('__act');
            $(this).parent().addClass('__act');

            window.location.hash = '#' + id;

            return false;
        }

        return true;
    });

    $('.item_page__thumbs').delegate('.__3d, .__video', 'click', function(){
        CSite.showMessage(($(this).hasClass('__3d') ? 'Просмотр в 3D':'Просмотр видео'), $(this).find('.hide').html(), ($(this).hasClass('__3d') ? '__3d':'__video'));
    });

    $body.on('click', '#open_authorize_popup', function() {
        CSite.showMessage('Авторизация', tmpl('popup-authorize', {}), '__login');
        return false;
    });

    $body.on('submit', '#authorize_popup_form', function() {

        var form = this;
        $(this).find('.error_block').text('').hide();

        var validator = $(this).validate({
            rules: {
                'AUTH[EMAIL]': {
                    required: true,
                    email: true
                },
                'AUTH[PSWD]': {
                    required: true,
                    password: true,
                    minlength: 6
                }
            }
        });

        if(!validator.form()) {
            return false;
        }

        $.post('/ajax/authorize.php', $(form).serialize(), function(data){
            if(data.status == 'OK'){
                document.location.reload();
            } else {
                $(form).find('.error_block').text(data.message).show();
            }
        }, 'json').always(function() {
            CSite.unLock();
        });

        return false;
    });

    $('.item_gift__link').on('click', function() {
        var offset = $('#gifts-block').offset();
        $('html, body').stop().animate({
            'scroll-top': (offset.top - $('.header_wrapper').outerHeight() - 20)
        }, 500);
        return false;
    });

    //авторизация и регистрация из соц сети
    $body.on('click', 'a[data-site]', function(e) {
        e.preventDefault();
        CSite.openWindow(
            $(this).data('window-url'),
            $(this).data('window-width'),
            $(this).data('window-height')
        );
    });

    $.ajaxSetup({
        beforeSend: function() {
            $('.popup').addClass('__load');
        },
        complete: function() {
            $('.popup').removeClass('__load');
        }
    });

    $.validator.setDefaults({
        ignore: '.ignore',
        highlight: function(element, errorClass) {
            $(element).addClass(errorClass);

            if($(element).data('selectric')) {
                $(element).closest('.selectricWrapper').addClass(errorClass);
            }
        },
        unighlight: function(element, errorClass) {
            $(element).removeClass(errorClass);

            if($(element).data('selectric')) {
                $(element).closest('.selectricWrapper').removeClass(errorClass);
            }
        }
    });

    $.validator.addMethod('email', function(value, element) {
        return CSite.checkEmail(value);
    }, 'Пожалуйста, введите корректный email адрес.');

    $.validator.addMethod('password', function(value, element) {
        return true;
        //return /^[\d\w]*$/.test(value);
    }, 'Используйте только латинские буквы A-Z и цифры.');

    $.validator.addMethod('empty-phone', function(value, element) {
        return /^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/.test(value) || value.length == 0;
    }, 'Введите телефон в формате +7 (ХХХ) ХХХ-ХХ-ХХ');

    $.validator.addMethod('phone', function(value, element) {
        return /^\+7 \(\d{3}\) \d{3}\-\d{2}\-\d{2}$/.test(value);
    }, 'Введите телефон в формате +7 (ХХХ) ХХХ-ХХ-ХХ');

    $.datepicker.regional['ru'] = {
        closeText: 'Закрыть',
        prevText: '&#x3c;Пред',
        nextText: 'След&#x3e;',
        currentText: 'Сегодня',
        monthNames: ['Январь','Февраль','Март','Апрель','Май','Июнь',
            'Июль','Август','Сентябрь','Октябрь','Ноябрь','Декабрь'],
        monthNamesShort: ['Янв','Фев','Мар','Апр','Май','Июн',
            'Июл','Авг','Сен','Окт','Ноя','Дек'],
        dayNames: ['воскресенье','понедельник','вторник','среда','четверг','пятница','суббота'],
        dayNamesShort: ['вск','пнд','втр','срд','чтв','птн','сбт'],
        dayNamesMin: ['Вс','Пн','Вт','Ср','Чт','Пт','Сб'],
        weekHeader: 'Не',
        dateFormat: 'dd.mm.yy',
        firstDay: 1,
        isRTL: false,
        showMonthAfterYear: false,
        yearSuffix: ''
    };

    $.datepicker.setDefaults($.datepicker.regional['ru']);
});





    