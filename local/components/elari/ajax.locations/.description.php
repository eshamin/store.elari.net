<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
	"NAME" => GetMessage("SAL_DEFAULT_TEMPLATE_NAME"),
	"DESCRIPTION" => GetMessage("SAL_DEFAULT_TEMPLATE_DESCRIPTION"),
	"ICON" => "/images/sale_ajax_locations.gif",
    "PATH" => array(
        "ID" => "adv",
        "NAME" => "Компоненты ADV/web-engineering co.",
        "SORT" => 1000
    ),
);