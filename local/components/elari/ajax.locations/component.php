<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $this CBitrixComponent
 * @var $arParams array
 * @var $arResult array
 * @var $APPLICATION CMain
 */
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if (!CModule::IncludeModule('sale')) {
    ShowError(GetMessage('SALE_MODULE_NOT_INSTALL'));
    return;
}

$arParams['LOCATION_VALUE'] = intval($arParams['LOCATION_VALUE']) > 0 ? intval($arParams['LOCATION_VALUE']):\Adv\Utils::getDefaultCityId();
$arParams['ZIPCODE'] = IntVal($arParams['ZIPCODE']);
$arParams['ADMIN_SECTION'] = (defined('ADMIN_SECTION') && ADMIN_SECTION === true) ? 'Y' : 'N';

$arParams['INPUT_ID'] = $arParams['INPUT_ID'] ? $arParams['INPUT_ID']:str_replace(array('[', ']') , '_', $arParams['INPUT_NAME']);

$arParams['MANUAL_INPUT_ID'] = $arParams['MANUAL_INPUT_ID'] ? $arParams['MANUAL_INPUT_ID']:$arParams['INPUT_ID'] . '_val';
$arParams['MANUAL_INPUT_NAME'] = $arParams['MANUAL_INPUT_NAME'] ? $arParams['MANUAL_INPUT_NAME']:$arParams['INPUT_ID'] . '_val';

$arResult['MANUAL_INPUT'] = $arParams['MANUAL_INPUT'] == 'Y';
$arResult['USE_REGION_SELECT'] = $arParams['REGION_SELECT_FLAG'] == 'Y';

if ($arParams['LOCATION_VALUE'] > 0) {
    if ($arResult['LOCATION'] = CSaleLocation::GetByID($arParams['LOCATION_VALUE'])) {
        $arResult['LOCATION_VALUE'] = $arResult['LOCATION']['ID'];

        $location = array();

        if($arResult['LOCATION']['CITY_NAME']) {
            $location[] = $arResult['LOCATION']['CITY_NAME'];
        }

        if($arResult['LOCATION']['REGION_NAME']) {
            $location[] = $arResult['LOCATION']['REGION_NAME'];
        }

        if($arResult['LOCATION']['COUNTRY_NAME_LANG']) {
            $location[] = $arResult['LOCATION']['COUNTRY_NAME_LANG'];
        }

        $arResult['LOCATION_STRING'] = implode(', ', $location);
    }
}

if($arResult['MANUAL_INPUT']) {
    $arResult['REGIONS'] = array();

    $db = CSaleLocation::GetList(
        array(
            'REGION_NAME_LANG' => 'ASC',
            'SORT' => 'ASC',
        ),
        array(
            'REGION_LID' => LANGUAGE_ID,
            'COUNTRY_LID' => LANGUAGE_ID,
            'CITY_ID' => false
        ),
        false,
        false,
        array('ID', 'CITY_ID', 'CITY_NAME', 'COUNTRY_NAME_LANG', 'REGION_NAME_LANG')
    );
    while ($location = $db->GetNext()) {
        $arResult['REGIONS'][] = $location;
    }
}

if(strlen($arParams['LOCATION_STRING']) || $arResult['USE_REGION_SELECT']) {
    $arResult['LOCATION_STRING'] = $arParams['LOCATION_STRING'];
}

$this->IncludeComponentTemplate();