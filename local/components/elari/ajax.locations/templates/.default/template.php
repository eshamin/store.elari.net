<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $arResult array
 * @var $arParams array
 * @var $this CBitrixComponentTemplate
 * @var $APPLICATION CMain
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<script type="text/javascript">
    $(function() {
        var manual = <?=$arResult['MANUAL_INPUT'] ? 1:0?>;

        var $region = $('#region-id');
        var $location = $('#<?=$arParams['INPUT_ID']?>');
        var $input = $('#<?=$arParams['MANUAL_INPUT_ID']?>');
        var $changeMode = $('#delivery-change-mode');

        function changeModeCallback() {
            $(this).hide();
            $(this).prev().show();

            $region.data('selectric').destroy();
            $region.hide();

            $('#region-flag').remove();
            $('#region-id-error').remove();

            $input.val('').autocomplete('enable');

            $(this).off('click');

            return false;
        }

        function insertRegionSelectFlag() {
            if($('#region-flag').length <= 0) {
                $('<input type="text" name="REGION_SELECT_FLAG" value="Y" id="region-flag" />').insertBefore($region.closest('.selectricWrapper'));
            }
        }

        $.ui.autocomplete.prototype._renderItem = function(ul, item) {
            var t = item.label.replace((new RegExp('^(' + this.term + ')', 'i')), '<b>$1</b>');
            return $('<li></li>').data('item.autocomplete', item).append('<a>' + t + '</a>').appendTo(ul);
        };

        $.ui.autocomplete.prototype._resizeMenu = function() {
            this.menu.element.outerWidth(this.element.outerWidth());
        };

        $.ui.autocomplete.prototype._renderMenu = function(ul, items) {
            var _this = this;
            $.each(items, function(index, item) {
                _this._renderItemData(ul, item);
            });
            $(ul).css({'position': 'absolute'});
        };

        var originaCloseMethod = $.ui.autocomplete.prototype.close;
        $.ui.autocomplete.prototype.close = function(event, ui) {
            if($input.data('suggested') != 1) {
                originaCloseMethod.apply(this, arguments);
            }
        };

        $region.on('change', function() {
            $input.val('');
            $location.remove();

            insertRegionSelectFlag();

            <?=$arParams['ONCITYCHANGE']?>;
        });

        $input.autocomplete({
            source: '<?=$this->__component->getPath() . '/search.php?manual=' . ($arResult['MANUAL_INPUT'] ? '1':'0') ?>',
            minLength: 2,
            appendTo: $input.parent(),
            search: function(event, ui) {
                $input.data('suggested', 1);
            },
            select: function(event, ui) {
                $input.data('suggested', 0);

                if(parseInt(ui.item.id, 10) > 0) {
                    $input.val(ui.item.value);
                    $location.val(ui.item.id).trigger('change');
                } else {
                    $changeMode.show();
                    $changeMode.prev().hide();

                    $changeMode.on('click', changeModeCallback);

                    $region.show().focus().selectric({
                        maxHeight: 200
                    });

                    insertRegionSelectFlag();

                    $input.val('').autocomplete('disable');

                    return false;
                }
            }
        }).on('blur', function() {
            if($(this).autocomplete( "instance" ).options.disabled === false) {
                if ($(this).data('suggested') == 1) {
                    $input.addClass('error');
                    return false;
                }

                $(this).data('suggested', 0).removeClass('error');
            }
        }).on('change', function() {
            if($(this).autocomplete( "instance" ).options.disabled !== false) {
                <?=$arParams['ONCITYCHANGE']?>;
            }
        });

        <? if($arResult['USE_REGION_SELECT']) { ?>
            $changeMode.show();
            $changeMode.prev().hide();
            $input.autocomplete('disable');

            $changeMode.on('click', changeModeCallback);
        <? } ?>
    });
</script>
<?/* if($arResult['REGIONS']) {
    if($arResult['USE_REGION_SELECT']) {
        ?><input type="hidden" name="REGION_SELECT_FLAG" value="Y" id="region-flag" /><?
    } ?>
    <select name="<?=$arParams['INPUT_NAME']?>" id="region-id" class="reg-selector reg-form_select manual" <?=$arResult['USE_REGION_SELECT'] ? ' style="display:block;"':''?>>
        <option value="0">Выбрать регион</option>
        <? foreach($arResult['REGIONS'] as $region) { ?>
            <option value="<?=$region['ID']?>"<?=$arResult['LOCATION_VALUE'] == $region['ID'] ? ' selected':''?>><?=$region['REGION_NAME_LANG']?></option>
        <? } ?>
    </select>
<? } */?>
<div class="form-group">
    <input type="hidden" name="MANUAL_LOCATION_NAME" value="<?=$arParams['MANUAL_INPUT_NAME']?>" />
    <input
    	size="<?=$arParams['SIZE1']?>"
    	name="<?=$arParams['MANUAL_INPUT_NAME']?>"
    	id="<?=$arParams['MANUAL_INPUT_ID']?>"
    	value="<?=$arResult['LOCATION_STRING']?>"
    	class="<?=$arParams['INPUT_CLASS'] ? $arParams['INPUT_CLASS']:'reg-form_input'?>" type="text"
    	autocomplete="off"
    	<?=($arResult['SINGLE_CITY'] == 'Y' ? ' disabled':'');?> />
    <input type="hidden" name="<?=$arParams['INPUT_NAME']?>" id="<?=$arParams['INPUT_ID']?>" value="<?=$arResult['LOCATION_VALUE']?>" onchange="<?=$arParams['ONCITYCHANGE']?>" />
</div>