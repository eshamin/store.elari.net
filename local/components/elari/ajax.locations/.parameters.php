<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (!CModule::IncludeModule("sale")) return;

$arComponentParameters = Array(
	"PARAMETERS" => Array(
		"INPUT_NAME" => array(
			"NAME" => GetMessage("SALE_SAL_PARAM_CITY_INPUT_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "LOCATION",
			"PARENT" => "BASE",
		),
		"COUNTRY" => array(
			"NAME" => GetMessage("SALE_SAL_PARAM_COUNTRY"),
			"TYPE" => "LIST",
			"VALUES" => $arCountries,
			"ADDITIONAL_VALUES" => "N",
			"MULTIPLE" => "N",
			"PARENT" => "BASE",
		),
		"NAME" => array(
			"PARENT" => "BASE",
			"NAME" => GetMessage("CP_BSSI_NAME"),
			"TYPE" => "STRING",
			"DEFAULT" => "q",
		),
	)
);