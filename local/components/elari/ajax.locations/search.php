<?
/**
 * Created by ADV/web-engineering co.
 *
 * @var $APPLICATION CMain
 */
define('STOP_STATISTICS', true);
define('PUBLIC_AJAX_MODE', true);

require_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

function arrayChangeKeyCaseRecursive(array $input, $case = CASE_LOWER)
{
    return array_map(function($item) use ($case) {

        if(is_array($item)) {
            $item = arrayChangeKeyCaseRecursive($item, $case);
        }

        return $item;
    }, array_change_key_case($input, $case));
}

$result = array();

if (\Bitrix\Main\Loader::includeModule('sale')) {
    if (!empty($_REQUEST['term']) && is_string($_REQUEST['term'])) {
        $search = $APPLICATION->UnJSEscape($_REQUEST['term']);

        $search = strpos($search, ',') !== false ? substr($search, 0, strpos($search, ',') - 1):$search;

        $filter = array(
            '~CITY_NAME' => $search . '%',
            'REGION_LID' => LANGUAGE_ID,
            'COUNTRY_LID' => LANGUAGE_ID,
            'CITY_LID' => LANGUAGE_ID,
        );
        $db = CSaleLocation::GetList(
            array(
                'CITY_NAME_LANG' => 'ASC',
                'COUNTRY_NAME_LANG' => 'ASC',
                'SORT' => 'ASC',
            ),
            $filter,
            false,
            array('nTopCount' => 10),
            array('ID', 'CITY_ID', 'CITY_NAME', 'COUNTRY_NAME_LANG', 'REGION_NAME_LANG')
        );
        while ($location = $db->GetNext()) {
            $result[] = array(
                'ID' => $location['ID'],
                'VALUE' => $location['CITY_NAME'] . ', ' . $location['REGION_NAME_LANG'] . ', ' . $location['COUNTRY_NAME_LANG'],
                'LABEL' => $location['CITY_NAME'] . ', ' . $location['REGION_NAME_LANG'] . ', ' . $location['COUNTRY_NAME_LANG'],
            );
        }


        $filter = array(
            '~REGION_NAME' =>$search . '%',
            'LID' => LANGUAGE_ID,
            'CITY_ID' => false
        );
        $db = CSaleLocation::GetList(
            array(
                'CITY_NAME_LANG' => 'ASC',
                'COUNTRY_NAME_LANG' => 'ASC',
                'SORT' => 'ASC',
            ),
            $filter,
            false,
            array('nTopCount' => 10),
            array('ID', 'CITY_ID', 'CITY_NAME', 'COUNTRY_NAME_LANG', 'REGION_NAME_LANG')
        );
        while ($location = $db->GetNext()) {
            $result[] = array(
                'ID' => $location['ID'],
                'VALUE' => $location['REGION_NAME_LANG'] . ', ' . $location['COUNTRY_NAME_LANG'],
                'LABEL' => $location['REGION_NAME_LANG'] . ', ' . $location['COUNTRY_NAME_LANG'],
            );
        }


        $filter = array(
            '~COUNTRY_NAME' => $search . '%',
            'LID' => LANGUAGE_ID,
            'CITY_ID' => false,
            'REGION_ID' => false
        );
        $db = CSaleLocation::GetList(
            array(
                'COUNTRY_NAME_LANG' => 'ASC',
                'SORT' => 'ASC',
            ),
            $filter,
            false,
            array('nTopCount' => 10),
            array('ID', 'COUNTRY_NAME_LANG')
        );
        while ($location = $db->GetNext()) {
            $result[] = array(
                'ID' => $location['ID'],
                'VALUE' => $location['COUNTRY_NAME_LANG'],
                'LABEL' => $location['COUNTRY_NAME_LANG'],
            );
        }
    }
}

/*if($_REQUEST['manual'] == 1) {
    $result[] = array(
        'ID' => 0,
        'VALUE' => 'Указать другой населенный пункт',
        'LABEL' => 'Указать другой населенный пункт',
    );
}*/

echo json_encode(arrayChangeKeyCaseRecursive($result, CASE_LOWER));