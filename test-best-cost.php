<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
use \Bitrix\Main\Web\HttpClient;

$apiKey = "748a22d3bcfc30398bffd651d9403ddf";
$url = "https://api.marschroute.ru/";//<api_key>/order[?crossdock=1][&check=1]	
$httpClientOptions = array(
	"waitResponse" => true,
	"socketTimeout" => 30,
	"streamTimeout" => 60,
	"version" => HttpClient::HTTP_1_1
);
// $data = array(
	// "order" => array(
		// "delivery_sum" => 100,//Стоимость доставки
		// "payment_type" => 1,//1 - наличные, 2 - предоплаченный заказ
		// "send_date" => "20.08.2018",//Планируемая дата передачи ЗП 
		// "delivery_interval" => 0,//Интервал доставки
		// "weight" => 100,//Вес в граммах заказа
		// "city_id" => 77000000000,//КЛАДР или ФИАС идентификатор города
		// "place_id" => -1,//Идентификатор типа доставки (см. лист расчет стоимости доставки) Если передать -1, то заказ создастся с доставкой силами ИМ 
		// "consignee" => "СДЭК", //Наименование грузополучателя для ярлыка заказа с доставкой силами ИМ (place_id = -1). В поле можно передать код грузополучателя из справочника,  при этом данное поле и consignee_doc получат значения из справочника. Справочник ведется в СМ по согласованию с партнером
		// "consignee_doc" => "",//Наименование грузополучателя для акта приема-передачи, Необязательное ноле. Если не пустое, то выводится в акте заказа вместо поля consignee
		// "barcode" => "A123456789",//ШК для печати на этикетке заказа в формате code39 для доставки силами ИМ (place_id = -1) 
		// "index" => "", //Почтовый индекс
		// "street" => "Кантемировская ул.", //Улица адреса доставки
		// "building_1" => "2",//Дом
		// "building_2" => "2",//Строение/корпус
		// "room" => "4013",//Квартира/офис
	// ),
	// "customer" => array(
		// "id" => "1",//id покупателя
		// "firstname" => "firstname",//Имя
		// "middlename" => "middlename",//Отчество
		// "lastname" => "lastname",//Фамилия
		// "phone" => "79999999999"
	// ),
	// "items" => array(
		// 0 => array(
			// "item_id" => 1, //Код товара ИМ
			// "name" => "Название товара", //Название товара
			// "nds" => 0, //НДС
			// "price" => 100, //Цена товара
			// "quantity" => 1 //Количество
		// )
	// )
// );

// Создание http-клиента и отправка запроса
$httpClient = new HttpClient($httpClientOptions);
// $httpClient->query(HttpClient::HTTP_PUT, $url . $apiKey . "/order?crossdock=1&check=1", json_encode($data));
// $httpClient->query(HttpClient::HTTP_GET, $url . $apiKey . "/delivery_city?kladr=77000000000&weight=1000&payment_type=1&parcel_size=[10,10,10]&order_sum=1000&best_cost=0");
$httpClient->query(HttpClient::HTTP_GET, $url . $apiKey . "/delivery_city?kladr=77000000000&weight=1000&payment_type=1&parcel_size=[10,10,10]&order_sum=1000&best_cost=1");
// $httpClient->query(HttpClient::HTTP_GET, $url . $apiKey . "/orders");
// Результат ответа
$result = json_decode( $httpClient->getResult(), true );

echo "<pre>", print_R($result), "</pre>";