<?php
global $APPLICATION;

if(!defined('ERROR_404')) {
    include_once($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/urlrewrite.php');

    CHTTP::SetStatus("404 Not Found");
    @define("ERROR_404", "Y");

    require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	CBXShortUri::CheckUri();
    $APPLICATION->SetTitle("Страница не найдена");
} else {
    require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_after.php');
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>