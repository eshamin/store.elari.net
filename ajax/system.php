<?php
/**
 * Created by ADV/web-engineering co.
 * User: burtsev
 * Date: 15.12.14
 * Time: 19:28
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

try {

    if($_SERVER['REQUEST_METHOD'] != 'POST' && !check_bitrix_sessid()) {
        throw new Exception('Bad request!');
    }

    switch ($_REQUEST['action']) {

        case 'sort':
            \Adv\Helper::setSort($_REQUEST['value']);
            break;

        case 'template':
            \Adv\Helper::setCatalogTemplate($_REQUEST['value']);
            break;

        case 'onpage':
            \Adv\Helper::setOnPageCnt($_REQUEST['value']);
            break;

        default:
            throw new Exception('Undefined action!');
            break;

    }

    echo json_encode(array('status' => 'OK'));

} catch (Exception $e) {
    echo json_encode(array('status' => 'ERR', 'message' => $e->getMessage()));
}