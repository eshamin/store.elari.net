<?php
/**
 * Created by ADV/web-engineering co.
 *
 * @var $APPLICATION CMain
 */

use Adv\BasketHelper;

require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

try {

    global $USER;

    \Bitrix\Main\Loader::includeModule('sale');
    \Bitrix\Main\Loader::includeModule('iblock');
    \Bitrix\Main\Loader::includeModule('catalog');

    if($_SERVER['REQUEST_METHOD'] != 'POST' && !check_bitrix_sessid()) {
        throw new Exception('Bad request!');
    }

    $result = array();

    switch ($_REQUEST['action']) {

        case 'add':
            $basketId = BasketHelper::add2Basket($_REQUEST['params']['product_id'], $_REQUEST['params']['quantity'], $_REQUEST['params']['gift']);

            if($gift = BasketHelper::getBasketGift($basketId)) {
                $result['GIFT'] = $gift;
            }

            $result['PRODUCT'] = BasketHelper::getProductInfo($_REQUEST['params']['product_id']);
            $result['TOTAL'] = BasketHelper::getTotalBasketState();
            break;

        case 'delete':
            BasketHelper::delete($_REQUEST['params']['id']);
            $result['TOTAL'] = BasketHelper::getTotalBasketState();
            break;

        case 'get.product':
            $result['PRODUCT'] = BasketHelper::getProductInfo($_REQUEST['params']['product_id']);
            break;

        case 'order':
            $result = BasketHelper::makeOrder($_REQUEST[ 'params' ][ 'product_id' ], $_REQUEST[ 'params' ][ 'name' ], $_REQUEST[ 'params' ][ 'phone' ], $_REQUEST[ 'params' ][ 'email' ], true, $_REQUEST[ 'params' ][ 'captcha_sid' ]);
            //$result = BasketHelper::makeOrder($_REQUEST[ 'params' ][ 'product_id' ], $_REQUEST[ 'params' ][ 'name' ], $_REQUEST[ 'params' ][ 'phone' ], $_REQUEST[ 'params' ][ 'email' ]);
            break;

        case 'cancel.order':
            if(!\CSaleOrder::CancelOrder($_REQUEST['params']['order_id'], 'Y', $_REQUEST['params']['reason'])) {
                throw new Exception($APPLICATION->GetException()->GetString());
            }
            break;

        case 'pre.order':
            $preorder = \Adv\Entity\PreordersTable::add(array(
                'UF_USER_NAME' => $_REQUEST['params']['name'],
                'UF_USER_EMAIL' => $_REQUEST['params']['email'],
                'UF_USER_ID' => $USER->GetID(),
                'UF_PRODUCT_ID' => $_REQUEST['params']['product_id'],
                'UF_USER_PHONE' => $_REQUEST['params']['phone'],
                'UF_USER_CITY' => $_REQUEST['params']['city'],
            ));

            //Create lead in Bitrix24
            //$bx24_log_file = $_SERVER["DOCUMENT_ROOT"]."/upload/bx24_lead_log.txt";
            $bx24_url = "https://small.bitrix24.ru/crm/configs/import/lead.php";
            $bx24_login = "small.lead@bitrix24.ru";
            $bx24_pwd = "160287";
            $bx24_product = CIBlockElement::GetByID(intval($_REQUEST['params']['product_id']))->Fetch()['NAME'];

            $bx24_data = array(
                "LOGIN" => $bx24_login,
                "PASSWORD" => $bx24_pwd,
                "TITLE" => $_REQUEST['params']['name'],
                "SOURCE_ID" => "2",
                "SOURCE_DESCRIPTION" => $bx24_product,
                "EMAIL_HOME" => $_REQUEST['params']['email'],
                "NAME" => $_REQUEST['params']['name'],
                "ADDRESS" => $_REQUEST['params']['city'],
                "PHONE_MOBILE" => $_REQUEST['params']['phone'],
                "UF_CRM_1433454816" => $bx24_product,
                "OPPORTUNITY" => intval(CPrice::GetBasePrice(intval($_REQUEST['params']['product_id']))['PRICE'])
            );

            $bx24_options = array(
                'http' => array(
                    'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method'  => 'POST',
                    'content' => http_build_query($bx24_data),
                ),
            );
            $bx24_context  = stream_context_create($bx24_options);
            $bx24_result = file_get_contents($bx24_url, false, $bx24_context);

            //logging to a file
            //file_put_contents($bx24_log_file, date('d.m.Y H:i:s')."   ".$_REQUEST['params']['email']."   ".$bx24_result."\n", FILE_APPEND | LOCK_EX);

            //Bitrix24 lead creation completed

            if(!$preorder->isSuccess()) {
                throw new Exception(implode(' / ', $preorder->getErrorMessages()));
            }
            break;

        case 'get.gifts':
            $result['GIFTS'] = array_map(function($item) {
                $item['IMAGE'] = \Adv\Utils::showImage($item['IMAGE'] ? $item['IMAGE']:'/static/img/no_photo.png', 80, 130, false, 'single');
                return $item;
            }, BasketHelper::getProductGifts($_REQUEST['params']['product_id']));
            break;

        case 'set.gift':
            if(!BasketHelper::setBasketGift($_REQUEST['params']['basket_id'], $_REQUEST['params']['gift'])) {
                throw new Exception('Ошибка добавления подарка в корзину, попробуйте еще раз!');
            }
            break;

        case 'get.accessories':
            $result['ACCESSORIES'] = array_map(function($item) {
                $item['IMAGE'] = \Adv\Utils::showImage($item['IMAGE'] ? $item['IMAGE']:'/static/img/no_photo.png', 80, 130, false, 'single');
                return $item;
            }, BasketHelper::getProductAccessories($_REQUEST['params']['product_id']));
            break;

        case 'add.accessories':
            $properties = array(
                array(
                    'CODE' => 'ACCESSORY_TO_BASKET_ID',
                    'NAME' => 'Аксессуар к корзине №',
                    'VALUE' => intval($_REQUEST['params']['parent'])
                )
            );

            foreach($_REQUEST['params']['accessories'] as $id) {
                BasketHelper::add2Basket($id, 1, false, $properties);
            }
            break;

        case 'buy.set':
            $result = BasketHelper::buySet($_REQUEST['params']['product_id'], $_REQUEST['params']['items']);
            $result['TOTAL'] = BasketHelper::getTotalBasketState();
            break;

        default:
            throw new Exception('Undefined action!');
            break;
    }

    echo json_encode(array('status' => 'OK', 'result' => arrayChangeKeyCaseRecursive($result, CASE_LOWER)));

} catch (\Exception $e) {
    echo json_encode(array('status' => 'ERR', 'message' => $e->getMessage()));
}

function arrayChangeKeyCaseRecursive(array $input, $case = CASE_LOWER)
{
    return array_map(function($item) use ($case) {

        if(is_array($item)) {
            $item = arrayChangeKeyCaseRecursive($item, $case);
        }

        return $item;
    }, array_change_key_case($input, $case));
}