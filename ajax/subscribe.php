<?php
/**
 * Created by ADV/web-engineering co.
 */

require_once ($_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php');

\Bitrix\Main\Loader::includeModule('subscribe');

try {

    global $USER;

    $email = $_REQUEST['email'];

    if($_SERVER['REQUEST_METHOD'] != 'POST' || !check_bitrix_sessid()) {
        throw new Exception('Bad request!', 1);
    }

    if(!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        throw new Exception('Вы указали не корректный email адрес', 2);
    }

    $subscribe = new CSubscription();

    $fields = array(
        'USER_ID' => ($USER->IsAuthorized()? $USER->GetID():false),
        'FORMAT' => 'html',
        'EMAIL' => $email,
        'ACTIVE' => 'Y',
        'RUB_ID' => array(40, 41),
        'CONFIRMED' => 'Y',
        'SEND_CONFIRM' => 'N'
    );

    if($subscription = CSubscription::GetByEmail($email)->Fetch()) {
        //сохраняем старые подписки
        $arCurRubrics = CSubscription::GetRubricArray($subscription['ID']);
        $fields['RUB_ID'] = array_merge($fields['RUB_ID'], (array)$arCurRubrics);

        if($id = $subscribe->Update($subscription['ID'], $fields)) {
            CSubscription::Authorize($subscription['ID']);
        } else {
            throw new Exception($subscribe->LAST_ERROR, 3);
        }
    } else {
        if($id = $subscribe->Add($fields)) {
            CSubscription::Authorize($id);
        } else {
            throw new Exception($subscribe->LAST_ERROR, 3);
        }
    }

    echo json_encode(array(
        'status' => 'OK',
        'message' => 'Поздравляем! Ваша подписка оформлена.'
    ));

} catch (Exception $e) {
    echo json_encode(array('ststus' => 'ERR', 'message' => $e->getMessage()));
}