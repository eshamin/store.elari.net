<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<title>KidPhone 3G</title>
		<meta name="yandex-verification" content="3408b23731f530a3" />
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<style type="text/css">
			* {
				-ms-text-size-adjust:100%;
				-webkit-text-size-adjust:none;
				-webkit-text-resize:100%;
				text-resize:100%;
			}
			a{
				outline:none;
				color:#40aceb;
				text-decoration:underline;
			}
			a:hover{text-decoration:none !important;}
			.nav a:hover{text-decoration:underline !important;}
			.title a:hover{text-decoration:underline !important;}
			.title-2 a:hover{text-decoration:underline !important;}
			.btn:hover{opacity:0.8;}
			.btn a:hover{text-decoration:none !important;}
			.btn{
				-webkit-transition:all 0.3s ease;
				-moz-transition:all 0.3s ease;
				-ms-transition:all 0.3s ease;
				transition:all 0.3s ease;
			}
			table td {border-collapse: collapse !important;}
			.ExternalClass, .ExternalClass a, .ExternalClass span, .ExternalClass b, .ExternalClass br, .ExternalClass p, .ExternalClass div{line-height:inherit;}
			@media only screen and (max-width:500px) {
				table[class="flexible"]{width:100% !important;}
				table[class="center"]{
					float:none !important;
					margin:0 auto !important;
				}
				*[class="hide"]{
					display:none !important;
					width:0 !important;
					height:0 !important;
					padding:0 !important;
					font-size:0 !important;
					line-height:0 !important;
				}
				td[class="img-flex"] img{
					width:100% !important;
					height:auto !important;
				}				
				td[class="img-icon"] img{
					width:60px !important;
					height:auto !important;
				}
				td[class="aligncenter"]{text-align:center !important;}
				th[class="flex"]{
					display:block !important;
					width:100% !important;
				}
				td[class="wrapper"]{padding:0 !important;}
				td[class="holder"]{padding:30px 15px 20px !important;}
				td[class="nav"]{
					padding:20px 0 0 !important;
					text-align:center !important;
				}
				td[class="h-auto"]{height:auto !important;}
				td[class="banner"]{padding:0 20px 0 20px !important;}
				td[class="i-120"] img{
					width:120px !important;
					height:auto !important;
				}
				td[class="footer"]{padding:5px 20px 20px !important;}
				td[class="footer"] td[class="aligncenter"]{
					line-height:25px !important;
					padding:20px 0 0 !important;
				}
				tr[class="table-holder"]{
					display:table !important;
					width:100% !important;
				}
				th[class="thead"]{display:table-header-group !important; width:100% !important;}
				th[class="tfoot"]{display:table-footer-group !important; width:100% !important;}
			}
		</style>
	</head>
	<body style="margin:0; padding:0;" bgcolor="#eaeced">
	
	
<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym")

   ym(51439120, "init", {
        id:51439120,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/51439120" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->	
	
	
		<table style="min-width:320px;" width="100%" cellspacing="0" cellpadding="0" bgcolor="#eaeced">
			<!-- fix for gmail -->
			<tr>
				<td class="hide">
					<table width="100%" cellpadding="0" cellspacing="0" style="width:600px !important;">
						<tr>
							<td style="min-width:600px; font-size:0; line-height:0;">&nbsp;</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td class="wrapper" style="padding:0 10px;">


					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<a target="_blank" href="https://store.elari.net/catalog/tovary-dlya-detey/elari-kidphone-3g-red/?utm_source=elari&utm_medium=land&utm_campaign=kf3gred">
												<img src="http://elari.store/lp/kp3g/1.png" style="vertical-align:top;" width="100%" alt="" />											</a>
										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>
					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<img src="http://elari.store/lp/kp3g/2.png" style="vertical-align:top;" width="100%" alt="" />										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>		
					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<img src="http://elari.store/lp/kp3g/3.png" style="vertical-align:top;" width="100%" alt="" />										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>

					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<img src="http://elari.store/lp/kp3g/4.png" style="vertical-align:top;" width="100%" alt="" />										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>

					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<img src="http://elari.store/lp/kp3g/5.png" style="vertical-align:top;" width="100%" alt="" />										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>

					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<img src="http://elari.store/lp/kp3g/6.png" style="vertical-align:top;" width="100%" alt="" />										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>

					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>										<td class="img-flex">
											<img src="http://elari.store/lp/kp3g/7.png" style="vertical-align:top;" width="100%" alt="" />										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>


					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td class="img-flex">
											<a target="_blank" href="https://www.youtube.com/watch?v=01cRFwlmkCE">
												<img src="http://elari.store/lp/kp3g/8.png" style="vertical-align:top;" width="100%" alt="" />
											</a>
										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>
					
					<!-- module 2 -->
					<table data-module="module-2" data-thumb="thumbnails/02.png" width="100%" cellpadding="0" cellspacing="0">
						<tr>
							<td data-bgcolor="bg-module" bgcolor="#eaeced">
								<table class="flexible" width="100%" align="center" style="margin:0 auto;" cellpadding="0" cellspacing="0">
									<tr>
										<td class="img-flex">
											<a target="_blank" href="https://store.elari.net/catalog/tovary-dlya-detey/elari-kidphone-3g-red/?utm_source=elari&utm_medium=land&utm_campaign=kf3gred">
												<img src="http://elari.store/lp/kp3g/9.png" style="vertical-align:top;" width="100%" alt="" />
											</a>
										</td>	
									</tr>
									<tr><td height="0"></td></tr>
								</table>
							</td>
						</tr>
					</table>

	</body>
</html>