$(document).ready(function() {

     $(window).load(function() {
             $(function() {
                 $(".choice-color").change(function() {
                     var selectedProduct = $(this).children("option").filter(":selected").text();
                     $(".hideable").css('display', 'none');
                     if (selectedProduct == "Черный") {
                         $("#product_link1").css('display', 'inline-block');
                     } else if (selectedProduct == "Белый") {
                         $("#product_link2").css('display', 'inline-block');
                     }
                 })

             });
               $("#choice-form-one").trigger('reset');
         });

          //tablet
                        enquire.register("screen and (min-width: 769px)", {
                            match: function() {
                                $(window).scroll(function(e){
                                      	parallaxScroll();
                                    	});

                                    	function parallaxScroll(){
                                    		var scrolled = $(window).scrollTop();
                                    		$('#parallax-bg-1').css('top',(0-(scrolled*.5))+'px');
                                    	}

                            },
                            unmatch: function() {
                            }
                        });
                        //tablet

         //tablet
               enquire.register("screen and (max-width: 768px)", {
                   match: function() {
                       $(window).scroll(function(e){
                             	parallaxScroll();
                           	});

                           	function parallaxScroll(){
                           		var scrolled = $(window).scrollTop();
                           		$('#parallax-bg-1').css('top',(0-(scrolled*.12))+'px');
                           	}

                   },
                   unmatch: function() {
                   }
               });
               //tablet
});