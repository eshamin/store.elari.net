$(document).ready(function() {

     $(window).load(function() {
             $(function() {
                 $(".choice-color").change(function() {
                     var selectedProduct = $(this).children("option").filter(":selected").text();
                     $(".hideable").css('display', 'none');
                     if (selectedProduct == "Черный") {
                         $("#product_link1").css('display', 'inline-block');
                     } else if (selectedProduct == "Белый") {
                         $("#product_link2").css('display', 'inline-block');
                     }
                 })

             });
               $("#choice-form-one").trigger('reset');
         });


         var $page = $('html, body');
         $('a[href*="#"]').click(function() {
             $page.animate({
                 scrollTop: $($.attr(this, 'href')).offset().top
             }, 800);
             return false;
         });

         //tablet
                                 enquire.register("screen and (min-width: 1025px)", {
                                     match: function() {
                                         $(window).scroll(function(e){
                                               	parallaxScroll();
                                             	});

                                             	function parallaxScroll(){
                                             		var scrolled = $(window).scrollTop();
                                             		$('#parallax-bg-1').css('top',(0-(scrolled*.32))+'px');
                                             	}

                                     },
                                     unmatch: function() {
                                     }
                                 });
                                 //tablet

          //tablet
                        enquire.register("screen and (min-width: 769px) and (max-width: 1024px)", {
                            match: function() {
                                $(window).scroll(function(e){
                                      	parallaxScroll();
                                    	});

                                    	function parallaxScroll(){
                                    		var scrolled = $(window).scrollTop();
                                    		$('#parallax-bg-1').css('top',(0-(scrolled*.07))+'px');
                                    	}

                            },
                            unmatch: function() {
                            }
                        });
                        //tablet

         //tablet
               enquire.register("screen and (max-width: 768px)", {
                   match: function() {
                       $(window).scroll(function(e){
                             	parallaxScroll();
                           	});

                           	function parallaxScroll(){
                           		var scrolled = $(window).scrollTop();
                           		$('#parallax-bg-1').css('top',(0-(scrolled*.1))+'px');
                           	}

                   },
                   unmatch: function() {
                   }
               });
               //tablet
});