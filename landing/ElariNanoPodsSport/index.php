<!doctype html>
<html lang="ru">

<head>
    <meta charset="utf-8">
    <title>Elari</title>
    <link rel="stylesheet" type="text/css" href="assets/fonts/fonts.css">
    <link rel="stylesheet" type="text/css" href="assets/css/style.css">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-74590709-4"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-74590709-4');
</script>
</head>


<body>

<div class="page-container">
    <div class="content-container">
        <!--header-->

<!--header end-->

        <div id="parallax-bg-1">
            <img src="assets/img/background/body-bg.png" alt="">
        </div>
        <header>
                <div class="header-container">
                    <div class="header-container__logo">
                        <a href="#" title=""><img src="assets/img/logos/logo.svg" alt="" width="500"></a>
                    </div>
                    <div class="header-container__buy">
                         <div class="buy">
                             <div class="buy__price">
                                 6990<span>руб.</span>
                             </div>
                             <div class="buy__button">
                                 <a class="button" href="#buy" title="">КУПИТЬ</a>
                             </div>
                         </div>
                    </div>
                </div>
        </header>


        <div class="centering-container">
            <div class="centering-container__row">
                <div class="container container_one">
                    <div class="container__description">
                        Водонепроницаемые, беспроводные
                        наушники c Нi-Fi стерео для музыки
                        и голоса с магнитным зарядным
                        кейсом и Bluetooth 5.0
                    </div>
                </div>
            </div>
        </div>
        <div class="centering-container">
            <div class="centering-container__row">
                <div class="container container_two">
                    <div class="container__description">
                        Превосходный стереозвук –
                        глубокие басы, насыщенные средние
                        и кристально чистые высокие частоты.
                        Отличные шумоизоляция, эргономика
                        и время автономной работы
                    </div>
                </div>
            </div>
        </div>

        <div class="centering-container">
            <div class="centering-container__row">
                <div class="container container_three">
                    <div class="container__description">
                        Защита от влаги
                        по стандарту IP67 –
                        в наушниках можно плавать
                        и активно заниматься спортом
                    </div>
                </div>
            </div>
        </div>

        <div class="centering-container">
            <div class="centering-container__row">
                <div class="container container_four">
                    <div class="container__description">
                        Bluetooth 5.0 для более надежного
                        соединения со смартфоном: дальность
                        действия сигнала увеличена в 4 раза.
                        Режим стереогарнитуры
                    </div>
                </div>
            </div>
        </div>

        <div class="centering-container">
            <div class="centering-container__row">
                <div class="container container_five">
                    <div class="container__description">
                        Магнитный зарядный кейс
                        для удобного хранения
                        и подзарядки наушников
                    </div>
                </div>
            </div>
        </div>

        <div class="centering-container">
            <div class="centering-container__row">
                <div class="container container_six">
                    <div class="container__description">
                    </div>
                </div>
            </div>
        </div>


        <div class="buy-container">
            <div class="buy-container__mobile-product">
                <img src="assets/img/thumbnails/nanopods-mobile.png" alt="">
            </div>
            <div class="buy-container__left-product">
                <img src="assets/img/thumbnails/nanopods-white.png" alt="">
            </div>
            <div class="buy-container__control" id="buy">
                <form id="choice-form-one">
                    <div class="buy buy_little">
                        <div class="buy__left-column">
                            <div class="buy__title">
                                Купить сейчас
                            </div>
                            <div class="buy__price">
                                6990<span>руб.</span>
                            </div>
                        </div>
                        <div class="buy__right-column">
                            <div class="buy__button">
                                <p>
                                    <select class="choice-color">
                                        <option>Черный</option>
                                        <option selected>Белый</option>
                                    </select>
                                </p>

                                <p><a id="product_link1" class="button_white hideable" href="/personal/cart/?model=black&utm_source=store.elari&utm_medium=landing&utm_term=black&utm_campaign=NPS" title="">Купить</a>

                                    <a id="product_link2" class="button_white hideable" href="/personal/cart/?model=white&utm_source=store.elari&utm_medium=landing&utm_term=white&utm_campaign=NPS" style="display: inline-block;" title="">Купить</a></p>
								<!-- Старые ссылки <p><a id="product_link1" class="button_white hideable" href="/personal/cart/?model=black&utm_source=wifi&utm_medium=land&utm_term=black&utm_campaign=evropejskij_wht" title="">Купить</a>

                                    <a id="product_link2" class="button_white hideable" href="/personal/cart/?model=white&utm_source=wifi&utm_medium=land&utm_term=white&utm_campaign=evropejskij_wht" style="display: inline-block;" title="">Купить</a></p>-->
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="buy-container__right-product">
                <img src="assets/img/thumbnails/nanopods-black.png" alt="">
            </div>
        </div>

        <footer>
            <p class="footer-contacts"><a href="https://store.elari.net/?utm_source=store.elari&utm_medium=landing&utm_content=buttom&utm_campaign=NPS " title="">store.elari.net</a>   |  <a href="mailto:info@elari.net" title="">info@elari.net</a>   |   <span>8 (800) 500 56 00</span>   |   <span>+7 (495) 120 56 00</span></p>
            <p class="copy"><a href="https://store.elari.net/?utm_source=store.elari&utm_medium=landing&utm_content=buttom&utm_campaign=NPS " title=""><img src="assets/img/logos/logo_store.svg" width="350" alt=""></a></p>
        </footer>


        <!--footer-->

<!--footer end-->

<script src="assets/js/lib/jquery-1.10.2.min.js"></script>
<script src="assets/js/lib/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/js/lib/enquire.min.js"></script>
<script src="assets/js/main.js"></script>

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
   (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
   m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
   (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym");

   ym(49805371, "init", {
        id:49805371,
        clickmap:true,
        trackLinks:true,
        accurateTrackBounce:true,
        webvisor:true,
        trackHash:true,
        ecommerce:"dataLayer"
   });
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/49805371" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


    </div>
</div>



</body>

</html>